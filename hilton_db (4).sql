-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2016 at 12:00 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hilton_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

CREATE TABLE `advertisements` (
  `id` int(11) NOT NULL,
  `image_cover` varchar(500) NOT NULL,
  `path` int(11) NOT NULL,
  `status` enum('active','disable') NOT NULL,
  `type` enum('post','page','event','external') NOT NULL,
  `external` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `advertisement_content`
--

CREATE TABLE `advertisement_content` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `adv_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `type` varchar(500) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `type`) VALUES
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password');

-- --------------------------------------------------------

--
-- Table structure for table `cms_module_access`
--

CREATE TABLE `cms_module_access` (
  `id` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `sorting` int(11) NOT NULL,
  `type` enum('module','page') NOT NULL,
  `shadow` enum('yes','no') NOT NULL,
  `file_source` text NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_module_access`
--

INSERT INTO `cms_module_access` (`id`, `sid`, `title`, `icon`, `sorting`, `type`, `shadow`, `file_source`, `inserted_by`, `inserted_date`, `update_by`, `last_update`) VALUES
(5, 0, 'Media Library', 'icon-inbox', 2, 'module', '', 'media_library_directories', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(6, 0, 'Users', 'icon-user', 1, 'module', 'no', 'users', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(7, 0, 'Menus Group', ' icon-link', 3, 'module', '', 'menu_group,menu_link', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(10, 0, 'Nodes', 'icon-file-text-alt', 4, 'module', 'no', 'nodes', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(11, 0, 'Taxonomies', 'icon-tags', 8, 'module', 'no', 'taxonomies', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(12, 0, 'Setting', 'icon-cogs', 10, 'module', 'no', 'profiles,profile_pages,localization,cms_modules,general_settings,ecom_customers_groups,ecom_taxes,ecom_payment_methods,home_layout', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(13, 5, 'Media Directories', '', 1, 'page', 'no', 'media_library_directories/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(16, 6, 'show all', '', 1, 'page', 'no', 'users/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(17, 6, 'add new', '', 2, 'page', 'no', 'users/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(22, 10, 'Posts', '', 1, 'page', 'no', 'nodes/view_posts', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(23, 10, 'add new', '', 4, 'page', 'no', 'nodes/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(25, 11, 'tags', '', 6, 'page', 'no', 'taxonomies/view_tags', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(26, 12, 'profiles', '', 2, 'page', 'no', 'profiles/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(27, 12, 'Localization', '', 8, 'page', 'no', 'localization/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(28, 12, 'CPanel Menus Structure', '', 7, 'page', 'no', 'cms_modules/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(29, 7, 'show all ', '', 1, 'page', 'no', 'menu_group/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(30, 7, 'add new ', '', 2, 'page', 'no', 'menu_group/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(31, 11, 'update', '', 11, 'page', 'no', 'taxonomies/update', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(33, 0, 'Social Activity', ' icon-comments-alt', 7, 'module', 'no', 'social_suggestion_topics,social_comments,social_email_subscription,poll_questions_options,poll_questions,form_attributes,forms,advertisements', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(34, 33, 'Comments ', '', 1, 'page', 'no', 'social_comments/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(35, 33, 'Polls ', '', 5, 'page', 'no', 'poll_questions/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(36, 33, 'Poll Options  View', '', 10, 'page', 'yes', 'poll_questions_options/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(37, 33, 'Email Subscription ', '', 15, 'page', 'no', 'social_email_subscription/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(38, 33, 'suggestion topics ', '', 16, 'page', 'no', 'social_suggestion_topics/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(39, 12, 'General Settings', '', 1, 'page', 'no', 'general_settings/update', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(40, 275, 'Plugins', '', 1, 'page', 'no', 'plugins/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(42, 275, 'Themes & Layouts', '', 2, 'page', 'no', 'themes/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(43, 12, 'Profile Insert', '', 0, 'page', 'yes', 'profiles/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(44, 12, 'Profile Update', '', 2, 'page', 'yes', 'profiles/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(45, 12, 'Profile Delete', '', 2, 'page', 'yes', 'profiles/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(47, 12, 'CPanel Menus Structure Insert', '', 3, 'page', 'yes', 'cms_modules/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(48, 12, 'CPanel Menus Structure Update', '', 3, 'page', 'yes', 'cms_modules/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(50, 12, 'Localization  Insert ', '', 8, 'page', 'yes', 'localization/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(52, 12, 'Localization update', '', 8, 'page', 'yes', 'localization/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(53, 12, 'Localization Delete', '', 8, 'page', 'yes', 'localization/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(54, 12, 'CPanel Menus Structure Delete', '', 8, 'page', 'yes', 'cms_modules/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(56, 11, 'Categories', '', 1, 'page', 'no', 'taxonomies/view_categories', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(64, 10, 'update', '', 3, 'page', 'yes', 'nodes/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(65, 10, 'delete', '', 5, 'page', 'yes', 'nodes/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(70, 6, 'update', '', 3, 'page', 'yes', 'users/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(71, 6, 'delete', '', 5, 'page', 'yes', 'users/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(72, 7, 'group update ', '', 3, 'page', 'yes', 'menu_group/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(73, 7, 'group delete ', '', 5, 'page', 'yes', 'menu_group/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(74, 33, 'Comments update ', '', 2, 'page', 'yes', 'social_comments/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(77, 33, ' Poll Insert', '', 6, 'page', 'yes', 'poll_questions/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(78, 33, 'Poll Update ', '', 7, 'page', 'yes', 'poll_questions/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(79, 33, 'Poll full info', '', 8, 'page', 'yes', 'poll_questions/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(80, 33, ' Poll delete', '', 9, 'page', 'yes', 'poll_questions/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(81, 33, 'Poll Options Insert', '', 11, 'page', 'yes', 'poll_questions_options/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(83, 33, 'Poll Options update', '', 12, 'page', 'yes', 'poll_questions_options/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(84, 33, 'Poll Option full info', '', 13, 'page', 'yes', 'poll_questions_options/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(86, 33, 'Poll Options delete', '', 14, 'page', 'yes', 'poll_questions_options/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(87, 33, 'Suggestion topics delete ', '', 17, 'page', 'yes', ' social_suggestion_topics /delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(91, 6, 'full info', '', 4, 'page', 'yes', 'users/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(92, 7, 'group full info ', '', 4, 'page', 'yes', 'menu_group/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(93, 10, 'full info', '', 4, 'page', 'yes', 'nodes/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(95, 11, 'Add New ', '', 7, 'page', 'no', 'taxonomies/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(96, 11, 'Full info', '', 14, 'page', 'yes', 'taxonomies/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(97, 12, 'profile access updates', '', 2, 'page', 'yes', 'profile_pages/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(98, 7, 'menu show all', '', 6, 'page', 'yes', 'menu_link/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(99, 7, 'menu add new', '', 7, 'page', 'yes', 'menu_link/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(101, 7, 'menu update', '', 7, 'page', 'yes', 'menu_link/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(102, 7, 'menu delete', '', 9, 'page', 'yes', 'menu_link/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(103, 7, 'menu full info', '', 8, 'page', 'yes', 'menu_link/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(107, 12, 'localization label&message', '', 17, 'page', 'yes', 'localization/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(108, 12, 'Profile Full Info', '', 2, 'page', 'yes', 'profiles/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(109, 33, 'suggestion topics full info', '', 16, 'page', 'yes', ' social_suggestion_topics /full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(110, 33, 'comments full info', '', 3, 'page', 'yes', 'social_comments/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(111, 33, 'Email Subscription  delete', '', 15, 'page', 'yes', 'social_email_subscription/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(112, 33, 'Comments delete', '', 4, 'page', 'yes', 'social_comments/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(113, 11, 'Authors', '', 5, 'page', 'no', 'taxonomies/view_authors', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(114, 11, 'delete', '', 10, 'page', 'yes', 'taxonomies/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(116, 12, 'insert theme', '', 7, 'page', 'yes', 'themes/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(117, 12, 'view layouts', '', 7, 'page', 'yes', 'themes/view_layouts', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(118, 12, 'plugins insert', '', 6, 'page', 'yes', 'plugins/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(119, 5, 'media library Insert', '', 2, 'page', 'yes', 'media_library_directories/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(120, 5, 'media library edit', '', 3, 'page', 'yes', 'media_library_directories/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(121, 5, 'media File Insert', '', 4, 'page', 'yes', 'media_library_files/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(122, 12, 'customize layout plugin', '', 14, 'page', 'yes', 'themes/customize_layout_plugin', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(127, 10, 'Pages', '', 3, 'page', 'no', 'nodes/view_pages', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(129, 7, 'Insert menu link content', '', 11, 'page', 'yes', 'menu_link/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(131, 10, ' Plugin Option', '', 7, 'page', 'yes', 'nodes/plugin_option', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(132, 33, 'Forms', '', 2, 'page', 'no', 'forms/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(133, 33, 'form insert ', '', 21, 'page', 'yes', 'forms/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(134, 33, 'forms update', '', 22, 'page', 'yes', 'forms/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(135, 33, 'forms full info', '', 23, 'page', 'yes', 'forms/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(136, 33, 'forms delete', '', 24, 'page', 'yes', 'forms/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(137, 33, 'form attributes view', '', 25, 'page', 'yes', 'form_attributes/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(138, 33, 'form attributes Insert', '', 26, 'page', 'yes', 'form_attributes/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(139, 33, 'form attributes  update', '', 27, 'page', 'yes', 'form_attributes/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(140, 33, 'form attributes  full info', '', 28, 'page', 'yes', 'form_attributes/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(141, 33, 'form attributes delete', '', 29, 'page', 'yes', 'form_attributes/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(142, 33, 'advertisements ', '', 30, 'page', 'no', 'advertisements/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(143, 33, 'advertisements insert', '', 31, 'page', 'yes', 'advertisements/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(144, 33, 'advertisements update', '', 32, 'page', 'yes', 'advertisements/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(145, 33, 'advertisements full info', '', 33, 'page', 'yes', 'advertisements/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(146, 33, 'advertisements delete', '', 34, 'page', 'yes', 'advertisements/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(149, 33, 'form content', '', 34, 'page', 'yes', 'forms/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(150, 33, 'advertisements content', '', 36, 'page', 'yes', 'advertisements/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(151, 33, 'view form table', '', 38, 'page', 'yes', 'form_attributes/view_table', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(152, 33, 'form view table', '', 4, 'page', 'yes', 'form_attributes/view_table', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(153, 33, 'form inserted data', '', 5, 'page', 'yes', 'form_attributes/inserted_data', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(154, 5, 'view files', '', 2, 'page', 'yes', 'media_library_files/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(229, 12, 'taxes full info', '', 2, 'page', 'yes', 'ecom_taxes/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(235, 12, 'payment method insert', '', 3, 'page', 'yes', 'ecom_payment_methods/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(237, 12, 'payment methods update', '', 3, 'page', 'yes', 'ecom_payment_methods/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(238, 12, ' payment methods info', '', 3, 'page', 'yes', 'ecom_payment_methods/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(254, 12, 'Home layout ', '', 40, 'page', 'no', 'home_layout/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(255, 12, 'Home layout Insert', '', 41, 'page', 'yes', 'home_layout/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(256, 12, 'home layout update', '', 41, 'page', 'yes', 'home_layout/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(257, 12, 'home layout full_info', '', 41, 'page', 'yes', 'index_layout/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(258, 12, 'home layout delete', '', 41, 'page', 'yes', 'home_layout/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(267, 33, 'Contact US', '', 39, 'page', 'no', 'contact_us/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(268, 33, 'Contact us delete', '', 39, 'page', 'yes', 'contact_us/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(275, 0, 'utilities ', ' icon-wrench', 11, 'module', 'no', 'plugins,themes,cities,options,ecom_order_statuses', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(282, 10, 'Events', '', 3, 'page', 'no', 'nodes/view_events', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `user_name` varchar(256) NOT NULL,
  `email` varchar(265) CHARACTER SET utf16 COLLATE utf16_esperanto_ci NOT NULL,
  `body` longtext NOT NULL,
  `inserted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `user_name`, `email`, `body`, `inserted_date`) VALUES
(1, 'wewe', 'wew@yahoo.com', '', '2016-09-22 17:18:03'),
(2, 'wew', 'emadtab97@gmail.com', 'wew', '2016-09-22 17:19:06'),
(3, 'ewew', 'emadtab97@gmail.com', 'ewew', '2016-09-22 17:19:23'),
(4, 'emad', 'emadtab97@gmail.com', 'ahmed ahmed ae,ds ahmed ahmeds ', '2016-09-22 17:21:39'),
(5, 'ewer', 'ere@yahoo.com', '<div class=''alert alert-block alert-danger''>undefined</div >', '2016-09-22 17:29:47'),
(6, 'sd', 'emadtab97@gmail.com', 'dsd', '2016-09-23 07:28:33'),
(7, 'Emad Rashad', 'emadtab97@gmail.com', '', '2016-09-23 08:00:45'),
(8, 'Emad Rashad', 'emadtab97@gmail.com', 'ddfff', '2016-09-23 08:10:53');

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE `customer_info` (
  `id` int(11) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `fbid` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `company` varchar(250) NOT NULL,
  `birthday` date DEFAULT '0000-00-00',
  `registration_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `account_status` enum('verified','not_verified','blocked') NOT NULL,
  `activate_code` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_selections`
--

CREATE TABLE `customer_selections` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `events_details`
--

CREATE TABLE `events_details` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `place` varchar(2580) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events_details`
--

INSERT INTO `events_details` (`id`, `event_id`, `place`, `start_date`, `end_date`) VALUES
(1, 1, 'uytuty', '2016-06-02 11:58:00', '2016-06-02 11:58:00'),
(2, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 5, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 6, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 7, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 8, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 9, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 10, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 11, '', '2016-06-07 05:17:00', '2016-06-18 05:17:00'),
(12, 12, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 13, '', '2016-06-07 11:44:00', '2016-06-07 11:44:00'),
(14, 14, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 15, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 16, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 3, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 4, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 5, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 6, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 7, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 8, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 9, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 10, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 11, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 12, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 13, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 14, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 15, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 16, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 17, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 18, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 19, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 20, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 21, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 22, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 23, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 24, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 25, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 26, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 27, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 28, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 29, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 30, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 31, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 32, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 33, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 34, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 35, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 36, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 37, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 38, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 39, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 40, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `forget_password`
--

CREATE TABLE `forget_password` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `label` varchar(250) NOT NULL,
  `enable` enum('yes','no') NOT NULL,
  `email_to` varchar(500) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `name`, `label`, `enable`, `email_to`, `inserted_date`, `inserted_by`, `last_update`, `update_by`) VALUES
(1, 'dfgdfg', 'tret', 'no', 'ttrt', '2015-05-24 16:09:05', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_attributes`
--

CREATE TABLE `form_attributes` (
  `id` int(11) NOT NULL,
  `attribute_label` varchar(250) NOT NULL,
  `sorting` int(11) NOT NULL,
  `required` enum('yes','no') NOT NULL,
  `form_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_values` text NOT NULL,
  `inserted_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `last_update` date NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_attributes`
--

INSERT INTO `form_attributes` (`id`, `attribute_label`, `sorting`, `required`, `form_id`, `attribute_id`, `attribute_values`, `inserted_date`, `inserted_by`, `last_update`, `update_by`) VALUES
(1, 'Name', 1, 'no', 1, 1, '', '2015-05-24 16:10:14', 1, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_inserted_data`
--

CREATE TABLE `form_inserted_data` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `label` varchar(25) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `general_setting`
--

CREATE TABLE `general_setting` (
  `id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `site_url` varchar(500) NOT NULL,
  `meta_key` text NOT NULL,
  `email` varchar(256) NOT NULL,
  `time_zone_id` int(11) NOT NULL,
  `front_lang_id` int(11) NOT NULL,
  `translate_lang_id` int(11) NOT NULL,
  `enable_website` enum('yes','no') NOT NULL,
  `offline_messages` longtext NOT NULL,
  `description` longtext NOT NULL,
  `google_analitic` longtext CHARACTER SET utf16 NOT NULL,
  `main_order_statues` int(11) NOT NULL,
  `enable_store` enum('yes','no') NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_setting`
--

INSERT INTO `general_setting` (`id`, `title`, `site_url`, `meta_key`, `email`, `time_zone_id`, `front_lang_id`, `translate_lang_id`, `enable_website`, `offline_messages`, `description`, `google_analitic`, `main_order_statues`, `enable_store`, `update_by`, `last_update`) VALUES
(1, 'Welcome To Hilton Ramsis|أهلا بكم فى هيلتون رمسيس', 'localhost/hilton', 'website design in Egypt, web development in egypt,Mobile solutions, web applications, web based applications, web marketing, web services, seo, search engine optimization||', 'emadtab97@gmail.com', 35, 1, 1, 'yes', '7567', 'Diva company provides the latest technologies in web design and web development in Egypt & ME providing responsive and good user experience interface||', '767', 8, '', 1, '2016-09-23 05:04:45');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_layout`
--

CREATE TABLE `home_page_layout` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `status` enum('publish','draft') NOT NULL,
  `position` int(11) NOT NULL,
  `plugin` int(11) NOT NULL,
  `header_title` varchar(500) NOT NULL,
  `plugin_value` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_page_layout`
--

INSERT INTO `home_page_layout` (`id`, `title`, `status`, `position`, `plugin`, `header_title`, `plugin_value`) VALUES
(1, 'position one ', 'publish', 1, 38, '', ''),
(2, 'postion2', 'publish', 3, 39, '', ''),
(3, '3', 'publish', 2, 40, '', ''),
(4, '7', 'publish', 9, 41, 'Latest News||Ø§Ø®Ø± Ø§Ù„Ø§Ø®Ø¨Ø§Ø±', '2'),
(5, '9', 'publish', 7, 42, 'Important links||Ø±ÙˆØ§Ø¨Ø· Ù…Ù‡Ù…Ø©', 'main_menu'),
(6, '8', 'publish', 8, 43, 'Upcoming Event ||Ø§Ø®Ø± Ø§Ù„Ø§Ø­Ø¯Ø§Ø«', '12');

-- --------------------------------------------------------

--
-- Table structure for table `localization`
--

CREATE TABLE `localization` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `sorting` int(11) NOT NULL,
  `status` enum('active','disable') NOT NULL,
  `label` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `localization`
--

INSERT INTO `localization` (`id`, `name`, `sorting`, `status`, `label`) VALUES
(1, 'English', 1, 'active', 'en'),
(2, 'Arabic', 2, 'active', 'ar');

-- --------------------------------------------------------

--
-- Table structure for table `membership_inquery`
--

CREATE TABLE `membership_inquery` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_email` varchar(150) NOT NULL,
  `customer_position` varchar(150) NOT NULL,
  `customer_company` varchar(150) NOT NULL,
  `customer_mobile` varchar(150) NOT NULL,
  `customer_fax` varchar(150) NOT NULL,
  `customer_marital_status` varchar(150) NOT NULL,
  `customer_credit_type` varchar(150) NOT NULL,
  `customer_graduation_year` varchar(150) NOT NULL,
  `customer_education_year` varchar(150) NOT NULL,
  `customer_nationality` varchar(150) NOT NULL,
  `customer_residence` varchar(150) NOT NULL,
  `customer_monthly_income` varchar(150) NOT NULL,
  `cutomer_traveled_country` varchar(150) NOT NULL,
  `cutomer_traveled_hotel` varchar(150) NOT NULL,
  `customer_traveled_year` varchar(150) NOT NULL,
  `customer_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `membership_inquery`
--

INSERT INTO `membership_inquery` (`id`, `customer_name`, `customer_email`, `customer_position`, `customer_company`, `customer_mobile`, `customer_fax`, `customer_marital_status`, `customer_credit_type`, `customer_graduation_year`, `customer_education_year`, `customer_nationality`, `customer_residence`, `customer_monthly_income`, `cutomer_traveled_country`, `cutomer_traveled_hotel`, `customer_traveled_year`, `customer_comment`) VALUES
(1, 'cv', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(5, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `nodes`
--

CREATE TABLE `nodes` (
  `id` int(11) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `enable_summary` enum('yes','no') NOT NULL,
  `inserted_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `start_publishing` datetime NOT NULL,
  `end_publishing` datetime NOT NULL,
  `enable_comments` enum('yes','no') NOT NULL,
  `front_page` enum('yes','no') NOT NULL,
  `slide_show` enum('yes','no') NOT NULL,
  `side_bar` enum('yes','no') NOT NULL,
  `cover_image` varchar(250) NOT NULL,
  `slider_cover` varchar(250) NOT NULL,
  `node_type` enum('post','page','event') NOT NULL,
  `place` varchar(256) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `model` int(11) NOT NULL,
  `shortcut_link` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes`
--

INSERT INTO `nodes` (`id`, `status`, `enable_summary`, `inserted_date`, `inserted_by`, `last_update`, `update_by`, `start_publishing`, `end_publishing`, `enable_comments`, `front_page`, `slide_show`, `side_bar`, `cover_image`, `slider_cover`, `node_type`, `place`, `start_date`, `end_date`, `model`, `shortcut_link`) VALUES
(1, 'publish', 'no', '2016-09-13 04:38:44', 1, '2016-09-25 14:28:47', 1, '2016-09-13 04:38:44', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473734324'),
(2, 'publish', 'no', '2016-09-13 08:51:35', 1, '2016-09-13 09:56:56', 1, '2016-09-13 08:51:35', '0000-00-00 00:00:00', 'no', 'yes', 'yes', 'no', '', '1.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473749495'),
(3, 'publish', 'no', '2016-09-13 10:53:00', 1, '0000-00-00 00:00:00', 0, '2016-09-13 10:53:00', '0000-00-00 00:00:00', 'no', 'yes', 'yes', 'no', '', '2.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473756780'),
(4, 'publish', 'no', '2016-09-13 13:12:09', 1, '0000-00-00 00:00:00', 0, '2016-09-13 13:12:09', '0000-00-00 00:00:00', 'no', 'yes', 'yes', 'no', '', '3_2016-09-13031125.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473765129'),
(5, 'publish', 'no', '2016-09-13 23:27:06', 1, '0000-00-00 00:00:00', 0, '2016-09-13 23:27:06', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473802026'),
(6, 'publish', 'no', '2016-09-13 23:57:39', 1, '2016-09-14 00:30:28', 1, '2016-09-13 23:57:39', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'no', 'images/footer-col1.jpg ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473803859'),
(7, 'publish', 'no', '2016-09-14 00:58:51', 1, '2016-09-20 11:30:41', 1, '2016-09-14 00:58:51', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'no', 'images/hotel2.jpg      ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473807531'),
(8, 'publish', 'no', '2016-09-14 01:21:01', 1, '2016-09-20 10:48:15', 1, '2016-09-14 01:21:01', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'no', 'images/hotel2.jpg     ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473808861'),
(9, 'publish', 'no', '2016-09-14 01:31:53', 1, '2016-09-20 09:13:12', 1, '2016-09-14 01:31:53', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'no', 'images/hotel.jpg ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473809513'),
(10, 'publish', 'no', '2016-09-14 09:14:07', 1, '2016-09-14 09:23:26', 1, '2016-09-14 09:14:07', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'no', 'testimonials/slide.jpg', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473837247'),
(11, 'publish', 'no', '2016-09-15 02:30:42', 1, '2016-09-20 10:38:56', 1, '2016-09-15 02:30:42', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'no', 'testimonials/apple-touch-icon-144x144-precomposed.png  ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473899442'),
(12, 'publish', 'no', '2016-09-15 02:37:28', 1, '2016-09-20 10:42:16', 1, '2016-09-15 02:37:28', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'no', 'testimonials/testo_img.jpg ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473899848'),
(13, 'publish', 'no', '2016-09-15 02:51:22', 1, '2016-09-20 11:10:37', 1, '2016-09-15 02:51:22', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'no', 'images/sidebar_bg.jpg     ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473900682'),
(14, 'publish', 'no', '2016-09-15 03:07:54', 1, '2016-09-15 03:11:27', 1, '2016-09-15 03:07:54', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'no', 'images/footer-col1.jpg', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473901674'),
(15, 'publish', 'no', '2016-09-15 03:28:48', 1, '2016-09-15 03:31:33', 1, '2016-09-15 03:28:48', '0000-00-00 00:00:00', 'no', 'yes', 'no', 'no', 'images/footer-col2.jpg', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1473902928'),
(16, 'publish', 'no', '2016-09-17 05:49:03', 1, '2016-09-18 10:01:49', 1, '2016-09-17 05:49:03', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'map/qr.svg ', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, '1474084143'),
(17, 'publish', 'no', '2016-09-18 12:35:57', 1, '2016-09-21 10:37:22', 1, '2016-09-18 12:35:57', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, '1474194957'),
(18, 'publish', 'no', '2016-09-18 12:36:21', 1, '2016-09-25 14:31:05', 1, '2016-09-18 12:36:21', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, '1474194981'),
(21, 'publish', 'no', '2016-09-18 13:02:47', 1, '2016-09-18 14:38:28', 1, '2016-09-18 13:02:47', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, '1474196567'),
(22, 'publish', 'no', '2016-09-18 16:13:51', 1, '2016-09-20 14:55:31', 1, '2016-09-18 16:13:51', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, '1474208031'),
(23, 'publish', 'no', '2016-09-19 22:57:32', 1, '2016-09-19 23:47:54', 1, '2016-09-19 22:57:32', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, '1474318652'),
(24, 'publish', 'no', '2016-09-20 09:04:43', 1, '2016-09-20 09:09:02', 1, '2016-09-20 09:04:43', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, '1474355083'),
(26, 'publish', 'no', '2016-09-20 09:36:12', 1, '0000-00-00 00:00:00', 0, '2016-09-20 09:36:12', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, '1474356972'),
(27, 'publish', 'no', '2016-09-20 10:20:22', 1, '2016-09-20 10:23:16', 1, '2016-09-20 10:20:22', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, '1474359622'),
(28, 'publish', 'no', '2016-09-21 10:38:40', 1, '2016-09-25 13:52:13', 1, '2016-09-21 10:38:40', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'Rooms/Green-Hotel-Room.jpg ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1474447120'),
(29, 'publish', 'no', '2016-09-21 10:42:22', 1, '0000-00-00 00:00:00', 0, '2016-09-21 10:42:22', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, '1474447342'),
(30, 'publish', 'no', '2016-09-21 10:43:33', 1, '2016-09-25 13:51:16', 1, '2016-09-21 10:43:33', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'Rooms/msyrvr-omni-riverfront-hotel-deluxe-room.jpg  ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1474447413'),
(31, 'publish', 'no', '2016-09-21 10:45:10', 1, '2016-09-21 11:32:37', 1, '2016-09-21 10:45:10', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'Rooms/full-room-view.jpg   ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1474447510'),
(32, 'publish', 'no', '2016-09-21 14:14:45', 1, '2016-09-22 07:46:53', 1, '2016-09-21 14:14:45', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, '1474460085'),
(33, 'publish', 'no', '2016-09-21 15:06:24', 1, '2016-09-26 05:48:51', 1, '2016-09-21 15:06:24', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'sharm%20elshiekh/sharm1.jpg   ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1474463184'),
(34, 'publish', 'no', '2016-09-22 07:28:09', 1, '2016-09-26 05:49:17', 1, '2016-09-21 00:47:00', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'sokhna/3841504_7_z.jpg    ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1474522089'),
(35, 'publish', 'no', '2016-09-22 08:27:22', 1, '2016-09-26 05:50:42', 1, '2016-09-21 01:33:00', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'usa/msyrvr-omni-riverfront-hotel-deluxe-room.jpg    ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1474525642'),
(36, 'publish', 'no', '2016-09-25 08:11:31', 1, '2016-09-26 05:51:04', 1, '2016-09-25 08:11:31', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'dubai/Green-Hotel-Room.jpg    ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1474783891'),
(37, 'publish', 'no', '2016-09-25 12:42:26', 1, '2016-09-26 06:58:20', 1, '2016-09-25 12:42:26', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'sharm%20elshiekh/sharm3.jpg  ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1474800146'),
(38, 'publish', 'no', '2016-09-25 12:58:22', 1, '2016-09-26 07:44:54', 1, '2016-09-25 12:58:22', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'dubai/Green-Hotel-Room.jpg  ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1474801102'),
(39, 'publish', 'no', '2016-09-25 13:01:59', 1, '2016-09-25 13:02:10', 1, '2016-09-25 13:01:59', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/hotel.jpg', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1474801319'),
(40, 'publish', 'no', '2016-09-25 13:07:29', 1, '2016-09-25 13:07:37', 1, '2016-09-25 13:07:29', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'about_folder/hotel.jpg', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1474801649');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_content`
--

CREATE TABLE `nodes_content` (
  `id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `alias` varchar(250) NOT NULL,
  `summary` text NOT NULL,
  `body` longtext NOT NULL,
  `meta_keys` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes_content`
--

INSERT INTO `nodes_content` (`id`, `node_id`, `lang_id`, `title`, `alias`, `summary`, `body`, `meta_keys`, `meta_description`) VALUES
(1, 1, 2, 'معلومات الهيدر الرئيسى', 'معلومات_الهيدر_الرئيسى', '<p>info@hilton.comeere</p>', '<p>+20-100-600 1270gffgfg</p>', '', ''),
(2, 1, 1, 'Header Contact Info', 'header_contact_info', '<p>info@hilton.comerer</p>', '<p>+20-100-600 1270fgfgf</p>', '', ''),
(3, 2, 2, 'سليد رقم 1', 'سليد_رقم_1', '<p>اختبار اختبار</p>', '', '', ''),
(4, 2, 1, 'First Slide', 'first_slide', '<p>test test</p>', '<p>test just body test </p>', '', ''),
(5, 3, 2, 'سليد رقم 2', 'سليد_رقم_2', '<p>تكست سليد 2</p>', '', '', ''),
(6, 3, 1, 'Slide 2', 'slide_2', '<p>slide 2 text</p>', '', '', ''),
(7, 4, 2, 'سليد رقم 3', 'سليد_رقم_3', '<p>تكست تكست </p>', '', '', ''),
(8, 4, 1, 'Slide 3', 'slide_3', '<p>text text</p>', '', '', ''),
(9, 5, 2, 'العروض الخاصه', 'العروض_الخاصه', '', '', '', ''),
(10, 5, 1, 'Special Offers', 'special_offers', '', '', '', ''),
(11, 6, 2, 'اطفال اطفال', 'اطفال_اطفال', '<p>عرض خاص رئيسى</p>', '<p>عرض الاطفال الخاص هو افضل عرض للاطفال عامه موجود فى رمسيس هيلتون - يبدا من - 5000 جنيه</p>', '', ''),
(12, 6, 1, 'Free Kids', 'free_kids', '<p>main special offer</p>', '<p>Kids Special offer is the best one in Hitlon Ramses it''s amazing - From - 2000 L.E</p>', '', ''),
(13, 7, 2, 'العرض 1', 'العرض_1', '<p>العروض</p>', '<p>مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار</p>', '', ''),
(14, 7, 1, 'Offer 1', 'offer_1', '<p>Offers</p>', '<p>Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees</p>', '', ''),
(15, 8, 2, 'العرض 2', 'العرض_2', '<p>العروض</p>', '<p>مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار</p>', '', ''),
(16, 8, 1, 'Offer 2', 'offer_2', '<p>Offers</p>', '<p>Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees</p>', '', ''),
(17, 9, 2, 'اختبار', 'اختبار', '<p>مجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهات</p>', '<p>مجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهاتمجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهاتمجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهاتمجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهاتمجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهاتمجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهاتمجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهاتمجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهاتمجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهاتمجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهاتمجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهاتمجرد اختبار لديسكريبشن هيتحط هنا المفروض يقدم رمسيس هيلتون عروض مميزه جدا من غرف واجنحه وقاعات افراح واستديوهات</p>', '', ''),
(18, 9, 1, 'Test News', 'test_news', '<p>Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees </p>', '<p>Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees Ramses Hilton Offers many facilities to customers enjoyable week-ends and categories and sub sub categorieees</p>', '', ''),
(19, 10, 2, 'قالوا عن هيلتون', 'قالوا_عن_هيلتون', '', '', '', ''),
(20, 10, 1, 'Testimonials', 'testimonials', '', '', '', ''),
(21, 11, 2, 'تستى رقم واحد', 'تستى_رقم_واحد', '<p>قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون </p>', '<p>قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون</p>', '', ''),
(22, 11, 1, 'Testi number one', 'testi_number_one', '<p>Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton </p>', '<p>Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton Say About Hilton</p>', '', ''),
(23, 12, 2, 'قالوا عن هيلتون 2', 'قالوا_عن_هيلتون_2', '<p>قالوا عن رمسيس هيلتون 2 قالوا عن رمسيس هيلتون 2 قالوا عن رمسيس هيلتون 2 قالوا عن رمسيس هيلتون 2 قالوا عن رمسيس هيلتون 2 قالوا عن رمسيس هيلتون 2 قالوا عن رمسيس هيلتون 2 قالوا عن رمسيس هيلتون 2 قالوا عن رمسيس هيلتون 2 قالوا عن رمسيس هيلتون 2 قالوا عن رمسيس هيلتون 2 قالوا عن رمسيس هيلتون 2 قالوا عن رمسيس هيلتون 2 </p>', '<p>قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالواقالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالواقالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالواقالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالواقالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالواقالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالواقالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالواقالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالواقالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالواقالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالواقالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالواقالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالواقالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا عن هيلتون قالوا</p>', '', ''),
(24, 12, 1, 'Say About Hilton 2', 'say_about_hilton_2', '<p>Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2</p>', '<p>Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2Say About Hilton 2</p>', '', ''),
(25, 13, 2, 'عن التايم شير', 'عن_التايم_شير', '<p>Escape to the banks of the Nile at the exquisite Hilton Luxor Resort & Spa, just 10 minutes from Luxor town center. Our stylish resort is the ideal base for exploring the region’s historical attractions. Among the eight restaurants and bars at our resort, you''ll find fragrant Arabian-style teas and Turkish coffees with waterfront views at Diwan Shisha Café. The sunken Oasis Pool Bar serves tasty drinks.</p>', '<p>Escape to the banks of the Nile at the exquisite Hilton Luxor Resort & Spa, just 10 minutes from Luxor town center. Our stylish resort is the ideal base for exploring the region’s historical attractions. Among the eight restaurants and bars at our resort, you''ll find fragrant Arabian-style teas and Turkish coffees with waterfront views at Diwan Shisha Café. The sunken Oasis Pool Bar serves tasty drinks Escape to the banks of the Nile at the exquisite Hilton Luxor Resort & Spa, just 10 minutes from Luxor town center. Our stylish resort is the ideal base for exploring the region’s historical attractions. Among the eight restaurants and bars at our resort, you''ll find fragrant Arabian-style teas and Turkish coffees with waterfront views at Diwan Shisha Café. The sunken Oasis Pool Bar serves tasty drinks</p>', '', ''),
(26, 13, 1, 'About Time Share', 'about_time_share', '<p>Escape to the banks of the Nile at the exquisite Hilton Luxor Resort & Spa, just 10 minutes from Luxor town center. Our stylish resort is the ideal base for exploring the region’s historical attractions. Among the eight restaurants and bars at our resort, you''ll find fragrant Arabian-style teas and Turkish coffees with waterfront views at Diwan Shisha Café. The sunken Oasis Pool Bar serves tasty drinks.</p>', '<p>Escape to the banks of the Nile at the exquisite Hilton Luxor Resort & Spa, just 10 minutes from Luxor town center. Our stylish resort is the ideal base for exploring the region’s historical attractions. Among the eight restaurants and bars at our resort, you''ll find fragrant Arabian-style teas and Turkish coffees with waterfront views at Diwan Shisha Café. The sunken Oasis Pool Bar serves tasty drinks Escape to the banks of the Nile at the exquisite Hilton Luxor Resort & Spa, just 10 minutes from Luxor town center. Our stylish resort is the ideal base for exploring the region’s historical attractions. Among the eight restaurants and bars at our resort, you''ll find fragrant Arabian-style teas and Turkish coffees with waterfront views at Diwan Shisha Café. The sunken Oasis Pool Bar serves tasty drinks</p>', '', ''),
(27, 14, 2, 'القائمه البريديه', 'القائمه_البريديه', '<p>اشترك فى </p>', '<p>القائمه البريديه</p>', '', ''),
(28, 14, 1, 'News Letter', 'news_letter', '<p>Subscribe To</p>', '<p>Newsletter</p>', '', ''),
(29, 15, 2, 'معلومات الفوتر', 'معلومات_الفوتر', '<p>العنوان هيتحط هنا</p>', '', '', ''),
(30, 15, 1, 'Footer info', 'footer_info', '<p>address will go here</p>', '', '', ''),
(31, 16, 2, 'اتصل بنا', 'اتصل_بنا', '<p>https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13827.245523305372!2d30.91304335!3d29.956103600000006!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2seg!4v1465214332398</p>', '<p>النيل بدراوى كورنيش النيل المعادى القاهره مصر</p>', '', ''),
(32, 16, 1, 'Contact Us', 'contact_us', '<p>https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13827.245523305372!2d30.91304335!3d29.956103600000006!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2seg!4v1465214332398</p>', '<p>11th Floor, Nile Badrawi building, Corniche El Nile, Maadi, Cairo, egypt.</p>', '', ''),
(33, 17, 2, 'الغرف', 'الغرف', '', '', '', ''),
(34, 17, 1, 'Rooms', 'rooms', '', '', '', ''),
(35, 18, 2, 'سبا', 'سبا', '', '<section class="col-md-12 give-padding-tb"><!--start of internal content-->\n<div class="container">\n<h2 class="col-md-12 give-title-slogan give-display-inline">Sharm El Shiekh</h2>\n<div class="row clearfix give-mb-lg ">\n<div class="col-md-6 about-text">\n<p class="col-md-12 give-text-c">Ideally situated on the southern tip of the Sinai Peninsula, Sharm El Sheikh is considered as one of the world’s renowned vacation destinations. With its unique marine life, Sharm El Sheikh has given the Red Sea an international reputation as one of the world’s most extraordinary and exquisite diving sites. Sharm El Sheikh is known for its magical desert mountains, which offer adventure and excitement on jeep safaris, horse and camel rides. As for the nightlife, Sharm El Sheikh offers a wide range of venues, discos, pubs, and casinos. A wide array of restaurants offering a variety of cuisines including Oriental, Lebanese, Indian, Chinese, French and Italian are available, health clubs, golf courses, and shops offering local and foreign products. Sharm El Sheikh combines the thrill of water sports and the simplicity of the sun, sea and sand with the joy of night activities. Enjoying a warm and sunny climate all year round, Sharm El Sheikh is only a 50-minute flight from the Egyptian capital; Cairo, and many European charter operators now offer direct flights to Sharm El Sheikh from several European and Arabs capitals.</p>\n</div>\n[cms_plugin@spa_gallery@spa_folder]</div>\n</div>\n</section>\n<!--end of internal content-->', '', ''),
(36, 18, 1, 'Spa', 'spa', '', '<section class="col-md-12 give-padding-tb"><!--start of internal content-->\n<div class="container">\n<h2 class="col-md-12 give-title-slogan give-display-inline">Sharm El Shiekh</h2>\n<div class="row clearfix give-mb-lg ">\n<div class="col-md-6 about-text">\n<p class="col-md-12 give-text-c">Ideally situated on the southern tip of the Sinai Peninsula, Sharm El Sheikh is considered as one of the world’s renowned vacation destinations. With its unique marine life, Sharm El Sheikh has given the Red Sea an international reputation as one of the world’s most extraordinary and exquisite diving sites. Sharm El Sheikh is known for its magical desert mountains, which offer adventure and excitement on jeep safaris, horse and camel rides. As for the nightlife, Sharm El Sheikh offers a wide range of venues, discos, pubs, and casinos. A wide array of restaurants offering a variety of cuisines including Oriental, Lebanese, Indian, Chinese, French and Italian are available, health clubs, golf courses, and shops offering local and foreign products. Sharm El Sheikh combines the thrill of water sports and the simplicity of the sun, sea and sand with the joy of night activities. Enjoying a warm and sunny climate all year round, Sharm El Sheikh is only a 50-minute flight from the Egyptian capital; Cairo, and many European charter operators now offer direct flights to Sharm El Sheikh from several European and Arabs capitals.</p>\n</div>\n[cms_plugin@spa_gallery@spa_folder]</div>\n</div>\n</section>\n<!--end of internal content-->', '', ''),
(41, 21, 2, 'العضويه', 'العضويه', '', '', '', ''),
(42, 21, 1, 'Membership Inquery', 'membership_inquery', '', '', '', ''),
(43, 22, 2, 'عن شرم الشيخ', 'عن_شرم_الشيخ', '', '<section class="col-md-12 give-padding-tb"><!--start of internal content-->\n<div class="container">\n<h2 class="col-md-12 give-title-slogan give-display-inline">شرم الشيخ</h2>\n<div class="row clearfix give-mb-lg">\n<div class="col-md-6 about-text">\n<p class="col-md-12 give-text-c">شيدت مدينة شرم الشيخ عام 1968، وتشتهر باسم مدينة السلام. وتطورت المدينة منذ ذلك التاريخ بشكل سريع حتى أصبحت من أشهر المدن السياحية في سيناء والعالم، وتعد واحدة من أجمل أربع مدن في العالم طبقاً لتصنيف البي بي سي لعام 2005. كما أدى تحول المدينة إلى النظم الحديثة في المعمار والترفيه والأمان والخدمة الفندقية لتأهيلها للفوز بجائزة منظمة اليونسكو لاختيارها ضمن أفضل خمس مدن سلام على مستوى العالم من بين 400 مدينة عالمية.</p>\n</div>\n[cms_plugin@page_gallery@about_folder]</div>\n<h2 class="col-md-12 give-title-slogan give-display-inline">HSDVC Location</h2>\n<p class="col-md-12 give-text-c">Hilton Sharm Dreams Vacation Club commands one of the finest positions in central Sharm El Sheikh, in the heart of Naama Bay and 7.5 miles from Sharm el-Sheikh International Airport. Providing its owners and guests 750 meters of the best stretch of Naama Bay; a truly unique experience in the center of the most popular beachfront in Sharm El Sheikh.  Excellent facilities on the beach such as football, volleyball courts, water sports activities, onsite diving center and offshore activities to keep the most active guest satisfied. Gourmet restaurants on the beach for the perfect romantic dinner and bars serving a full complement of beverages and snacks to enjoy an unforgettable seaside experience. Board the Caribi Bar ''ship'' to listen to live music or play billiards. Dine on a delicious buffet on the terrace of Le Jardin, try Italian dishes at Casa Sharm or Mexican food at Tex-Mex. Enjoy authentic Lebanese food in Al Arze overlooking the main swimming pool and the mountains of Sinai.</p>\n<div class="col-md-12 internal-map"><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13827.245523305372!2d30.91304335!3d29.956103600000006!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2seg!4v1465214332398" width="100%" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>\n</div>\n</section>\n<!--end of internal content-->\n<p> </p>', '', ''),
(44, 22, 1, 'About Sharm Elsheikh', 'about_sharm_elsheikh', '', '<section class="col-md-12 give-padding-tb"><!--start of internal content-->\n<div class="container">\n<h2 class="col-md-12 give-title-slogan give-display-inline">Sharm El Shiekh</h2>\n<div class="row clearfix give-mb-lg">\n<div class="col-md-6 about-text">\n<p class="col-md-12 give-text-c">Ideally situated on the southern tip of the Sinai Peninsula, Sharm El Sheikh is considered as one of the world’s renowned vacation destinations. With its unique marine life, Sharm El Sheikh has given the Red Sea an international reputation as one of the world’s most extraordinary and exquisite diving sites. Sharm El Sheikh is known for its magical desert mountains, which offer adventure and excitement on jeep safaris, horse and camel rides. As for the nightlife, Sharm El Sheikh offers a wide range of venues, discos, pubs, and casinos. A wide array of restaurants offering a variety of cuisines including Oriental, Lebanese, Indian, Chinese, French and Italian are available, health clubs, golf courses, and shops offering local and foreign products. Sharm El Sheikh combines the thrill of water sports and the simplicity of the sun, sea and sand with the joy of night activities. Enjoying a warm and sunny climate all year round, Sharm El Sheikh is only a 50-minute flight from the Egyptian capital; Cairo, and many European charter operators now offer direct flights to Sharm El Sheikh from several European and Arabs capitals.</p>\n</div>\n[cms_plugin@page_gallery@about_folder]</div>\n<h2 class="col-md-12 give-title-slogan give-display-inline">HSDVC Location</h2>\n<p class="col-md-12 give-text-c">Hilton Sharm Dreams Vacation Club commands one of the finest positions in central Sharm El Sheikh, in the heart of Naama Bay and 7.5 miles from Sharm el-Sheikh International Airport. Providing its owners and guests 750 meters of the best stretch of Naama Bay; a truly unique experience in the center of the most popular beachfront in Sharm El Sheikh.  Excellent facilities on the beach such as football, volleyball courts, water sports activities, onsite diving center and offshore activities to keep the most active guest satisfied. Gourmet restaurants on the beach for the perfect romantic dinner and bars serving a full complement of beverages and snacks to enjoy an unforgettable seaside experience. Board the Caribi Bar ''ship'' to listen to live music or play billiards. Dine on a delicious buffet on the terrace of Le Jardin, try Italian dishes at Casa Sharm or Mexican food at Tex-Mex. Enjoy authentic Lebanese food in Al Arze overlooking the main swimming pool and the mountains of Sinai.</p>\n<div class="col-md-12 internal-map"><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13827.245523305372!2d30.91304335!3d29.956103600000006!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2seg!4v1465214332398" width="100%" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>\n</div>\n</section>\n<!--end of internal content-->\n<p> </p>', '', ''),
(45, 23, 2, 'العروض', 'العروض', '<p>مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار</p>', '', '', ''),
(46, 23, 1, 'Offers', 'offers', '<p>Welcome to Hilton Luxor Resort & Spa''s Special Offer page. We are delighted to give you the opportunity to enjoy our latest and best offers including accommodation, dining, Hotel activities and Spa treatments as well.</p>', '', '', ''),
(47, 24, 2, 'نادى الاجازه', 'نادى_الاجازه', '', '<section class="col-md-12 give-padding-tb"><!--start of internal content-->\n<div class="container">\n<div class="col-md-12  give-padding-tb">\n<h2 class="col-md-12 give-title-slogan give-display-inline">الخدمات والتسهيلات </h2>\n<p class="col-md-12 give-text-c">Owners at Hilton Sharm Dreams Vacation Club will also enjoy the privilege of using various facilities and services offered at the resort.</p>\n<ul class="col-md-12 internal-vacation-list give-list-block give-nolist give-padding-rl">\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n</ul>\n</div>\n<div class="col-md-12  give-padding-tb">\n<h2 class="col-md-12 give-title-slogan give-display-inline">الخدمات والتسهيلات </h2>\n<p class="col-md-12 give-text-c">Owners at Hilton Sharm Dreams Vacation Club will also enjoy the privilege of using various facilities and services offered at the resort.</p>\n<ul class="col-md-12 internal-vacation-list give-list-inline give-nolist give-padding-rl">\n<li>Limousine Desk.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n</ul>\n</div>\n<div class="col-md-12  give-padding-tb">\n<h2 class="col-md-12 give-title-slogan give-display-inline">الخدمات والتسهيلات </h2>\n<p class="col-md-12 give-text-c">Owners at Hilton Sharm Dreams Vacation Club will also enjoy the privilege of using various facilities and services offered at the resort.</p>\n<ul class="col-md-12 internal-vacation-list give-list-block give-nolist give-padding-rl">\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n</ul>\n</div>\n</div>\n</section>', '', ''),
(48, 24, 1, 'Vacation Club', 'vacation_club', '', '<section class="col-md-12 give-padding-tb"><!--start of internal content-->\n<div class="container">\n<div class="col-md-12  give-padding-tb">\n<h2 class="col-md-12 give-title-slogan give-display-inline">Resort Facilities & Services</h2>\n<p class="col-md-12 give-text-c">Owners at Hilton Sharm Dreams Vacation Club will also enjoy the privilege of using various facilities and services offered at the resort.</p>\n<ul class="col-md-12 internal-vacation-list give-list-block give-nolist give-padding-rl">\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n</ul>\n</div>\n<div class="col-md-12  give-padding-tb">\n<h2 class="col-md-12 give-title-slogan give-display-inline">Resort Facilities & Services</h2>\n<p class="col-md-12 give-text-c">Owners at Hilton Sharm Dreams Vacation Club will also enjoy the privilege of using various facilities and services offered at the resort.</p>\n<ul class="col-md-12 internal-vacation-list give-list-inline give-nolist give-padding-rl">\n<li>Limousine Desk.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n</ul>\n</div>\n<div class="col-md-12  give-padding-tb">\n<h2 class="col-md-12 give-title-slogan give-display-inline">Resort Facilities & Services</h2>\n<p class="col-md-12 give-text-c">Owners at Hilton Sharm Dreams Vacation Club will also enjoy the privilege of using various facilities and services offered at the resort.</p>\n<ul class="col-md-12 internal-vacation-list give-list-block give-nolist give-padding-rl">\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n<li>Nine swimming pools - four heated in winter.</li>\n</ul>\n</div>\n</div>\n</section>', '', ''),
(51, 26, 2, 'استطلاع الراى', 'استطلاع_الراى', '', '', '', ''),
(52, 26, 1, 'Hotel Survey', 'hotel_survey', '', '', '', ''),
(53, 27, 2, 'قالوا عن هيلتون', 'قالوا_عن_هيلتون', '', '', '', ''),
(54, 27, 1, 'Testimonials', 'testimonials', '', '', '', ''),
(55, 28, 2, 'الغرفه رقم 1', 'الغرفه_رقم_1', '<p>مميزات الغرفه-تتميز هذه الغرفه بكل المواصفات الجيده تتميز هذه الغرفه بكل المواصفات الجيده تتميز هذه الغرفه بكل المواصفات الجيده تتميز هذه الغرفه بكل المواصفات الجيده</p>', '<p>مكونات الغرفه - تتكون الغرفه من تتكون الغرفه من العديد تتكون الغرفه من العديد تتكون الغرفه من العديد</p>', '', ''),
(56, 28, 1, 'Room 1', 'room_1', '<p>Unit Facilities:-Twin Bedroom that accommodates two persons fully furnished connected to a terrace and equipped with satellite TV, Mini Bar and en-suite Luxury Bathroom with shower & tub.</p>', '<p>Unit Components:-Twin Bedroom that accommodates two persons fully furnished connected to a terrace and equipped with satellite TV, Mini Bar and en-suite Luxury Bathroom with shower & tub.</p>', '', ''),
(57, 29, 2, 'الغرفه رقم 2', 'الغرفه_رقم_2', '', '', '', ''),
(58, 29, 1, 'Room number 2', 'room_number_2', '', '', '', ''),
(59, 30, 2, 'الغرفه رقم 3', 'الغرفه_رقم_3', '<p>مميزات الغرفه-تتميز هذه الغرفه بكل المواصفات الجيده تتميز هذه الغرفه بكل المواصفات الجيده تتميز هذه الغرفه بكل المواصفات الجيده تتميز هذه الغرفه بكل المواصفات الجيده</p>', '<p>مكونات الغرفه - تتكون الغرفه من تتكون الغرفه من العديد تتكون الغرفه من العديد تتكون الغرفه من العديد</p>', '', ''),
(60, 30, 1, 'Room number 3', 'room_number_3', '<p>Unit Facilities:-Twin Bedroom that accommodates two persons fully furnished connected to a terrace and equipped with satellite TV, Mini Bar and en-suite Luxury Bathroom with shower & tub.</p>', '<p>Unit Components:-Twin Bedroom that accommodates two persons fully furnished connected to a terrace and equipped with satellite TV, Mini Bar and en-suite Luxury Bathroom with shower & tub.</p>', '', ''),
(61, 31, 2, 'الغرفه رقم 2', 'الغرفه_رقم_2', '<p>مميزات الغرفه-تتميز هذه الغرفه بكل المواصفات الجيده تتميز هذه الغرفه بكل المواصفات الجيده تتميز هذه الغرفه بكل المواصفات الجيده تتميز هذه الغرفه بكل المواصفات الجيده</p>', '<p>مكونات الغرفه - تتكون الغرفه من تتكون الغرفه من العديد تتكون الغرفه من العديد تتكون الغرفه من العديد</p>', '', ''),
(62, 31, 1, 'room number 2', 'room_number_2', '<p>Unit Facilities:-Twin Bedroom that accommodates two persons fully furnished connected to a terrace and equipped with satellite TV, Mini Bar and en-suite Luxury Bathroom with shower &amp; tub.</p>', '<p>Unit Components:-Twin Bedroom that accommodates two persons fully furnished connected to a terrace and equipped with satellite TV, Mini Bar and en-suite Luxury Bathroom with shower &amp; tub.</p>', '', ''),
(63, 32, 2, 'معرض الصور', 'معرض_الصور', '', '', '', ''),
(64, 32, 1, 'Photo Gallery', 'photo_gallery', '', '', '', ''),
(65, 33, 2, 'معرض اختبار', 'معرض_اختبار', '', '', '', ''),
(66, 33, 1, 'Test Gallery', 'test_gallery', '', '', '', ''),
(67, 34, 2, 'معرض 2', 'معرض_2', '', '', '', ''),
(68, 34, 1, 'Gallery test 2', 'gallery_test_2', '', '', '', ''),
(69, 35, 2, 'جاليرى تست 3', 'جاليرى_تست_3', '', '', '', ''),
(70, 35, 1, 'Gallery Test 3', 'gallery_test_3', '', '', '', ''),
(71, 36, 2, 'جاليرى رابع', 'جاليرى_رابع', '', '', '', ''),
(72, 36, 1, 'Gallery test 4', 'gallery_test_4', '', '', '', ''),
(73, 37, 2, 'اختبار معرض كات مختلف', 'اختبار_معرض_كات_مختلف', '', '', '', ''),
(74, 37, 1, 'Test gallery different cat', 'test_gallery_different_cat', '', '', '', ''),
(75, 38, 2, 'جاليرى 3 ', 'جاليرى_3_', '', '', '', ''),
(76, 38, 1, 'test gallery cat 3', 'test_gallery_cat_3', '', '', '', ''),
(77, 39, 2, 'جاليرى 2 كات', 'جاليرى_2_كات', '', '', '', ''),
(78, 39, 1, 'test gallery with cat 2 again', 'test_gallery_with_cat_2_again', '', '', '', ''),
(79, 40, 2, 'معرض رابع', 'معرض_رابع', '', '', '', ''),
(80, 40, 1, 'Gallery 4', 'gallery_4', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_image_gallery`
--

CREATE TABLE `nodes_image_gallery` (
  `id` int(11) NOT NULL,
  `type` enum('node','propertie') NOT NULL,
  `related_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `sort` varchar(5) NOT NULL,
  `caption` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes_image_gallery`
--

INSERT INTO `nodes_image_gallery` (`id`, `type`, `related_id`, `image`, `sort`, `caption`) VALUES
(24, 'node', 22, 'about_folder/dummy-image.jpg', '1', 'San Francisco-USA'),
(25, 'node', 22, 'about_folder/hotel2.jpg', '2', 'Cairo-Egypt'),
(26, 'node', 22, 'about_folder/footer-col1.jpg', '3', 'Sharm Elsheikh-Hilton Ramses'),
(27, 'node', 22, 'about_folder/testo_img.jpg', '4', 'Cairo-Egypt'),
(28, 'node', 22, 'about_folder/slide.jpg', '5', 'Cairo-Egypt'),
(29, 'node', 22, 'about_folder/footer-col2.jpg', '6', 'Cairo-Egypt'),
(30, 'node', 22, 'about_folder/hotel.jpg', '7', 'Cairo-Egypt'),
(88, 'node', 36, 'dubai/full-room-view.jpg', '', ''),
(125, 'node', 39, 'sharm%20elshiekh/sharm4.jpg', '', ''),
(126, 'node', 39, 'sokhna/3841504_7_z.jpg', '', ''),
(131, 'node', 40, 'usa/msyrvr-omni-riverfront-hotel-deluxe-room.jpg', '', ''),
(132, 'node', 40, 'sokhna/hotel-hilton-old-town-praha-689366.jpg', '', ''),
(142, 'node', 18, 'spa_folder/hotel.jpg', '1', 'San Fransisco - USA'),
(143, 'node', 18, 'spa_folder/hotel2.jpg', '2', 'Cairo - EGYPT'),
(144, 'node', 18, 'spa_folder/slide.jpg', '', 'Sharm Elshiekh'),
(148, 'node', 33, 'sharm%20elshiekh/sharm2.jpg', '', ''),
(149, 'node', 33, 'sharm%20elshiekh/sharm3.jpg', '', ''),
(150, 'node', 33, 'sharm%20elshiekh/sharm4.jpg', '', ''),
(151, 'node', 34, 'sokhna/219-double-no-props1-e1380752836920.jpg', '', ''),
(152, 'node', 34, 'sokhna/hotel-hilton-old-town-praha-689366.jpg', '', ''),
(153, 'node', 34, 'sokhna/jumbotron-rooms.jpg', '', ''),
(154, 'node', 34, 'sharm%20elshiekh/sharm4.jpg', '', ''),
(155, 'node', 34, 'sharm%20elshiekh/sharm3.jpg', '', ''),
(158, 'node', 35, 'usa/full-room-view.jpg', '', ''),
(159, 'node', 35, 'usa/Green-Hotel-Room.jpg', '', ''),
(160, 'node', 37, 'usa/msyrvr-omni-riverfront-hotel-deluxe-room.jpg', '', ''),
(161, 'node', 37, 'sokhna/3841504_7_z.jpg', '', ''),
(162, 'node', 37, 'sharm%20elshiekh/sharm1.jpg', '', ''),
(163, 'node', 38, 'images/hotel.jpg', '', ''),
(164, 'node', 38, 'sokhna/3841504_7_z.jpg', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_plugins_values`
--

CREATE TABLE `nodes_plugins_values` (
  `id` int(11) NOT NULL,
  `type` enum('post','page','event') NOT NULL,
  `title` varchar(250) NOT NULL,
  `node_id` int(11) NOT NULL,
  `plugin_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes_plugins_values`
--

INSERT INTO `nodes_plugins_values` (`id`, `type`, `title`, `node_id`, `plugin_id`, `lang_id`, `content`) VALUES
(1, 'post', 'test', 10, 44, 1, '5'),
(2, 'post', 'test', 10, 44, 2, '4'),
(3, 'post', 'Latest News', 10, 46, 1, '2'),
(4, 'post', 'Ø£Ø­Ø¯Ø« Ø§Ù„Ø£Ø®Ø¨Ø§Ø±', 10, 46, 2, '2'),
(5, 'post', '', 10, 45, 1, ''),
(6, 'post', '', 10, 45, 2, ''),
(7, 'post', 'test', 2, 44, 1, '1'),
(8, 'post', '', 2, 44, 2, '0'),
(9, 'post', 'rtyret', 2, 46, 1, '2'),
(10, 'post', 'tete', 2, 46, 2, '2'),
(11, 'post', '', 2, 45, 1, ''),
(12, 'post', '', 2, 45, 2, ''),
(13, 'post', 'Upcoming Events', 2, 47, 1, '12'),
(14, 'post', 'Ø§Ù„Ø£Ø­Ø¯Ø§Ø« Ø§Ù„Ù‚Ø§Ø¯Ù…Ø©', 2, 47, 2, '12'),
(15, 'event', 'Social Media', 11, 44, 1, '5'),
(16, 'event', '', 11, 44, 2, '0'),
(17, 'event', 'Upcoming Events', 11, 47, 1, '12'),
(18, 'event', '', 11, 47, 2, '0'),
(19, 'page', 'upcoming events ', 13, 47, 1, '12'),
(20, 'page', '', 13, 47, 2, '0'),
(21, 'page', 'Latest News', 13, 46, 1, '2'),
(22, 'page', '', 13, 46, 2, '0'),
(23, 'page', 'Important lINKS ', 13, 44, 1, '1'),
(24, 'page', '', 13, 44, 2, '0'),
(25, 'post', '', 10, 47, 1, ''),
(26, 'post', '', 10, 47, 2, ''),
(27, 'post', '', 1, 44, 1, ''),
(28, 'post', '', 1, 44, 2, ''),
(29, 'post', '', 1, 45, 1, ''),
(30, 'post', '', 1, 45, 2, ''),
(31, 'post', '', 1, 46, 1, ''),
(32, 'post', '', 1, 46, 2, ''),
(33, 'post', '', 1, 47, 1, ''),
(34, 'post', '', 1, 47, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_selected_taxonomies`
--

CREATE TABLE `nodes_selected_taxonomies` (
  `id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `taxonomy_type` enum('tag','category','author','country') NOT NULL,
  `node_type` enum('post','page','event','faq','product') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes_selected_taxonomies`
--

INSERT INTO `nodes_selected_taxonomies` (`id`, `taxonomy_id`, `node_id`, `taxonomy_type`, `node_type`) VALUES
(1, 1, 6, 'category', 'post'),
(2, 2, 7, 'category', 'post'),
(3, 2, 8, 'category', 'post'),
(4, 3, 9, 'category', 'post'),
(5, 4, 11, 'category', 'post'),
(6, 4, 12, 'category', 'post'),
(11, 2, 23, 'category', 'page'),
(12, 3, 23, 'category', 'page'),
(13, 4, 27, 'category', 'page'),
(14, 4, 18, 'category', 'page'),
(15, 5, 17, 'category', 'page'),
(16, 5, 28, 'category', 'post'),
(17, 5, 29, 'category', 'page'),
(18, 5, 30, 'category', 'post'),
(19, 5, 31, 'category', 'post'),
(25, 6, 33, 'category', 'post'),
(27, 6, 34, 'category', 'post'),
(28, 11, 32, 'category', 'page'),
(29, 6, 32, 'category', 'page'),
(30, 7, 32, 'category', 'page'),
(31, 8, 32, 'category', 'page'),
(32, 10, 32, 'category', 'page'),
(33, 11, 35, 'category', 'post'),
(34, 6, 35, 'category', 'post'),
(35, 11, 36, 'category', 'post'),
(36, 6, 36, 'category', 'post'),
(37, 11, 37, 'category', 'post'),
(38, 7, 37, 'category', 'post'),
(39, 11, 38, 'category', 'post'),
(41, 11, 39, 'category', 'post'),
(42, 7, 39, 'category', 'post'),
(43, 7, 38, 'category', 'post'),
(44, 11, 40, 'category', 'post'),
(45, 7, 40, 'category', 'post'),
(46, 11, 33, 'category', 'post'),
(47, 12, 33, 'category', 'post'),
(48, 11, 34, 'category', 'post'),
(49, 16, 34, 'category', 'post'),
(51, 17, 35, 'category', 'post'),
(52, 18, 36, 'category', 'post'),
(53, 19, 37, 'category', 'post'),
(54, 20, 38, 'category', 'post');

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE `plugins` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `version` varchar(50) NOT NULL,
  `author` varchar(150) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `source` varchar(150) NOT NULL,
  `enable_options` enum('yes','no') NOT NULL,
  `enable_translate` enum('yes','no') NOT NULL,
  `enable_event` enum('no','yes') NOT NULL,
  `enable_post` enum('no','yes') NOT NULL,
  `enable_page` enum('no','yes') NOT NULL,
  `enable_index` enum('no','yes') NOT NULL,
  `position` enum('left','right','no_position') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `name`, `description`, `version`, `author`, `uploaded_date`, `uploaded_by`, `source`, `enable_options`, `enable_translate`, `enable_event`, `enable_post`, `enable_page`, `enable_index`, `position`) VALUES
(1, 'Show Page details', 'this plug to Show Page details', '1.0', 'diva', '2015-07-29 20:21:57', 1, 'show_page_details', '', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(3, 'Show contact us page details', 'this plug to Show contact us Page details', '1.0', 'diva', '2015-07-31 13:16:26', 1, 'contact_us_page', '', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(7, 'Show post details', 'this plug to list to Show post details', '1.0', 'diva', '2015-08-12 15:24:19', 1, 'show_post_details', '', 'yes', 'no', 'yes', 'no', 'no', 'left'),
(34, 'Show event details', 'this plug to list to Show event details', '1.0', 'diva', '2016-01-24 17:07:36', 1, 'show_event_details', '', 'yes', 'yes', 'no', 'no', 'no', 'left'),
(35, 'posts list for page', 'posts_list_for_page', '1.0', 'diva', '2016-05-18 00:00:00', 1, 'posts_list_for_page', 'no', 'no', 'no', 'no', 'yes', 'no', 'left'),
(36, 'list events in brands page', 'this plug to list all brands in page', '1.0', 'diva', '2016-05-19 13:50:56', 1, 'page_list_brands', '', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(38, 'Home page latest database', 'this plug to list all database in home page', '1.0', 'diva', '2016-06-05 13:07:27', 1, 'home_page_latest_data_base', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(39, 'Home page latest database resourses', 'this plug to list all database  resourses in home page', '1.0', 'diva', '2016-06-05 13:07:33', 1, 'home_page_latest_data_base_resourses', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(40, 'Home page latest Courses', 'this plug to list all Courses in home page', '1.0', 'diva', '2016-06-05 13:07:41', 1, 'home_page_upcoming_courses', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(41, 'Sidebare latest news', 'this plug to latest news in side bar', '1.0', 'diva', '2016-06-05 13:07:55', 1, 'side_bar_latest_news', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(42, 'Sidebare menu links', 'this plug to sidebar menu links in sidebar', '1.0', 'diva', '2016-06-05 13:08:02', 1, 'side_bar_menu_links', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(43, 'sidebar  upcoming events', 'this plug to list all sidebar  upcoming events in side bar', '1.0', 'diva', '2016-06-05 13:08:11', 1, 'side_bar_upcoming_events', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(44, 'sidebar menu link in internal page', 'this plugin to view menu link in sidebar bin internal page', '1.0', 'diva', '2016-06-07 09:17:43', 1, 'sidebar_menu_link_internal', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'right'),
(45, 'sidebar text editor', 'this plug-in used to add free text in sidebar', '1.0', 'diva', '2016-06-07 09:50:35', 1, 'sidebar_texteditor', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'right'),
(46, 'sidebar menu link in post by category page', 'this plugin to view menu link in sidebar bin internal post by category page', '1.0', 'diva', '2016-06-07 09:58:56', 1, 'side_bar_latest_posts_internal', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'right'),
(47, 'sidebar Upcoming events', 'this plugin to view Upcoming events', '1.0', 'diva', '2016-06-07 11:00:00', 1, 'side_bar_upcoming_events_internal', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'right'),
(48, 'list view events', 'this plug to list to view all events', '1.0', 'diva', '2016-06-07 12:12:33', 1, 'events_list_for_page', '', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(49, 'parsing', 'this plug is to parse xml ', '1.0', '1', '2016-08-29 00:00:00', 1, 'parsing', '', '', '', '', 'yes', '', 'left'),
(50, 'membership', 'this plugin to show membership inquiry form', '1.0', 'diva', '2016-09-18 00:00:00', 1, 'membership', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(51, 'offers plugin', 'this plug to show all posts for offers categories \r\n', '1.0', 'diva', '2016-09-19 00:00:00', 1, 'offers', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(52, 'survey plugin', 'this plug to show survey  form  ', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'survey', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(53, 'testimonials plugin', 'this plug to list all testimonials under this page', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'testimonials', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(54, 'about sharm plugin', 'this plugin to show about plugin with gallery', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'about', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(55, 'page_gallery', 'this plug to insert photo gallery inside page ', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'page_gallery', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(56, 'about_part_2', 'test_part 2 of about ', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'about_part_2', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(57, 'spa_gallery', 'this plugin to insert gallery with short code inside spa page  ', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'spa_gallery', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(58, 'testimonials_for_spa', 'list three testimonials', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'tetimonials_for_spa', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(59, 'rooms plugin', 'to get posts for rooms inside rooms page', '1.0', 'diva', '2016-09-21 00:00:00', 1, 'rooms', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(60, 'gallery_page plugin', 'to list all galleries inside this page and filtrate by category ', '1.0', 'diva', '2016-09-21 00:00:00', 1, 'gallery_page', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left');

-- --------------------------------------------------------

--
-- Table structure for table `poll_questions`
--

CREATE TABLE `poll_questions` (
  `id` int(11) NOT NULL,
  `poll` varchar(250) NOT NULL,
  `status` enum('publish','draft') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `poll_questions_options`
--

CREATE TABLE `poll_questions_options` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `poll_option` varchar(250) NOT NULL,
  `option_counter` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `title` varchar(25) NOT NULL,
  `description` varchar(150) NOT NULL,
  `global_edit` enum('all_records','awn_record') NOT NULL,
  `global_delete` enum('all_records','awn_record') NOT NULL,
  `developer_mode` enum('yes','no') NOT NULL,
  `profile_block` enum('yes','no') NOT NULL,
  `post_publishing` enum('yes','no') NOT NULL,
  `page_publishing` enum('yes','no') NOT NULL,
  `event_publishing` enum('yes','no') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `title`, `description`, `global_edit`, `global_delete`, `developer_mode`, `profile_block`, `post_publishing`, `page_publishing`, `event_publishing`, `inserted_by`, `inserted_date`, `last_update`) VALUES
(1, 'access all', '', 'all_records', 'all_records', 'yes', 'no', 'yes', 'yes', 'yes', 1, '2014-05-04 10:42:08', '2016-08-23 16:33:51'),
(9, 'new admin', '', 'all_records', 'awn_record', 'no', 'yes', 'yes', 'yes', 'yes', 1, '2016-02-03 18:40:32', '2016-08-23 17:41:32'),
(10, 'admin', '', 'all_records', 'all_records', 'no', 'no', 'yes', 'yes', 'yes', 1, '2016-02-24 08:34:23', '2016-08-23 16:57:42'),
(11, 'Super Admin', '', 'awn_record', 'awn_record', 'no', 'no', 'no', 'no', 'no', 1, '2016-02-29 11:40:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profile_modules_access`
--

CREATE TABLE `profile_modules_access` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `access` enum('yes','no') NOT NULL,
  `profile_id` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_modules_access`
--

INSERT INTO `profile_modules_access` (`id`, `module_id`, `access`, `profile_id`, `inserted_by`, `inserted_date`) VALUES
(1, 5, 'yes', 1, 1, '2014-05-04 10:42:08'),
(2, 6, 'yes', 1, 1, '2014-05-04 10:42:08'),
(3, 7, 'yes', 1, 1, '2014-05-04 10:42:09'),
(6, 10, 'yes', 1, 1, '2014-05-04 10:42:09'),
(7, 11, 'yes', 1, 1, '2014-05-04 10:42:09'),
(8, 12, 'yes', 1, 1, '2014-05-04 10:42:09'),
(9, 33, 'yes', 1, 1, '2014-05-10 08:51:36'),
(33, 275, 'yes', 1, 1, '2015-07-01 05:16:22'),
(107, 6, 'yes', 9, 1, '2016-02-03 18:40:32'),
(108, 5, 'yes', 9, 1, '2016-02-03 18:40:32'),
(109, 7, 'yes', 9, 1, '2016-02-03 18:40:32'),
(112, 10, 'yes', 9, 1, '2016-02-03 18:40:32'),
(113, 33, 'no', 9, 1, '2016-02-03 18:40:32'),
(114, 11, 'no', 9, 1, '2016-02-03 18:40:32'),
(115, 12, 'no', 9, 1, '2016-02-03 18:40:32'),
(116, 275, 'no', 9, 1, '2016-02-03 18:40:32'),
(117, 6, 'yes', 10, 1, '2016-02-24 08:34:23'),
(118, 5, 'yes', 10, 1, '2016-02-24 08:34:23'),
(119, 7, 'yes', 10, 1, '2016-02-24 08:34:23'),
(122, 10, 'yes', 10, 1, '2016-02-24 08:34:23'),
(123, 33, 'no', 10, 1, '2016-02-24 08:34:23'),
(124, 11, 'yes', 10, 1, '2016-02-24 08:34:23'),
(125, 12, 'no', 10, 1, '2016-02-24 08:34:23'),
(126, 275, 'no', 10, 1, '2016-02-24 08:34:23'),
(127, 6, 'yes', 11, 1, '2016-02-29 11:40:27'),
(128, 5, 'yes', 11, 1, '2016-02-29 11:40:27'),
(129, 7, 'yes', 11, 1, '2016-02-29 11:40:27'),
(132, 10, 'yes', 11, 1, '2016-02-29 11:40:27'),
(133, 33, 'yes', 11, 1, '2016-02-29 11:40:27'),
(134, 11, 'yes', 11, 1, '2016-02-29 11:40:27'),
(135, 12, 'yes', 11, 1, '2016-02-29 11:40:27'),
(136, 275, 'yes', 11, 1, '2016-02-29 11:40:27');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pages_access`
--

CREATE TABLE `profile_pages_access` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `access` enum('yes','no') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_pages_access`
--

INSERT INTO `profile_pages_access` (`id`, `profile_id`, `module_id`, `page_id`, `access`, `inserted_by`, `inserted_date`) VALUES
(1, 1, 5, 13, 'yes', 1, '2014-05-04 10:42:09'),
(3, 1, 6, 16, 'yes', 1, '2014-05-04 10:42:09'),
(4, 1, 6, 17, 'yes', 1, '2014-05-04 10:42:09'),
(5, 1, 7, 29, 'yes', 1, '2014-05-04 10:42:09'),
(6, 1, 7, 30, 'yes', 1, '2014-05-04 10:42:09'),
(11, 1, 10, 22, 'yes', 1, '2014-05-04 10:42:09'),
(12, 1, 10, 23, 'yes', 1, '2014-05-04 10:42:09'),
(14, 1, 11, 25, 'yes', 1, '2014-05-04 10:42:09'),
(15, 1, 12, 26, 'yes', 1, '2014-05-04 10:42:09'),
(16, 1, 12, 27, 'yes', 1, '2014-05-04 10:42:09'),
(17, 1, 12, 28, 'yes', 1, '2014-05-04 10:42:09'),
(18, 1, 11, 31, 'yes', 1, '2014-05-04 10:42:09'),
(20, 1, 33, 34, 'yes', 1, '2014-05-10 08:56:40'),
(21, 1, 33, 35, 'yes', 1, '2014-05-10 08:57:56'),
(22, 1, 33, 36, 'yes', 1, '2014-05-10 08:58:46'),
(23, 1, 33, 37, 'yes', 1, '2014-05-10 02:54:51'),
(24, 1, 33, 38, 'no', 1, '2014-05-11 08:38:53'),
(49, 1, 12, 39, 'yes', 1, '2014-05-31 09:45:31'),
(51, 1, 275, 40, 'yes', 1, '2014-06-23 07:44:38'),
(53, 1, 275, 42, 'yes', 1, '2014-06-23 08:04:35'),
(237, 1, 12, 43, 'yes', 1, '2014-07-19 15:06:57'),
(238, 1, 12, 44, 'yes', 1, '2014-07-19 15:24:29'),
(239, 1, 12, 45, 'yes', 1, '2014-07-19 15:27:29'),
(241, 1, 12, 47, 'yes', 1, '2014-07-19 15:43:14'),
(242, 1, 12, 48, 'yes', 1, '2014-07-19 15:46:43'),
(244, 1, 12, 50, 'yes', 1, '2014-07-19 15:55:50'),
(246, 1, 12, 52, 'yes', 1, '2014-07-19 15:59:55'),
(247, 1, 12, 53, 'yes', 1, '2014-07-19 16:03:18'),
(248, 1, 12, 54, 'yes', 1, '2014-07-19 16:07:07'),
(250, 1, 11, 56, 'yes', 1, '2014-07-19 16:12:43'),
(258, 1, 10, 64, 'yes', 1, '2014-07-19 16:24:05'),
(259, 1, 10, 65, 'yes', 1, '2014-07-19 16:25:03'),
(264, 1, 6, 70, 'no', 1, '2014-07-19 16:31:33'),
(265, 1, 6, 71, 'yes', 1, '2014-07-19 16:32:05'),
(266, 1, 7, 72, 'yes', 1, '2014-07-19 16:33:37'),
(267, 1, 7, 73, 'yes', 1, '2014-07-19 16:33:54'),
(268, 1, 33, 74, 'yes', 1, '2014-07-19 16:36:05'),
(271, 1, 33, 77, 'yes', 1, '2014-07-19 16:47:13'),
(272, 1, 33, 78, 'yes', 1, '2014-07-19 16:47:49'),
(273, 1, 33, 79, 'yes', 1, '2014-07-19 16:48:28'),
(274, 1, 33, 80, 'yes', 1, '2014-07-19 16:51:47'),
(275, 1, 33, 81, 'yes', 1, '2014-07-19 16:53:27'),
(277, 1, 33, 83, 'yes', 1, '2014-07-19 16:56:50'),
(278, 1, 33, 84, 'yes', 1, '2014-07-19 16:58:58'),
(280, 1, 33, 86, 'yes', 1, '2014-07-19 17:09:19'),
(281, 1, 33, 87, 'no', 1, '2014-07-19 17:12:40'),
(285, 1, 6, 91, 'yes', 1, '2014-07-19 17:22:38'),
(286, 1, 7, 92, 'yes', 1, '2014-07-19 17:23:39'),
(287, 1, 10, 93, 'yes', 1, '2014-07-19 17:24:46'),
(289, 1, 11, 95, 'yes', 1, '2014-07-19 17:28:12'),
(290, 1, 11, 96, 'yes', 1, '2014-07-19 17:29:24'),
(291, 1, 12, 97, 'yes', 1, '2014-07-20 13:23:07'),
(292, 1, 7, 98, 'yes', 1, '2014-07-20 14:07:32'),
(293, 1, 7, 99, 'yes', 1, '2014-07-20 14:08:28'),
(295, 1, 7, 101, 'yes', 1, '2014-07-20 14:12:07'),
(296, 1, 7, 102, 'yes', 1, '2014-07-20 14:13:13'),
(297, 1, 7, 103, 'yes', 1, '2014-07-20 14:18:52'),
(301, 1, 12, 107, 'yes', 1, '2014-07-23 14:43:37'),
(302, 1, 12, 108, 'yes', 1, '2014-07-24 02:03:06'),
(303, 1, 33, 109, 'no', 1, '2014-07-27 19:28:05'),
(304, 1, 33, 110, 'yes', 1, '2014-07-27 19:43:43'),
(305, 1, 33, 111, 'yes', 1, '2014-07-27 20:19:35'),
(306, 1, 33, 112, 'yes', 1, '2014-07-31 10:45:13'),
(307, 1, 11, 113, 'yes', 1, '2014-07-31 10:47:42'),
(308, 1, 11, 114, 'yes', 1, '2014-07-31 10:49:27'),
(310, 1, 12, 116, 'no', 1, '2014-08-02 14:27:33'),
(311, 1, 12, 117, 'yes', 1, '2014-08-02 14:28:00'),
(312, 1, 12, 118, 'yes', 1, '2014-08-02 16:53:09'),
(313, 1, 5, 119, 'yes', 1, '2014-08-07 07:25:56'),
(314, 1, 5, 120, 'yes', 1, '2014-08-07 07:52:13'),
(315, 1, 5, 121, 'yes', 1, '2014-08-07 10:25:41'),
(316, 1, 12, 122, 'yes', 1, '2014-08-10 07:55:26'),
(321, 1, 10, 127, 'yes', 1, '2014-08-17 10:31:22'),
(323, 1, 7, 129, 'yes', 1, '2014-08-18 11:46:56'),
(325, 1, 10, 131, 'yes', 1, '2014-08-23 16:33:24'),
(326, 1, 33, 132, 'yes', 1, '2014-09-17 11:04:43'),
(327, 1, 33, 133, 'yes', 1, '2014-09-17 11:06:57'),
(328, 1, 33, 134, 'yes', 1, '2014-09-17 11:07:19'),
(329, 1, 33, 135, 'yes', 1, '2014-09-17 11:07:50'),
(330, 1, 33, 136, 'yes', 1, '2014-09-17 11:08:18'),
(331, 1, 33, 137, 'yes', 1, '2014-09-17 11:10:05'),
(332, 1, 33, 138, 'yes', 1, '2014-09-17 11:10:25'),
(333, 1, 33, 139, 'yes', 1, '2014-09-17 11:10:55'),
(334, 1, 33, 140, 'yes', 1, '2014-09-17 11:11:19'),
(335, 1, 33, 141, 'yes', 1, '2014-09-17 11:11:46'),
(336, 1, 33, 142, 'yes', 1, '2014-09-17 11:46:09'),
(337, 1, 33, 143, 'yes', 1, '2014-09-17 11:46:49'),
(338, 1, 33, 144, 'yes', 1, '2014-09-17 11:47:16'),
(339, 1, 33, 145, 'yes', 1, '2014-09-17 11:47:41'),
(340, 1, 33, 146, 'yes', 1, '2014-09-17 11:47:59'),
(341, 1, 33, 149, 'yes', 1, '2014-09-20 02:09:15'),
(342, 1, 33, 150, 'yes', 1, '2014-09-20 02:10:19'),
(343, 1, 33, 152, 'yes', 1, '2014-09-20 11:12:36'),
(344, 1, 33, 153, 'yes', 1, '2014-09-20 17:12:38'),
(345, 1, 5, 154, 'yes', 1, '2014-09-21 00:01:05'),
(540, 1, 12, 229, 'yes', 1, '2015-03-30 08:00:31'),
(546, 1, 12, 235, 'yes', 1, '2015-03-31 14:02:26'),
(548, 1, 12, 237, 'yes', 1, '2015-03-31 14:04:06'),
(549, 1, 12, 238, 'yes', 1, '2015-03-31 14:06:46'),
(565, 1, 12, 254, 'yes', 1, '2015-04-06 18:30:02'),
(566, 1, 12, 255, 'yes', 1, '2015-04-06 18:30:32'),
(567, 1, 12, 256, 'yes', 1, '2015-04-06 18:30:57'),
(568, 1, 12, 257, 'yes', 1, '2015-04-06 18:31:36'),
(569, 1, 12, 258, 'yes', 1, '2015-04-06 18:32:04'),
(784, 1, 33, 267, 'yes', 1, '2015-05-24 16:14:17'),
(786, 1, 33, 268, 'yes', 1, '2015-05-24 16:14:45'),
(2401, 9, 6, 16, 'yes', 1, '2016-02-03 18:40:32'),
(2402, 9, 6, 17, 'yes', 1, '2016-02-03 18:40:32'),
(2403, 9, 6, 70, 'yes', 1, '2016-02-03 18:40:32'),
(2404, 9, 6, 91, 'yes', 1, '2016-02-03 18:40:32'),
(2405, 9, 6, 71, 'yes', 1, '2016-02-03 18:40:32'),
(2406, 9, 5, 13, 'yes', 1, '2016-02-03 18:40:32'),
(2407, 9, 5, 119, 'yes', 1, '2016-02-03 18:40:32'),
(2408, 9, 5, 154, 'yes', 1, '2016-02-03 18:40:32'),
(2409, 9, 5, 120, 'yes', 1, '2016-02-03 18:40:32'),
(2410, 9, 5, 121, 'yes', 1, '2016-02-03 18:40:32'),
(2411, 9, 7, 29, 'yes', 1, '2016-02-03 18:40:32'),
(2412, 9, 7, 30, 'yes', 1, '2016-02-03 18:40:32'),
(2413, 9, 7, 72, 'yes', 1, '2016-02-03 18:40:32'),
(2414, 9, 7, 92, 'yes', 1, '2016-02-03 18:40:32'),
(2415, 9, 7, 73, 'yes', 1, '2016-02-03 18:40:32'),
(2416, 9, 7, 98, 'yes', 1, '2016-02-03 18:40:32'),
(2417, 9, 7, 99, 'yes', 1, '2016-02-03 18:40:32'),
(2418, 9, 7, 101, 'yes', 1, '2016-02-03 18:40:32'),
(2419, 9, 7, 103, 'yes', 1, '2016-02-03 18:40:32'),
(2420, 9, 7, 102, 'yes', 1, '2016-02-03 18:40:32'),
(2421, 9, 7, 129, 'yes', 1, '2016-02-03 18:40:32'),
(2436, 9, 10, 22, 'yes', 1, '2016-02-03 18:40:32'),
(2437, 9, 10, 23, 'yes', 1, '2016-02-03 18:40:32'),
(2438, 9, 10, 64, 'yes', 1, '2016-02-03 18:40:32'),
(2439, 9, 10, 127, 'yes', 1, '2016-02-03 18:40:32'),
(2440, 9, 10, 93, 'yes', 1, '2016-02-03 18:40:32'),
(2441, 9, 10, 65, 'yes', 1, '2016-02-03 18:40:32'),
(2442, 9, 10, 131, 'yes', 1, '2016-02-03 18:40:32'),
(2443, 9, 33, 34, 'no', 1, '2016-02-03 18:40:32'),
(2444, 9, 33, 74, 'no', 1, '2016-02-03 18:40:32'),
(2445, 9, 33, 132, 'no', 1, '2016-02-03 18:40:32'),
(2446, 9, 33, 110, 'no', 1, '2016-02-03 18:40:32'),
(2447, 9, 33, 112, 'no', 1, '2016-02-03 18:40:32'),
(2448, 9, 33, 152, 'no', 1, '2016-02-03 18:40:32'),
(2449, 9, 33, 35, 'no', 1, '2016-02-03 18:40:32'),
(2450, 9, 33, 153, 'no', 1, '2016-02-03 18:40:32'),
(2451, 9, 33, 77, 'no', 1, '2016-02-03 18:40:32'),
(2452, 9, 33, 78, 'no', 1, '2016-02-03 18:40:32'),
(2453, 9, 33, 79, 'no', 1, '2016-02-03 18:40:32'),
(2454, 9, 33, 80, 'no', 1, '2016-02-03 18:40:32'),
(2455, 9, 33, 36, 'no', 1, '2016-02-03 18:40:32'),
(2456, 9, 33, 81, 'no', 1, '2016-02-03 18:40:32'),
(2457, 9, 33, 83, 'no', 1, '2016-02-03 18:40:32'),
(2458, 9, 33, 84, 'no', 1, '2016-02-03 18:40:32'),
(2459, 9, 33, 86, 'no', 1, '2016-02-03 18:40:32'),
(2460, 9, 33, 37, 'no', 1, '2016-02-03 18:40:32'),
(2461, 9, 33, 111, 'no', 1, '2016-02-03 18:40:32'),
(2462, 9, 33, 38, 'no', 1, '2016-02-03 18:40:32'),
(2463, 9, 33, 109, 'no', 1, '2016-02-03 18:40:32'),
(2464, 9, 33, 87, 'no', 1, '2016-02-03 18:40:32'),
(2465, 9, 33, 133, 'no', 1, '2016-02-03 18:40:32'),
(2466, 9, 33, 134, 'no', 1, '2016-02-03 18:40:32'),
(2467, 9, 33, 135, 'no', 1, '2016-02-03 18:40:32'),
(2468, 9, 33, 136, 'no', 1, '2016-02-03 18:40:32'),
(2469, 9, 33, 137, 'no', 1, '2016-02-03 18:40:32'),
(2470, 9, 33, 138, 'no', 1, '2016-02-03 18:40:32'),
(2471, 9, 33, 139, 'no', 1, '2016-02-03 18:40:32'),
(2472, 9, 33, 140, 'no', 1, '2016-02-03 18:40:32'),
(2473, 9, 33, 141, 'no', 1, '2016-02-03 18:40:32'),
(2474, 9, 33, 142, 'no', 1, '2016-02-03 18:40:32'),
(2475, 9, 33, 143, 'no', 1, '2016-02-03 18:40:32'),
(2476, 9, 33, 144, 'no', 1, '2016-02-03 18:40:32'),
(2477, 9, 33, 145, 'no', 1, '2016-02-03 18:40:32'),
(2478, 9, 33, 146, 'no', 1, '2016-02-03 18:40:32'),
(2479, 9, 33, 149, 'no', 1, '2016-02-03 18:40:32'),
(2480, 9, 33, 150, 'no', 1, '2016-02-03 18:40:32'),
(2481, 9, 33, 151, 'no', 1, '2016-02-03 18:40:32'),
(2482, 9, 33, 267, 'no', 1, '2016-02-03 18:40:32'),
(2483, 9, 33, 268, 'no', 1, '2016-02-03 18:40:32'),
(2486, 9, 11, 56, 'no', 1, '2016-02-03 18:40:32'),
(2488, 9, 11, 113, 'no', 1, '2016-02-03 18:40:32'),
(2489, 9, 11, 25, 'no', 1, '2016-02-03 18:40:32'),
(2492, 9, 11, 95, 'no', 1, '2016-02-03 18:40:32'),
(2493, 9, 11, 114, 'no', 1, '2016-02-03 18:40:32'),
(2494, 9, 11, 31, 'no', 1, '2016-02-03 18:40:32'),
(2497, 9, 11, 96, 'no', 1, '2016-02-03 18:40:32'),
(2503, 9, 12, 43, 'no', 1, '2016-02-03 18:40:32'),
(2504, 9, 12, 39, 'no', 1, '2016-02-03 18:40:32'),
(2505, 9, 12, 26, 'no', 1, '2016-02-03 18:40:32'),
(2506, 9, 12, 44, 'no', 1, '2016-02-03 18:40:32'),
(2507, 9, 12, 45, 'no', 1, '2016-02-03 18:40:32'),
(2508, 9, 12, 97, 'no', 1, '2016-02-03 18:40:32'),
(2509, 9, 12, 108, 'no', 1, '2016-02-03 18:40:32'),
(2513, 9, 12, 229, 'no', 1, '2016-02-03 18:40:32'),
(2514, 9, 12, 47, 'no', 1, '2016-02-03 18:40:32'),
(2515, 9, 12, 48, 'no', 1, '2016-02-03 18:40:32'),
(2517, 9, 12, 235, 'no', 1, '2016-02-03 18:40:32'),
(2518, 9, 12, 237, 'no', 1, '2016-02-03 18:40:32'),
(2519, 9, 12, 238, 'no', 1, '2016-02-03 18:40:32'),
(2520, 9, 12, 118, 'no', 1, '2016-02-03 18:40:32'),
(2521, 9, 12, 28, 'no', 1, '2016-02-03 18:40:32'),
(2522, 9, 12, 116, 'no', 1, '2016-02-03 18:40:32'),
(2523, 9, 12, 117, 'no', 1, '2016-02-03 18:40:32'),
(2524, 9, 12, 27, 'no', 1, '2016-02-03 18:40:32'),
(2525, 9, 12, 50, 'no', 1, '2016-02-03 18:40:32'),
(2526, 9, 12, 52, 'no', 1, '2016-02-03 18:40:32'),
(2527, 9, 12, 53, 'no', 1, '2016-02-03 18:40:32'),
(2528, 9, 12, 54, 'no', 1, '2016-02-03 18:40:32'),
(2529, 9, 12, 122, 'no', 1, '2016-02-03 18:40:32'),
(2530, 9, 12, 107, 'no', 1, '2016-02-03 18:40:32'),
(2533, 9, 12, 254, 'no', 1, '2016-02-03 18:40:32'),
(2534, 9, 12, 256, 'no', 1, '2016-02-03 18:40:32'),
(2535, 9, 12, 257, 'no', 1, '2016-02-03 18:40:32'),
(2536, 9, 12, 258, 'no', 1, '2016-02-03 18:40:32'),
(2537, 9, 12, 255, 'no', 1, '2016-02-03 18:40:32'),
(2538, 9, 275, 40, 'no', 1, '2016-02-03 18:40:32'),
(2539, 9, 275, 42, 'no', 1, '2016-02-03 18:40:32'),
(2540, 10, 6, 16, 'yes', 1, '2016-02-24 08:34:23'),
(2541, 10, 6, 17, 'yes', 1, '2016-02-24 08:34:23'),
(2542, 10, 6, 70, 'yes', 1, '2016-02-24 08:34:23'),
(2543, 10, 6, 91, 'yes', 1, '2016-02-24 08:34:23'),
(2544, 10, 6, 71, 'yes', 1, '2016-02-24 08:34:23'),
(2545, 10, 5, 13, 'yes', 1, '2016-02-24 08:34:23'),
(2546, 10, 5, 119, 'yes', 1, '2016-02-24 08:34:23'),
(2547, 10, 5, 154, 'yes', 1, '2016-02-24 08:34:23'),
(2548, 10, 5, 120, 'yes', 1, '2016-02-24 08:34:23'),
(2549, 10, 5, 121, 'yes', 1, '2016-02-24 08:34:23'),
(2550, 10, 7, 29, 'yes', 1, '2016-02-24 08:34:23'),
(2551, 10, 7, 30, 'yes', 1, '2016-02-24 08:34:23'),
(2552, 10, 7, 72, 'yes', 1, '2016-02-24 08:34:23'),
(2553, 10, 7, 92, 'yes', 1, '2016-02-24 08:34:23'),
(2554, 10, 7, 73, 'yes', 1, '2016-02-24 08:34:23'),
(2555, 10, 7, 98, 'yes', 1, '2016-02-24 08:34:23'),
(2556, 10, 7, 99, 'yes', 1, '2016-02-24 08:34:23'),
(2557, 10, 7, 101, 'yes', 1, '2016-02-24 08:34:23'),
(2558, 10, 7, 103, 'yes', 1, '2016-02-24 08:34:23'),
(2559, 10, 7, 102, 'yes', 1, '2016-02-24 08:34:23'),
(2560, 10, 7, 129, 'yes', 1, '2016-02-24 08:34:23'),
(2575, 10, 10, 22, 'yes', 1, '2016-02-24 08:34:23'),
(2576, 10, 10, 23, 'yes', 1, '2016-02-24 08:34:23'),
(2577, 10, 10, 64, 'yes', 1, '2016-02-24 08:34:23'),
(2578, 10, 10, 127, 'yes', 1, '2016-02-24 08:34:23'),
(2579, 10, 10, 93, 'yes', 1, '2016-02-24 08:34:23'),
(2580, 10, 10, 65, 'yes', 1, '2016-02-24 08:34:23'),
(2581, 10, 10, 131, 'yes', 1, '2016-02-24 08:34:23'),
(2582, 10, 33, 34, 'no', 1, '2016-02-24 08:34:23'),
(2583, 10, 33, 74, 'no', 1, '2016-02-24 08:34:23'),
(2584, 10, 33, 132, 'no', 1, '2016-02-24 08:34:23'),
(2585, 10, 33, 110, 'no', 1, '2016-02-24 08:34:23'),
(2586, 10, 33, 112, 'no', 1, '2016-02-24 08:34:23'),
(2587, 10, 33, 152, 'no', 1, '2016-02-24 08:34:23'),
(2588, 10, 33, 35, 'no', 1, '2016-02-24 08:34:23'),
(2589, 10, 33, 153, 'no', 1, '2016-02-24 08:34:23'),
(2590, 10, 33, 77, 'no', 1, '2016-02-24 08:34:23'),
(2591, 10, 33, 78, 'no', 1, '2016-02-24 08:34:23'),
(2592, 10, 33, 79, 'no', 1, '2016-02-24 08:34:23'),
(2593, 10, 33, 80, 'no', 1, '2016-02-24 08:34:23'),
(2594, 10, 33, 36, 'no', 1, '2016-02-24 08:34:23'),
(2595, 10, 33, 81, 'no', 1, '2016-02-24 08:34:23'),
(2596, 10, 33, 83, 'no', 1, '2016-02-24 08:34:23'),
(2597, 10, 33, 84, 'no', 1, '2016-02-24 08:34:23'),
(2598, 10, 33, 86, 'no', 1, '2016-02-24 08:34:23'),
(2599, 10, 33, 37, 'no', 1, '2016-02-24 08:34:23'),
(2600, 10, 33, 111, 'no', 1, '2016-02-24 08:34:23'),
(2601, 10, 33, 38, 'no', 1, '2016-02-24 08:34:23'),
(2602, 10, 33, 109, 'no', 1, '2016-02-24 08:34:23'),
(2603, 10, 33, 87, 'no', 1, '2016-02-24 08:34:23'),
(2604, 10, 33, 133, 'no', 1, '2016-02-24 08:34:23'),
(2605, 10, 33, 134, 'no', 1, '2016-02-24 08:34:23'),
(2606, 10, 33, 135, 'no', 1, '2016-02-24 08:34:23'),
(2607, 10, 33, 136, 'no', 1, '2016-02-24 08:34:23'),
(2608, 10, 33, 137, 'no', 1, '2016-02-24 08:34:23'),
(2609, 10, 33, 138, 'no', 1, '2016-02-24 08:34:23'),
(2610, 10, 33, 139, 'no', 1, '2016-02-24 08:34:23'),
(2611, 10, 33, 140, 'no', 1, '2016-02-24 08:34:23'),
(2612, 10, 33, 141, 'no', 1, '2016-02-24 08:34:23'),
(2613, 10, 33, 142, 'no', 1, '2016-02-24 08:34:23'),
(2614, 10, 33, 143, 'no', 1, '2016-02-24 08:34:23'),
(2615, 10, 33, 144, 'no', 1, '2016-02-24 08:34:23'),
(2616, 10, 33, 145, 'no', 1, '2016-02-24 08:34:23'),
(2617, 10, 33, 146, 'no', 1, '2016-02-24 08:34:23'),
(2618, 10, 33, 149, 'no', 1, '2016-02-24 08:34:23'),
(2619, 10, 33, 150, 'no', 1, '2016-02-24 08:34:23'),
(2620, 10, 33, 151, 'no', 1, '2016-02-24 08:34:23'),
(2621, 10, 33, 267, 'no', 1, '2016-02-24 08:34:23'),
(2622, 10, 33, 268, 'no', 1, '2016-02-24 08:34:23'),
(2625, 10, 11, 56, 'yes', 1, '2016-02-24 08:34:23'),
(2627, 10, 11, 113, 'yes', 1, '2016-02-24 08:34:23'),
(2628, 10, 11, 25, 'yes', 1, '2016-02-24 08:34:23'),
(2631, 10, 11, 95, 'yes', 1, '2016-02-24 08:34:23'),
(2632, 10, 11, 114, 'yes', 1, '2016-02-24 08:34:23'),
(2633, 10, 11, 31, 'yes', 1, '2016-02-24 08:34:23'),
(2636, 10, 11, 96, 'yes', 1, '2016-02-24 08:34:23'),
(2642, 10, 12, 43, 'no', 1, '2016-02-24 08:34:23'),
(2643, 10, 12, 39, 'no', 1, '2016-02-24 08:34:23'),
(2644, 10, 12, 26, 'no', 1, '2016-02-24 08:34:23'),
(2645, 10, 12, 44, 'no', 1, '2016-02-24 08:34:23'),
(2646, 10, 12, 45, 'no', 1, '2016-02-24 08:34:23'),
(2647, 10, 12, 97, 'no', 1, '2016-02-24 08:34:23'),
(2648, 10, 12, 108, 'no', 1, '2016-02-24 08:34:23'),
(2652, 10, 12, 229, 'no', 1, '2016-02-24 08:34:23'),
(2653, 10, 12, 47, 'no', 1, '2016-02-24 08:34:23'),
(2654, 10, 12, 48, 'no', 1, '2016-02-24 08:34:23'),
(2656, 10, 12, 235, 'no', 1, '2016-02-24 08:34:23'),
(2657, 10, 12, 237, 'no', 1, '2016-02-24 08:34:23'),
(2658, 10, 12, 238, 'no', 1, '2016-02-24 08:34:23'),
(2659, 10, 12, 118, 'no', 1, '2016-02-24 08:34:23'),
(2660, 10, 12, 28, 'no', 1, '2016-02-24 08:34:23'),
(2661, 10, 12, 116, 'no', 1, '2016-02-24 08:34:23'),
(2662, 10, 12, 117, 'no', 1, '2016-02-24 08:34:23'),
(2663, 10, 12, 27, 'no', 1, '2016-02-24 08:34:23'),
(2664, 10, 12, 50, 'no', 1, '2016-02-24 08:34:23'),
(2665, 10, 12, 52, 'no', 1, '2016-02-24 08:34:23'),
(2666, 10, 12, 53, 'no', 1, '2016-02-24 08:34:23'),
(2667, 10, 12, 54, 'no', 1, '2016-02-24 08:34:23'),
(2668, 10, 12, 122, 'no', 1, '2016-02-24 08:34:23'),
(2669, 10, 12, 107, 'no', 1, '2016-02-24 08:34:23'),
(2672, 10, 12, 254, 'no', 1, '2016-02-24 08:34:23'),
(2673, 10, 12, 256, 'no', 1, '2016-02-24 08:34:23'),
(2674, 10, 12, 257, 'no', 1, '2016-02-24 08:34:23'),
(2675, 10, 12, 258, 'no', 1, '2016-02-24 08:34:23'),
(2676, 10, 12, 255, 'no', 1, '2016-02-24 08:34:23'),
(2677, 10, 275, 40, 'no', 1, '2016-02-24 08:34:23'),
(2678, 10, 275, 42, 'no', 1, '2016-02-24 08:34:23'),
(2679, 11, 6, 16, 'yes', 1, '2016-02-29 11:40:27'),
(2680, 11, 6, 17, 'yes', 1, '2016-02-29 11:40:27'),
(2681, 11, 6, 70, 'yes', 1, '2016-02-29 11:40:27'),
(2682, 11, 6, 91, 'yes', 1, '2016-02-29 11:40:27'),
(2683, 11, 6, 71, 'yes', 1, '2016-02-29 11:40:27'),
(2684, 11, 5, 13, 'yes', 1, '2016-02-29 11:40:27'),
(2685, 11, 5, 119, 'yes', 1, '2016-02-29 11:40:27'),
(2686, 11, 5, 154, 'yes', 1, '2016-02-29 11:40:27'),
(2687, 11, 5, 120, 'yes', 1, '2016-02-29 11:40:27'),
(2688, 11, 5, 121, 'yes', 1, '2016-02-29 11:40:27'),
(2689, 11, 7, 29, 'yes', 1, '2016-02-29 11:40:27'),
(2690, 11, 7, 30, 'yes', 1, '2016-02-29 11:40:27'),
(2691, 11, 7, 72, 'yes', 1, '2016-02-29 11:40:27'),
(2692, 11, 7, 92, 'yes', 1, '2016-02-29 11:40:27'),
(2693, 11, 7, 73, 'yes', 1, '2016-02-29 11:40:27'),
(2694, 11, 7, 98, 'yes', 1, '2016-02-29 11:40:27'),
(2695, 11, 7, 99, 'yes', 1, '2016-02-29 11:40:27'),
(2696, 11, 7, 101, 'yes', 1, '2016-02-29 11:40:27'),
(2697, 11, 7, 103, 'yes', 1, '2016-02-29 11:40:27'),
(2698, 11, 7, 102, 'yes', 1, '2016-02-29 11:40:27'),
(2699, 11, 7, 129, 'yes', 1, '2016-02-29 11:40:27'),
(2714, 11, 10, 22, 'yes', 1, '2016-02-29 11:40:27'),
(2715, 11, 10, 23, 'yes', 1, '2016-02-29 11:40:27'),
(2716, 11, 10, 64, 'yes', 1, '2016-02-29 11:40:27'),
(2717, 11, 10, 127, 'yes', 1, '2016-02-29 11:40:27'),
(2718, 11, 10, 93, 'yes', 1, '2016-02-29 11:40:27'),
(2719, 11, 10, 65, 'yes', 1, '2016-02-29 11:40:27'),
(2720, 11, 10, 131, 'yes', 1, '2016-02-29 11:40:27'),
(2721, 11, 33, 34, 'yes', 1, '2016-02-29 11:40:27'),
(2722, 11, 33, 74, 'yes', 1, '2016-02-29 11:40:27'),
(2723, 11, 33, 132, 'yes', 1, '2016-02-29 11:40:27'),
(2724, 11, 33, 110, 'yes', 1, '2016-02-29 11:40:27'),
(2725, 11, 33, 112, 'yes', 1, '2016-02-29 11:40:27'),
(2726, 11, 33, 152, 'yes', 1, '2016-02-29 11:40:27'),
(2727, 11, 33, 35, 'yes', 1, '2016-02-29 11:40:27'),
(2728, 11, 33, 153, 'yes', 1, '2016-02-29 11:40:27'),
(2729, 11, 33, 77, 'yes', 1, '2016-02-29 11:40:27'),
(2730, 11, 33, 78, 'yes', 1, '2016-02-29 11:40:27'),
(2731, 11, 33, 79, 'yes', 1, '2016-02-29 11:40:27'),
(2732, 11, 33, 80, 'yes', 1, '2016-02-29 11:40:27'),
(2733, 11, 33, 36, 'yes', 1, '2016-02-29 11:40:27'),
(2734, 11, 33, 81, 'yes', 1, '2016-02-29 11:40:27'),
(2735, 11, 33, 83, 'yes', 1, '2016-02-29 11:40:27'),
(2736, 11, 33, 84, 'yes', 1, '2016-02-29 11:40:27'),
(2737, 11, 33, 86, 'yes', 1, '2016-02-29 11:40:27'),
(2738, 11, 33, 37, 'yes', 1, '2016-02-29 11:40:27'),
(2739, 11, 33, 111, 'yes', 1, '2016-02-29 11:40:27'),
(2740, 11, 33, 38, 'yes', 1, '2016-02-29 11:40:27'),
(2741, 11, 33, 109, 'yes', 1, '2016-02-29 11:40:27'),
(2742, 11, 33, 87, 'yes', 1, '2016-02-29 11:40:27'),
(2743, 11, 33, 133, 'yes', 1, '2016-02-29 11:40:27'),
(2744, 11, 33, 134, 'yes', 1, '2016-02-29 11:40:27'),
(2745, 11, 33, 135, 'yes', 1, '2016-02-29 11:40:27'),
(2746, 11, 33, 136, 'yes', 1, '2016-02-29 11:40:27'),
(2747, 11, 33, 137, 'yes', 1, '2016-02-29 11:40:27'),
(2748, 11, 33, 138, 'yes', 1, '2016-02-29 11:40:27'),
(2749, 11, 33, 139, 'yes', 1, '2016-02-29 11:40:27'),
(2750, 11, 33, 140, 'yes', 1, '2016-02-29 11:40:27'),
(2751, 11, 33, 141, 'yes', 1, '2016-02-29 11:40:27'),
(2752, 11, 33, 142, 'yes', 1, '2016-02-29 11:40:27'),
(2753, 11, 33, 143, 'yes', 1, '2016-02-29 11:40:27'),
(2754, 11, 33, 144, 'yes', 1, '2016-02-29 11:40:27'),
(2755, 11, 33, 145, 'yes', 1, '2016-02-29 11:40:27'),
(2756, 11, 33, 146, 'yes', 1, '2016-02-29 11:40:27'),
(2757, 11, 33, 149, 'yes', 1, '2016-02-29 11:40:27'),
(2758, 11, 33, 150, 'yes', 1, '2016-02-29 11:40:27'),
(2759, 11, 33, 151, 'yes', 1, '2016-02-29 11:40:27'),
(2760, 11, 33, 267, 'yes', 1, '2016-02-29 11:40:27'),
(2761, 11, 33, 268, 'yes', 1, '2016-02-29 11:40:27'),
(2764, 11, 11, 56, 'yes', 1, '2016-02-29 11:40:27'),
(2766, 11, 11, 113, 'yes', 1, '2016-02-29 11:40:27'),
(2767, 11, 11, 25, 'yes', 1, '2016-02-29 11:40:27'),
(2770, 11, 11, 95, 'yes', 1, '2016-02-29 11:40:27'),
(2771, 11, 11, 114, 'yes', 1, '2016-02-29 11:40:27'),
(2772, 11, 11, 31, 'yes', 1, '2016-02-29 11:40:27'),
(2775, 11, 11, 96, 'yes', 1, '2016-02-29 11:40:27'),
(2781, 11, 12, 43, 'yes', 1, '2016-02-29 11:40:27'),
(2782, 11, 12, 39, 'yes', 1, '2016-02-29 11:40:27'),
(2783, 11, 12, 26, 'yes', 1, '2016-02-29 11:40:27'),
(2784, 11, 12, 44, 'yes', 1, '2016-02-29 11:40:27'),
(2785, 11, 12, 45, 'yes', 1, '2016-02-29 11:40:27'),
(2786, 11, 12, 97, 'yes', 1, '2016-02-29 11:40:27'),
(2787, 11, 12, 108, 'yes', 1, '2016-02-29 11:40:27'),
(2791, 11, 12, 229, 'yes', 1, '2016-02-29 11:40:27'),
(2792, 11, 12, 47, 'yes', 1, '2016-02-29 11:40:27'),
(2793, 11, 12, 48, 'yes', 1, '2016-02-29 11:40:27'),
(2795, 11, 12, 235, 'yes', 1, '2016-02-29 11:40:27'),
(2796, 11, 12, 237, 'yes', 1, '2016-02-29 11:40:27'),
(2797, 11, 12, 238, 'yes', 1, '2016-02-29 11:40:27'),
(2798, 11, 12, 118, 'yes', 1, '2016-02-29 11:40:27'),
(2799, 11, 12, 28, 'yes', 1, '2016-02-29 11:40:27'),
(2800, 11, 12, 116, 'yes', 1, '2016-02-29 11:40:27'),
(2801, 11, 12, 117, 'yes', 1, '2016-02-29 11:40:27'),
(2802, 11, 12, 27, 'yes', 1, '2016-02-29 11:40:27'),
(2803, 11, 12, 50, 'yes', 1, '2016-02-29 11:40:27'),
(2804, 11, 12, 52, 'yes', 1, '2016-02-29 11:40:27'),
(2805, 11, 12, 53, 'yes', 1, '2016-02-29 11:40:27'),
(2806, 11, 12, 54, 'yes', 1, '2016-02-29 11:40:27'),
(2807, 11, 12, 122, 'yes', 1, '2016-02-29 11:40:27'),
(2808, 11, 12, 107, 'yes', 1, '2016-02-29 11:40:27'),
(2811, 11, 12, 254, 'yes', 1, '2016-02-29 11:40:27'),
(2812, 11, 12, 256, 'yes', 1, '2016-02-29 11:40:27'),
(2813, 11, 12, 257, 'yes', 1, '2016-02-29 11:40:27'),
(2814, 11, 12, 258, 'yes', 1, '2016-02-29 11:40:27'),
(2815, 11, 12, 255, 'yes', 1, '2016-02-29 11:40:27'),
(2816, 11, 275, 40, 'yes', 1, '2016-02-29 11:40:27'),
(2817, 11, 275, 42, 'yes', 1, '2016-02-29 11:40:27'),
(2838, 1, 10, 282, 'yes', 1, '2016-05-31 13:38:52'),
(2839, 9, 10, 282, 'no', 1, '2016-05-31 13:38:52'),
(2840, 10, 10, 282, 'no', 1, '2016-05-31 13:38:52'),
(2841, 11, 10, 282, 'no', 1, '2016-05-31 13:38:52');

-- --------------------------------------------------------

--
-- Table structure for table `social_comments`
--

CREATE TABLE `social_comments` (
  `id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `user_name` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `email` varchar(256) NOT NULL,
  `body` longtext NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `social_email_subscription`
--

CREATE TABLE `social_email_subscription` (
  `id` int(11) NOT NULL,
  `email` varchar(265) NOT NULL,
  `inserted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `social_email_subscription`
--

INSERT INTO `social_email_subscription` (`id`, `email`, `inserted_date`) VALUES
(1, 'emadtab97@gmail.com', '0000-00-00 00:00:00'),
(2, 'emadtab97@gmail.comm', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `structure_menu_group`
--

CREATE TABLE `structure_menu_group` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `image` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structure_menu_group`
--

INSERT INTO `structure_menu_group` (`id`, `title`, `alias`, `image`, `description`, `inserted_by`, `inserted_date`, `update_by`, `last_update`) VALUES
(1, 'Main Menu', 'main_menu', '', '', 1, '2016-06-05 15:45:29', 0, '0000-00-00 00:00:00'),
(2, 'Footer menu one', 'footer_menu_one', '', '', 1, '2016-06-05 15:46:00', 0, '0000-00-00 00:00:00'),
(3, 'Footer menu two', 'footer_menu_two', '', '', 1, '2016-06-05 15:46:14', 0, '0000-00-00 00:00:00'),
(4, 'Footer menu three', 'footer_menu_three', '', '', 1, '2016-06-05 15:46:26', 0, '0000-00-00 00:00:00'),
(5, 'social menu', 'social_menu', '', '', 1, '2016-06-05 16:11:37', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `structure_menu_link`
--

CREATE TABLE `structure_menu_link` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `path_type` enum('post','external','page','event','category') NOT NULL,
  `path` int(11) DEFAULT NULL,
  `external_path` varchar(250) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `icon` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `drop_down` enum('yes','no') NOT NULL,
  `drop_down_style` enum('op1','op2','op3') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structure_menu_link`
--

INSERT INTO `structure_menu_link` (`id`, `parent_id`, `group_id`, `sorting`, `path_type`, `path`, `external_path`, `status`, `icon`, `image`, `drop_down`, `drop_down_style`, `inserted_by`, `inserted_date`, `update_by`, `last_update`, `description`) VALUES
(1, 0, 1, 1, 'external', 0, '#', 'publish', '', 'media-library/hotel2.jpg', '', '', 1, '2016-09-13 02:54:02', 1, '2016-09-25 14:12:20', ''),
(4, 0, 1, 2, 'external', 0, '', 'publish', '', '../', '', '', 1, '2016-09-13 03:10:24', 1, '2016-09-18 09:48:35', ''),
(5, 0, 1, 3, 'page', 24, '', 'publish', '', 'media-library/', '', '', 1, '2016-09-13 03:10:56', 1, '2016-09-20 09:06:36', ''),
(6, 0, 1, 4, 'page', 23, '', 'publish', '', 'media-library/', '', '', 1, '2016-09-13 03:11:19', 1, '2016-09-19 22:58:00', ''),
(7, 0, 1, 5, 'page', 32, '', 'publish', '', 'media-library/', '', '', 1, '2016-09-13 03:12:00', 1, '2016-09-21 14:15:25', ''),
(8, 0, 1, 6, 'page', 16, '', 'publish', '', '../', '', '', 1, '2016-09-13 03:12:36', 1, '2016-09-17 05:49:23', ''),
(9, 0, 1, 8, 'page', 21, '', 'publish', '', 'media-library/', '', '', 1, '2016-09-13 03:21:57', 1, '2016-09-18 13:03:12', ''),
(11, 1, 1, 1, 'page', 17, '#', 'publish', '', '../', '', '', 1, '2016-09-18 12:34:24', 1, '2016-09-18 12:41:30', ''),
(12, 1, 1, 2, 'page', 18, '#', 'publish', '', '../', '', '', 1, '2016-09-18 12:35:16', 1, '2016-09-18 12:41:39', ''),
(13, 1, 1, 3, 'page', 26, '', 'publish', '', 'media-library/', '', '', 1, '2016-09-18 12:42:09', 1, '2016-09-20 09:36:34', ''),
(14, 1, 1, 4, 'page', 27, '', 'publish', '', '../', '', '', 1, '2016-09-18 12:42:45', 1, '2016-09-20 10:21:49', ''),
(15, 1, 1, 5, 'page', 22, '', 'publish', '', '../', '', '', 1, '2016-09-18 16:14:27', 1, '2016-09-20 11:33:38', '');

-- --------------------------------------------------------

--
-- Table structure for table `structure_menu_link_content`
--

CREATE TABLE `structure_menu_link_content` (
  `id` int(11) NOT NULL,
  `link_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structure_menu_link_content`
--

INSERT INTO `structure_menu_link_content` (`id`, `link_id`, `title`, `lang_id`, `description`) VALUES
(1, 1, 'الوجهه', 2, ''),
(2, 1, 'Destination', 1, ''),
(7, 4, 'المالك ', 2, ''),
(8, 4, 'Owner Ship', 1, ''),
(9, 5, 'نادى الاجازه', 2, ''),
(10, 5, 'Vacation Club', 1, ''),
(11, 6, 'العروض', 2, ''),
(12, 6, 'Offers', 1, ''),
(13, 7, 'معرض الصور', 2, ''),
(14, 7, 'Photo Gallery', 1, ''),
(15, 8, 'اتصل بنا', 2, ''),
(16, 8, 'Contact Us', 1, ''),
(17, 9, 'احجز الان ', 2, '<p>btn give-btn-bg</p>'),
(18, 9, 'Book Now', 1, '<p>btn give-btn-bg</p>'),
(21, 11, 'الغرف', 2, ''),
(22, 11, 'Rooms', 1, ''),
(23, 12, 'سبا', 2, ''),
(24, 12, 'Spa', 1, ''),
(25, 13, 'استطلاع', 2, ''),
(26, 13, 'Survey', 1, ''),
(27, 14, 'قالوا عن هيلتون', 2, ''),
(28, 14, 'Testimonials', 1, ''),
(29, 15, 'عن شرم الشيخ', 2, ''),
(30, 15, 'About Sharm Elsheikh', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `taxonomies`
--

CREATE TABLE `taxonomies` (
  `id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `taxonomy_type` enum('tag','category','author','country') NOT NULL,
  `sorting` int(11) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `cover` varchar(250) NOT NULL,
  `main_menu` enum('no','yes') NOT NULL,
  `show_image` enum('no','yes') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxonomies`
--

INSERT INTO `taxonomies` (`id`, `sort`, `parent_id`, `taxonomy_type`, `sorting`, `status`, `cover`, `main_menu`, `show_image`, `inserted_by`, `inserted_date`, `update_by`, `last_update`) VALUES
(1, 0, 0, 'category', 1, 'publish', 'media-library/', 'no', 'no', 1, '2016-09-13 05:48:36', 1, '2016-09-13 23:45:10'),
(2, 0, 0, 'category', 2, 'publish', '', 'no', 'no', 1, '2016-09-14 00:55:35', 0, '0000-00-00 00:00:00'),
(3, 0, 0, 'category', 3, 'publish', 'media-library/', 'no', 'no', 1, '2016-09-14 01:22:37', 1, '2016-09-14 01:23:09'),
(4, 0, 0, 'category', 4, 'publish', '', 'no', 'no', 1, '2016-09-15 02:15:38', 0, '0000-00-00 00:00:00'),
(5, 0, 0, 'category', 5, 'publish', '', 'no', 'no', 1, '2016-09-21 10:35:39', 0, '0000-00-00 00:00:00'),
(6, 0, 11, 'category', 6, 'publish', 'media-library/', 'no', 'no', 1, '2016-09-21 14:05:37', 1, '2016-09-21 14:25:39'),
(7, 0, 11, 'category', 7, 'publish', 'media-library/', 'no', 'no', 1, '2016-09-21 14:06:04', 1, '2016-09-21 14:25:48'),
(8, 0, 11, 'category', 8, 'publish', 'media-library/', 'no', 'no', 1, '2016-09-21 14:06:51', 1, '2016-09-21 14:25:58'),
(9, 0, 0, '', 9, 'publish', 'media-library/', 'no', 'no', 1, '2016-09-21 14:07:12', 1, '2016-09-21 14:07:22'),
(10, 0, 11, 'category', 9, 'publish', 'media-library/', 'no', 'no', 1, '2016-09-21 14:07:57', 1, '2016-09-21 14:26:40'),
(11, 0, 0, 'category', 11, 'publish', '', 'no', 'no', 1, '2016-09-21 14:25:30', 0, '0000-00-00 00:00:00'),
(12, 0, 6, 'category', 1, 'publish', '', 'no', 'no', 1, '2016-09-26 04:31:42', 0, '0000-00-00 00:00:00'),
(16, 0, 6, 'category', 2, 'publish', '', 'no', 'no', 1, '2016-09-26 04:48:09', 0, '0000-00-00 00:00:00'),
(17, 0, 6, 'category', 3, 'publish', '', 'no', 'no', 1, '2016-09-26 04:48:36', 0, '0000-00-00 00:00:00'),
(18, 0, 6, 'category', 4, 'publish', '', 'no', 'no', 1, '2016-09-26 04:49:07', 0, '0000-00-00 00:00:00'),
(19, 0, 7, 'category', 1, 'publish', '', 'no', 'no', 1, '2016-09-26 04:51:59', 0, '0000-00-00 00:00:00'),
(20, 0, 7, 'category', 2, 'publish', '', 'no', 'no', 1, '2016-09-26 04:52:26', 0, '0000-00-00 00:00:00'),
(21, 0, 7, 'category', 3, 'publish', '', 'no', 'no', 1, '2016-09-26 04:53:04', 0, '0000-00-00 00:00:00'),
(22, 0, 7, 'category', 4, 'publish', '', 'no', 'no', 1, '2016-09-26 04:53:39', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `taxonomies_content`
--

CREATE TABLE `taxonomies_content` (
  `id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `alias` varchar(256) NOT NULL,
  `description` longtext CHARACTER SET utf16 NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxonomies_content`
--

INSERT INTO `taxonomies_content` (`id`, `taxonomy_id`, `name`, `alias`, `description`, `lang_id`) VALUES
(1, 1, '', '', '', 2),
(2, 1, 'Main Special Offer', 'main_special_offer', '', 1),
(3, 2, '', '', '', 2),
(4, 2, 'normal offers', 'normal_offers', '', 1),
(5, 3, 'عروض اخبار', 'عروض_اخبار', '', 2),
(6, 3, 'news offers', 'news_offers', '', 1),
(7, 4, 'سلايدر قالوا عنا', 'سلايدر_قالوا_عنا', '', 2),
(8, 4, 'Testimonials Slider', 'testimonials_slider', '', 1),
(9, 5, 'الغرف', 'الغرف', '', 2),
(10, 5, 'Rooms', 'rooms', '', 1),
(11, 6, 'المعرض الاول', 'المعرض_الاول', '', 2),
(12, 6, 'Gallery 1', 'gallery_1', '', 1),
(13, 7, 'المعرض الثاني', 'المعرض_الثاني', '', 2),
(14, 7, 'Gallery 2', 'gallery_2', '', 1),
(15, 8, 'المعرض الثالث', 'المعرض_الثالث', '', 2),
(16, 8, 'Gallery 3', 'gallery_3', '', 1),
(17, 9, 'المعرض الرابع', 'المعرض_الرابع', '', 2),
(18, 9, 'Gallery 4', 'gallery_4', '', 1),
(19, 10, 'المعرض الرابع', 'المعرض_الرابع', '', 2),
(20, 10, 'Gallery 4', 'gallery_4', '', 1),
(21, 11, 'المعرض', 'المعرض', '', 2),
(22, 11, 'Gallery', 'gallery', '', 1),
(23, 12, 'الموضع الاول', 'الموضع_الاول', '', 2),
(24, 12, 'Position One', 'position_one', '', 1),
(31, 16, '', '', '', 2),
(32, 16, 'Position Two', 'position_two', '', 1),
(33, 17, '', '', '', 2),
(34, 17, 'Position Three', 'position_three', '', 1),
(35, 18, '', '', '', 2),
(36, 18, 'Position Four', 'position_four', '', 1),
(37, 19, '', '', '', 2),
(38, 19, 'Position One', 'position_one', '', 1),
(39, 20, '', '', '', 2),
(40, 20, 'Position Two', 'position_two', '', 1),
(41, 21, '', '', '', 2),
(42, 21, 'Position Three', 'position_three', '', 1),
(43, 22, '', '', '', 2),
(44, 22, 'Position Four', 'position_four', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `version` char(5) NOT NULL,
  `author` varchar(50) NOT NULL,
  `source` varchar(150) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `uploaded_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `version`, `author`, `source`, `uploaded_date`, `uploaded_by`) VALUES
(2, 'main', '1.0', 'diva', 'main', '2014-08-02 14:33:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `themes_layouts`
--

CREATE TABLE `themes_layouts` (
  `id` int(11) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `version` char(5) NOT NULL,
  `author` varchar(50) NOT NULL,
  `layout_name` varchar(100) NOT NULL,
  `layout_cells` int(11) NOT NULL,
  `source` varchar(100) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `uploaded_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `themes_layouts`
--

INSERT INTO `themes_layouts` (`id`, `theme_id`, `version`, `author`, `layout_name`, `layout_cells`, `source`, `uploaded_date`, `uploaded_by`) VALUES
(4, 2, '1.0', 'diva', 'internal-page-2side', 2, 'main', '2014-08-02 14:33:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `theme_layout_model`
--

CREATE TABLE `theme_layout_model` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `type` enum('page','post','event') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `theme_layout_model`
--

INSERT INTO `theme_layout_model` (`id`, `name`, `theme_id`, `layout_id`, `type`) VALUES
(1, 'post model', 2, 4, 'post'),
(2, 'event model', 2, 4, 'event'),
(3, 'page model', 2, 4, 'page'),
(4, '', 2, 4, 'page'),
(5, 'events page', 2, 4, 'page'),
(6, 'parsing', 2, 4, 'page'),
(7, 'contact us page', 2, 4, 'page'),
(8, 'membership_model', 2, 4, 'page'),
(9, 'offers_model', 2, 4, 'page'),
(10, 'survey', 2, 4, 'page'),
(11, 'survey_model', 2, 4, 'page'),
(12, 'testimopnial_model', 2, 4, 'page'),
(13, 'about_page_model', 2, 4, 'page'),
(14, 'rooms_model', 2, 4, 'page'),
(15, 'gallery_model_page', 2, 4, 'page');

-- --------------------------------------------------------

--
-- Table structure for table `theme_layout_model_plugin`
--

CREATE TABLE `theme_layout_model_plugin` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `plugin_id` int(11) NOT NULL,
  `sorting` int(11) NOT NULL,
  `position` enum('left','right') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `theme_layout_model_plugin`
--

INSERT INTO `theme_layout_model_plugin` (`id`, `model_id`, `plugin_id`, `sorting`, `position`) VALUES
(1, 3, 1, 1, 'left'),
(32, 2, 34, 1, 'left'),
(33, 2, 44, 1, 'right'),
(34, 2, 47, 2, 'right'),
(35, 4, 35, 1, 'left'),
(36, 4, 47, 1, 'right'),
(37, 4, 46, 2, 'right'),
(38, 4, 44, 3, 'right'),
(39, 5, 48, 1, 'left'),
(40, 5, 44, 1, 'right'),
(41, 5, 45, 2, 'right'),
(42, 6, 49, 1, 'left'),
(63, 1, 7, 1, 'left'),
(64, 1, 44, 1, 'right'),
(65, 1, 45, 2, 'right'),
(66, 1, 46, 3, 'right'),
(67, 1, 47, 4, 'right'),
(68, 7, 3, 1, 'left'),
(69, 8, 50, 1, 'left'),
(70, 9, 51, 1, 'left'),
(71, 10, 52, 1, 'left'),
(72, 11, 52, 1, 'left'),
(73, 12, 53, 1, 'left'),
(74, 13, 54, 1, 'left'),
(75, 14, 59, 1, 'left'),
(76, 15, 60, 1, 'left');

-- --------------------------------------------------------

--
-- Table structure for table `time_zones`
--

CREATE TABLE `time_zones` (
  `id` int(11) NOT NULL,
  `GMT` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_zones`
--

INSERT INTO `time_zones` (`id`, `GMT`, `name`) VALUES
(1, '-12.0', '(GMT-12:00)-International Date Line West'),
(2, '-11.0', '(GMT-11:00)-Midway Island, Samoa'),
(3, '-10.0', '(GMT-10:00)-Hawaii'),
(4, '-9.0', '(GMT-09:00)-Alaska'),
(5, '-8.0', '(GMT-08:00)-Pacific Time (US & Canada); Tijuana'),
(6, '-7.0', '(GMT-07:00)-Arizona'),
(7, '-7.0', '(GMT-07:00)-Chihuahua, La Paz, Mazatlan'),
(8, '-7.0', '(GMT-07:00)-Mountain Time (US & Canada)'),
(9, '-6.0', '(GMT-06:00)-Central America'),
(10, '-6.0', '(GMT-06:00)-Central Time (US & Canada)'),
(11, '-6.0', '(GMT-06:00)-Guadalajara, Mexico City, Monterrey'),
(12, '-6.0', '(GMT-06:00)-Saskatchewan'),
(13, '-5.0', '(GMT-05:00)-Bogota, Lima, Quito'),
(14, '-5.0', '(GMT-05:00)-Eastern Time (US & Canada)'),
(15, '-5.0', '(GMT-05:00)-Indiana (East)'),
(16, '-4.0', '(GMT-04:00)-Atlantic Time (Canada)'),
(17, '-4.0', '(GMT-04:00)-Caracas, La Paz'),
(18, '-4.0', '(GMT-04:00)-Santiago'),
(19, '-3.5', '(GMT-03:30)-Newfoundland'),
(20, '-3.0', '(GMT-03:00)-Brasilia'),
(21, '-3.0', '(GMT-03:00)-Buenos Aires, Georgetown'),
(22, '-3.0', '(GMT-03:00)-Greenland'),
(23, '-2.0', '(GMT-02:00)-Mid-Atlantic'),
(24, '-1.0', '(GMT-01:00)-Azores'),
(25, '-1.0', '(GMT-01:00)-Cape Verde Is.'),
(26, '0.0', '(GMT)-Casablanca, Monrovia'),
(27, '0.0', '(GMT)-Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London'),
(28, '1.0', '(GMT+01:00)-Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'),
(29, '1.0', '(GMT+01:00)-Belgrade, Bratislava, Budapest, Ljubljana, Prague'),
(30, '1.0', '(GMT+01:00)-Brussels, Copenhagen, Madrid, Paris'),
(31, '1.0', '(GMT+01:00)-Sarajevo, Skopje, Warsaw, Zagreb'),
(32, '1.0', '(GMT+01:00)-West Central Africa'),
(33, '2.0', '(GMT+02:00)-Athens, Beirut, Istanbul, Minsk'),
(34, '2.0', '(GMT+02:00)-Bucharest'),
(35, '2.0', '(GMT+02:00)-Cairo'),
(36, '2.0', '(GMT+02:00)-Harare, Pretoria'),
(37, '2.0', '(GMT+02:00)-Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius'),
(38, '2.0', '(GMT+02:00)-Jerusalem'),
(39, '3.0', '(GMT+03:00)-Baghdad'),
(40, '3.0', '(GMT+03:00)-Kuwait, Riyadh'),
(41, '3.0', '(GMT+03:00)-Moscow, St. Petersburg, Volgograd'),
(42, '3.0', '(GMT+03:00)-Nairobi'),
(43, '3.5', '(GMT+03:30)-Tehran'),
(44, '4.0', '(GMT+04:00)-Abu Dhabi, Muscat'),
(45, '4.0', '(GMT+04:00)-Baku, Tbilisi, Yerevan'),
(46, '4.5', '(GMT+04:30)-Kabul'),
(47, '5.0', '(GMT+05:00)-Ekaterinburg'),
(48, '5.0', '(GMT+05:00)-Islamabad, Karachi, Tashkent'),
(49, '5.5', '(GMT+05:30)-Chennai, Kolkata, Mumbai, New Delhi'),
(50, '5.75', '(GMT+05:45)-Kathmandu'),
(51, '6.0', '(GMT+06:00)-Almaty, Novosibirsk'),
(52, '6.0', '(GMT+06:00)-Astana, Dhaka'),
(53, '6.0', '(GMT+06:00)-Sri Jayawardenepura'),
(54, '6.5', '(GMT+06:30)-Rangoon'),
(55, '7.0', '(GMT+07:00)-Bangkok, Hanoi, Jakarta'),
(56, '7.0', '(GMT+07:00)-Krasnoyarsk'),
(57, '8.0', '(GMT+08:00)-Beijing, Chongqing, Hong Kong, Urumqi'),
(58, '8.0', '(GMT+08:00)-Irkutsk, Ulaan Bataar'),
(59, '8.0', '(GMT+08:00)-Kuala Lumpur, Singapore'),
(60, '8.0', '(GMT+08:00)-Perth'),
(61, '8.0', '(GMT+08:00)-Taipei'),
(62, '9.0', '(GMT+09:00)-Osaka, Sapporo, Tokyo'),
(63, '9.0', '(GMT+09:00)-Seoul'),
(64, '9.0', '(GMT+09:00)-Vakutsk'),
(65, '9.5', '(GMT+09:30)-Adelaide'),
(66, '9.5', '(GMT+09:30)-Darwin'),
(67, '10.0', '(GMT+10:00)-Brisbane'),
(68, '10.0', '(GMT+10:00)-Canberra, Melbourne, Sydney'),
(69, '10.0', '(GMT+10:00)-Guam, Port Moresby'),
(70, '10.0', '(GMT+10:00)-Hobart'),
(71, '10.0', '(GMT+10:00)-Vladivostok'),
(72, '11.0', '(GMT+11:00)-Magadan, Solomon Is., New Caledonia'),
(73, '12.0', '(GMT+12:00)-Auckland, Wellington'),
(74, '12.0', '(GMT+12:00)-Fiji, Kamchatka, Marshall Is.'),
(75, '-12.0', '(GMT-12:00)-International Date Line West'),
(76, '-11.0', '(GMT-11:00)-Midway Island, Samoa'),
(77, '-10.0', '(GMT-10:00)-Hawaii'),
(78, '-9.0', '(GMT-09:00)-Alaska'),
(79, '-8.0', '(GMT-08:00)-Pacific Time (US & Canada); Tijuana'),
(80, '-7.0', '(GMT-07:00)-Arizona'),
(81, '-7.0', '(GMT-07:00)-Chihuahua, La Paz, Mazatlan'),
(82, '-7.0', '(GMT-07:00)-Mountain Time (US & Canada)'),
(83, '-6.0', '(GMT-06:00)-Central America'),
(84, '-6.0', '(GMT-06:00)-Central Time (US & Canada)'),
(85, '-6.0', '(GMT-06:00)-Guadalajara, Mexico City, Monterrey'),
(86, '-6.0', '(GMT-06:00)-Saskatchewan'),
(87, '-5.0', '(GMT-05:00)-Bogota, Lima, Quito'),
(88, '-5.0', '(GMT-05:00)-Eastern Time (US & Canada)'),
(89, '-5.0', '(GMT-05:00)-Indiana (East)'),
(90, '-4.0', '(GMT-04:00)-Atlantic Time (Canada)'),
(91, '-4.0', '(GMT-04:00)-Caracas, La Paz'),
(92, '-4.0', '(GMT-04:00)-Santiago'),
(93, '-3.5', '(GMT-03:30)-Newfoundland'),
(94, '-3.0', '(GMT-03:00)-Brasilia'),
(95, '-3.0', '(GMT-03:00)-Buenos Aires, Georgetown'),
(96, '-3.0', '(GMT-03:00)-Greenland'),
(97, '-2.0', '(GMT-02:00)-Mid-Atlantic'),
(98, '-1.0', '(GMT-01:00)-Azores'),
(99, '-1.0', '(GMT-01:00)-Cape Verde Is.'),
(100, '0.0', '(GMT)-Casablanca, Monrovia'),
(101, '0.0', '(GMT)-Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London'),
(102, '1.0', '(GMT+01:00)-Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'),
(103, '1.0', '(GMT+01:00)-Belgrade, Bratislava, Budapest, Ljubljana, Prague'),
(104, '1.0', '(GMT+01:00)-Brussels, Copenhagen, Madrid, Paris'),
(105, '1.0', '(GMT+01:00)-Sarajevo, Skopje, Warsaw, Zagreb'),
(106, '1.0', '(GMT+01:00)-West Central Africa'),
(107, '2.0', '(GMT+02:00)-Athens, Beirut, Istanbul, Minsk'),
(108, '2.0', '(GMT+02:00)-Bucharest'),
(109, '2.0', '(GMT+02:00)-Cairo'),
(110, '2.0', '(GMT+02:00)-Harare, Pretoria'),
(111, '2.0', '(GMT+02:00)-Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius'),
(112, '2.0', '(GMT+02:00)-Jerusalem'),
(113, '3.0', '(GMT+03:00)-Baghdad'),
(114, '3.0', '(GMT+03:00)-Kuwait, Riyadh'),
(115, '3.0', '(GMT+03:00)-Moscow, St. Petersburg, Volgograd'),
(116, '3.0', '(GMT+03:00)-Nairobi'),
(117, '3.5', '(GMT+03:30)-Tehran'),
(118, '4.0', '(GMT+04:00)-Abu Dhabi, Muscat'),
(119, '4.0', '(GMT+04:00)-Baku, Tbilisi, Yerevan'),
(120, '4.5', '(GMT+04:30)-Kabul'),
(121, '5.0', '(GMT+05:00)-Ekaterinburg'),
(122, '5.0', '(GMT+05:00)-Islamabad, Karachi, Tashkent'),
(123, '5.5', '(GMT+05:30)-Chennai, Kolkata, Mumbai, New Delhi'),
(124, '5.75', '(GMT+05:45)-Kathmandu'),
(125, '6.0', '(GMT+06:00)-Almaty, Novosibirsk'),
(126, '6.0', '(GMT+06:00)-Astana, Dhaka'),
(127, '6.0', '(GMT+06:00)-Sri Jayawardenepura'),
(128, '6.5', '(GMT+06:30)-Rangoon'),
(129, '7.0', '(GMT+07:00)-Bangkok, Hanoi, Jakarta'),
(130, '7.0', '(GMT+07:00)-Krasnoyarsk'),
(131, '8.0', '(GMT+08:00)-Beijing, Chongqing, Hong Kong, Urumqi'),
(132, '8.0', '(GMT+08:00)-Irkutsk, Ulaan Bataar'),
(133, '8.0', '(GMT+08:00)-Kuala Lumpur, Singapore'),
(134, '8.0', '(GMT+08:00)-Perth'),
(135, '8.0', '(GMT+08:00)-Taipei'),
(136, '9.0', '(GMT+09:00)-Osaka, Sapporo, Tokyo'),
(137, '9.0', '(GMT+09:00)-Seoul'),
(138, '9.0', '(GMT+09:00)-Vakutsk'),
(139, '9.5', '(GMT+09:30)-Adelaide'),
(140, '9.5', '(GMT+09:30)-Darwin'),
(141, '10.0', '(GMT+10:00)-Brisbane'),
(142, '10.0', '(GMT+10:00)-Canberra, Melbourne, Sydney'),
(143, '10.0', '(GMT+10:00)-Guam, Port Moresby'),
(144, '10.0', '(GMT+10:00)-Hobart'),
(145, '10.0', '(GMT+10:00)-Vladivostok'),
(146, '11.0', '(GMT+11:00)-Magadan, Solomon Is., New Caledonia'),
(147, '12.0', '(GMT+12:00)-Auckland, Wellington'),
(148, '12.0', '(GMT+12:00)-Fiji, Kamchatka, Marshall Is.'),
(149, '13.0', '(GMT+13:00)-Nuku''alofa ');

-- --------------------------------------------------------

--
-- Table structure for table `url`
--

CREATE TABLE `url` (
  `id` int(11) NOT NULL,
  `link` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `url`
--

INSERT INTO `url` (`id`, `link`) VALUES
(1, ''),
(2, 'gsdfgsdgsdf'),
(3, 'gsdfgsdgsdf'),
(4, 'http://www.w3schools.com/php/php_mysql_insert_lastid.asp'),
(5, 'http://www.w3schools.com/php/php_mysql_insert_lastid.asp'),
(6, 'http://www.jobs.net/jobs/empire-today/en-us/search/?keyword=&location='),
(7, 'http://www.jobs.net/jobs/empire-today/en-us/search/?keyword=&location='),
(8, 'http://www.jobs.net/jobs/empire-today/en-us/search/?keyword=&location='),
(9, 'http://www.jobs.net/jobs/empire-today/en-us/search/?keyword=&location='),
(10, 'http://www.jobs.net/jobs/empire-today/en-us/search/?keyword=&location='),
(11, ''),
(12, ''),
(13, 'http://www.jobs.net/jobs/empire-today/en-us/search/?keyword=&location='),
(14, ''),
(15, ''),
(16, ''),
(17, ''),
(18, ''),
(19, ''),
(20, ''),
(21, ''),
(22, ''),
(23, ''),
(24, ''),
(25, ''),
(26, ''),
(27, ''),
(28, 'http://vufind.org/demo/Search/Results?lookfor=egypt&type=AllFields&view=rss&skip_rss_sort=1'),
(29, 'http://vufind.org/demo/Search/Results?lookfor=egypt&type=AllFields&view=rss&skip_rss_sort=1'),
(30, ''),
(31, ''),
(32, ''),
(33, ''),
(34, ''),
(35, ''),
(36, 'http://vufind.org/demo/Search/Results?lookfor=egypt&type=AllFields&view=rss&skip_rss_sort=1'),
(37, 'http://vufind.org/demo/Search/Results?lookfor=egypt&type=AllFields&view=rss&skip_rss_sort=1'),
(38, 'http://vufind.org/demo/Search/Results?lookfor=egypt&type=AllFields&view=rss&skip_rss_sort=1'),
(39, 'http://vufind.org/demo/Search/Results?lookfor=egypt&type=AllFields&view=rss&skip_rss_sort=1'),
(40, 'http://vufind.org/demo/Search/Results?lookfor=egypt&type=AllFields');

-- --------------------------------------------------------

--
-- Table structure for table `url_before_parse`
--

CREATE TABLE `url_before_parse` (
  `id` int(11) NOT NULL,
  `link` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `inserted_date` date NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `alias` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `user_level` enum('admin','restaurant_owner','branch_manager','meal_handler','pilot','operation') NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_profile` int(11) NOT NULL,
  `password` varchar(150) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `first_name`, `last_name`, `user_level`, `restaurant_id`, `branch_id`, `email`, `user_profile`, `password`, `inserted_by`, `inserted_date`, `last_update`, `update_by`) VALUES
(1, 'waled.rayan80', 'waleed', 'rayan', 'admin', 0, 0, 'waled@gmail.com', 1, 'fe703d258c7ef5f50b71e06565a65aa07194907f', 4, '2014-04-14 10:45:49', '2015-10-27 15:55:29', 1),
(6, 'AlaaAssem', 'Alaa', 'Assem', 'admin', 0, 0, 'aassem@mls-egypt.org', 1, '889b4f5d5e81327037322672384ec4a719206516', 1, '2016-02-29 11:50:57', '0000-00-00 00:00:00', 0),
(7, 'heba.hussien', 'heba', 'hussien', 'admin', 0, 0, 'hebahussien83@gmail.com', 1, '2bb2125955689088cffd9a68ff483c98a6f694db', 1, '2016-03-01 09:09:03', '0000-00-00 00:00:00', 0),
(8, 'hello friends', 'let''s do it ', 'for you just do it ', 'admin', 0, 0, 'root', 10, '83353d597cbad458989f2b1a5c1fa1f9f665c858', 1, '2016-08-24 08:02:41', '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisement_content`
--
ALTER TABLE `advertisement_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD KEY `id` (`id`);

--
-- Indexes for table `cms_module_access`
--
ALTER TABLE `cms_module_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_selections`
--
ALTER TABLE `customer_selections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events_details`
--
ALTER TABLE `events_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forget_password`
--
ALTER TABLE `forget_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_attributes`
--
ALTER TABLE `form_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_inserted_data`
--
ALTER TABLE `form_inserted_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_setting`
--
ALTER TABLE `general_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page_layout`
--
ALTER TABLE `home_page_layout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `localization`
--
ALTER TABLE `localization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `membership_inquery`
--
ALTER TABLE `membership_inquery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes`
--
ALTER TABLE `nodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes_content`
--
ALTER TABLE `nodes_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes_image_gallery`
--
ALTER TABLE `nodes_image_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes_plugins_values`
--
ALTER TABLE `nodes_plugins_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes_selected_taxonomies`
--
ALTER TABLE `nodes_selected_taxonomies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plugins`
--
ALTER TABLE `plugins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poll_questions`
--
ALTER TABLE `poll_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poll_questions_options`
--
ALTER TABLE `poll_questions_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_modules_access`
--
ALTER TABLE `profile_modules_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_pages_access`
--
ALTER TABLE `profile_pages_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_comments`
--
ALTER TABLE `social_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_email_subscription`
--
ALTER TABLE `social_email_subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structure_menu_group`
--
ALTER TABLE `structure_menu_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structure_menu_link`
--
ALTER TABLE `structure_menu_link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structure_menu_link_content`
--
ALTER TABLE `structure_menu_link_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxonomies`
--
ALTER TABLE `taxonomies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxonomies_content`
--
ALTER TABLE `taxonomies_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes_layouts`
--
ALTER TABLE `themes_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theme_layout_model`
--
ALTER TABLE `theme_layout_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theme_layout_model_plugin`
--
ALTER TABLE `theme_layout_model_plugin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_zones`
--
ALTER TABLE `time_zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `url`
--
ALTER TABLE `url`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `url_before_parse`
--
ALTER TABLE `url_before_parse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `advertisement_content`
--
ALTER TABLE `advertisement_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cms_module_access`
--
ALTER TABLE `cms_module_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=283;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `customer_info`
--
ALTER TABLE `customer_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_selections`
--
ALTER TABLE `customer_selections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events_details`
--
ALTER TABLE `events_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `forget_password`
--
ALTER TABLE `forget_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `form_attributes`
--
ALTER TABLE `form_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `form_inserted_data`
--
ALTER TABLE `form_inserted_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `home_page_layout`
--
ALTER TABLE `home_page_layout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `localization`
--
ALTER TABLE `localization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `membership_inquery`
--
ALTER TABLE `membership_inquery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `nodes`
--
ALTER TABLE `nodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `nodes_content`
--
ALTER TABLE `nodes_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `nodes_image_gallery`
--
ALTER TABLE `nodes_image_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;
--
-- AUTO_INCREMENT for table `nodes_plugins_values`
--
ALTER TABLE `nodes_plugins_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `nodes_selected_taxonomies`
--
ALTER TABLE `nodes_selected_taxonomies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `plugins`
--
ALTER TABLE `plugins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `poll_questions`
--
ALTER TABLE `poll_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `poll_questions_options`
--
ALTER TABLE `poll_questions_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profile_modules_access`
--
ALTER TABLE `profile_modules_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `profile_pages_access`
--
ALTER TABLE `profile_pages_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2842;
--
-- AUTO_INCREMENT for table `social_comments`
--
ALTER TABLE `social_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `social_email_subscription`
--
ALTER TABLE `social_email_subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `structure_menu_group`
--
ALTER TABLE `structure_menu_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `structure_menu_link`
--
ALTER TABLE `structure_menu_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `structure_menu_link_content`
--
ALTER TABLE `structure_menu_link_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `taxonomies`
--
ALTER TABLE `taxonomies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `taxonomies_content`
--
ALTER TABLE `taxonomies_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `themes_layouts`
--
ALTER TABLE `themes_layouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `theme_layout_model`
--
ALTER TABLE `theme_layout_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `theme_layout_model_plugin`
--
ALTER TABLE `theme_layout_model_plugin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `time_zones`
--
ALTER TABLE `time_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;
--
-- AUTO_INCREMENT for table `url`
--
ALTER TABLE `url`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `url_before_parse`
--
ALTER TABLE `url_before_parse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
