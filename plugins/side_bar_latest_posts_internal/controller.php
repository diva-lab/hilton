<?php
	//get plugin data
	$get_plugin_id = Plugins::find_by_custom_filed('source','side_bar_latest_posts_internal');
    $get_plugin_value_for_plugin = NodesPluginsValues::get_plugin_values($get_plugin_id->id,$lang->id,$node_id,$node_type);
	//get all menu group
	$define_taxonomy_class = new Taxonomies();
	$define_taxonomy_class->enable_relation();
	$all_categories = $define_taxonomy_class->get_type(0,0,$lang->id,"category");
?>
<div class="form-group">
<label class="col-lg-3">Section Title-<?php echo ucfirst($lang->label)?>:</label>
<div class="col-lg-4">
<input type="text" class="form-control" name="side_bar_latest_posts_internal_title_<?php echo $lang->label?>" value="<?php  if($get_plugin_value_for_plugin->title){ echo $get_plugin_value_for_plugin->title;}?>" autocomplete="off">
</div>
</div>
<div class="form-group">
<label class="col-lg-3">Select Category-<?php echo ucfirst($lang->label)?>:</label>
<div class="col-lg-4">
<select class="form-control" name="side_bar_latest_posts_internal_<?php echo $lang->label?>">
<option value="0">Select Category</option>
<?php
 foreach($all_categories as $cat=>$value){
     echo "<option value='$cat'";
	 if($cat == $get_plugin_value_for_plugin->content){
		 echo " selected";
	 }
     echo ">$value</option>";
 }?>
</select>
</div>
</div>