<?php
 if($lang =="en"){
	$post_date = english_date($get_post_content->start_publishing);
 }else{
   $post_date = arabic_date($get_post_content->start_publishing);

}
 $image_path = get_image_src($get_post_content->cover_image,"large");

 ?>


 <article class="col-md-12 internal-post"><!--start of internal-post-->
            

              <h4 class="col-md-12 give-boldFamily give-darkRed-c"><?php echo $get_post_content->title ?></h4>

              <p class="col-md-12 give-yellow-c"><?php echo $post_date?></p>
              <?php 
				//process body and separate between plugin name and value
				$fullText = $get_post_content->body;
				//add plugin
				$add_plugin = preg_replace_callback("/\[.+\]/U", "plugin_callback", $fullText)."\n";
				echo $add_plugin;
				//print final
				//echo $add_plugin;
				function plugin_callback($match){
					global $node_id;
					global $node_type;
					global $define_image_gallery;
					$query = substr($match[0],1,-1);                           
					//delete [ and ]
					$text_manipulation = preg_replace('/[\[\]]/s', '', $match[0]);
					ob_start();
					$separate_text = explode('@',$text_manipulation);
					if($separate_text[0] == "cms_plugin"){
						$plugin_value = $separate_text[2];
						if(file_exists("plugins/$separate_text[1]/viewer.php")){
							include "plugins/".$separate_text[1]."/viewer.php";
						}else{
							echo "File Not Exist";	
						}
						$pluginContent = ob_get_contents();
						ob_end_clean();
						return $pluginContent;		
					}
				}
  
 ?> 

              </article>

