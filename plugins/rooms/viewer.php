  <section class="col-md-12 give-padding-tb"><!--start of internal content-->
  <div class="container">
  <div class="row clearfix"><!-- Nav tabs -->
  <?php
//get page category
//show posts
$define_page_categories = new NodesSelectedTaxonomies();
$define_page_categories->enable_relation();
$page_categories = $define_page_categories->return_node_taxonomy($get_page_content->id,'page','category','many',$lang_info->id);
$page_categories_array = array();
if(count($page_categories) > 0){
	
  foreach($page_categories as $category){
	  $page_categories_array[$category->id] = $category->taxonomy_name;
  }
  //get categories ids
  $page_categories_ids = array();
  foreach($page_categories_array as $key=>$value){
	  $page_categories_ids [] = $key;
  }
  
   $count = count($page_categories_ids);
   
   $categories = implode(",",$page_categories_ids);
   //count all posts in the page
   
//    $page_all_post = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$categories, null,null,null,"many");
//   //total posts
//   $total_posts = count($page_all_post);
//   //get page id if exist
//   $page_number = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
//  //number of posts in one page
//   $per_page =20 ;
//   $pagination = new Pagination($page_number , $per_page, $total_posts);
//   //calculate the offset
//   $offset = $pagination->offset();
//   $count = $offset+1;
  $List_posts_for_blog = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$categories, null,null,null,"many",null,null);
  $count_show_result = count($List_posts_for_blog);

// loop for the tabs  : 

$index = 0 ; 
 echo "<ul class='nav nav-tabs col-md-6 internal-room-tabs'>"; 
foreach($List_posts_for_blog as $tab){
  echo "<li class=' " ;
  if($index == 0){
        echo " active " ;
  } 
  echo " '> "; 
  echo " <a href='#$tab->id' data-toggle='tab'><span class='give-btn-c fa fa-angle-double-right'></span>$tab->title</a></li>"; 
  $index ++ ; 
}

echo "</ul>" ; 


  ?>

    
<?php


echo "<div class='tab-content col-md-6 internal-room-content'>" ; 
echo "<h2 class='col-md-12 give-title-slogan give-display-inline'>Type of rooms</h2>";
$loop = 0 ;  
 foreach($List_posts_for_blog as $post){
			  $image_src = get_image_src($post->cover_image,"medium");
			  if($lang == "en"){
						  $date = english_date($post->start_publishing);
			 }else{
						  $date = arabic_date($post->start_publishing);
		     }
			 if($post->cover_image){
				 $image_path = get_image_src($post->cover_image,"large");
			 }else{
				 $image_path = "";
			 }

        
       	 echo "<div id='$post->id' class='tab-pane "; 
          if($loop  == 0){
            echo " active " ;  
          }
        echo " '>" ; 

        // get summary , body and explode them because of many data  : 
        $summary = strip_tags($post->summary); 
        $summary_exploded = explode('-',$summary) ; 
        $main_title = $summary_exploded[0]; 
        $main_title_description = $summary_exploded[1]; 

        $body = strip_tags($post->body); 
        $body_exploded = explode('-',$body) ; 
        $main_title_2 = $body_exploded[0]; 
        $main_title_description_2 = $body_exploded[1]; 

    

        
         echo " <figure class='internal-rooms-figure give-mb-lg'><img src='$image_path' alt='' /> <figcaption><a class='fresco' href='$image_path'> <span class='give-btn-c fa fa-search-plus'></span>click to enlarge</a></figcaption></figure>
                <p>$post->title</p>
                <h5 class='col-md-12 give-btn-c'>$main_title</h5>
                <p>$main_title_description</p>
                <h5 class='col-md-12 give-btn-c'>$main_title_2</h5>
                <p>$main_title_description_2</p>
                </div>" ;  

                $loop ++; 	 
                
			 
		  }//end of foreach
      echo "</div>"; 

    
		  
 }//end of page_categories 
 ?>


 </div>
 </div>
 </section>
 
