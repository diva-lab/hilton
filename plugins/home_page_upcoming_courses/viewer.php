<section class="col-m-12 main-content-section"><!--start of main content section-->
               <header class="clearfix give-mb-med main-content-header"><!--start of main content section header-->
                <h1 class="col-md-12 give-boldFamily give-title-border give-maintitle-border pull-left">Upcoming Training Courses</h1>
                <a href="" class="col-md-12 give-red-c pull-right">View all events <span class="fa fa-long-arrow-right"></span></a>
              </header><!--end of main content section header-->

              <div class="row clearfix"><!--start of row-->
                <article class="col-md-6 secondary-post traninig-post "><!--start of training post-->
                 <div class="col-md-12 secondary-post-inner give-border">
                    <div  class="col-md-12 secondary-image" style="background-image: url('main/images/dummy-image.jpg');">
                    <ul class="col-md-12 post-meta give-red-bg give-padding-sm give-flex text-center give-absolute-bottom">
                     
                       <li title="Share">
                           <a href=""><span class="fa fa-share-square-o give-yellow-c"></span> Share</a>
                      </li>
                       <li title="Views">
                          <p class="fa fa-eye give-yellow-c"></p> 300
                      </li>
                       <li title="Save">
                          <a href=""><span class="fa fa-heart give-yellow-c"></span> Save</a>
                      </li>
                      
                   </ul>
                  </div>
                  <div class="col-md-12 secondary-text give-gray-bg give-padding">
                     <h4 class="col-md-12 give-darkRed-c give-boldFamily"><a href="">A Review of the UK's Interdisciplinary Research using a Citation-based Approach</a></h4>
                    <ul class="col-md-12 post-list">
                       <li>
                          <p class="col-md-12 give-darkRed-c give-boldFamily post-list-title">Duration :</p>
                           From Sunday, January 25, 2016 To February 25, 2016
                      </li>
                       <li>
                          <p class="col-md-12 give-darkRed-c give-boldFamily post-list-title">Traniner :</p>
                           Mohamed Ahmed
                      </li>
                      <li>
                          <p class="col-md-12 give-darkRed-c give-boldFamily post-list-title">Venue :</p>
                          Grand Hall
                      </li>
                      <li>
                         <a href="" class="btn give-red-bg">More Details</a>
                      </li>
                   </ul>
                  </div>
                 </div>
               </article><!--end of training post-->

                <article class="col-md-6 secondary-post traninig-post "><!--start of training post-->
                 <div class="col-md-12 secondary-post-inner give-border">
                    <div  class="col-md-12 secondary-image" style="background-image: url('main/images/dummy-image.jpg');">
                    <ul class="col-md-12 post-meta give-red-bg give-padding-sm give-flex text-center give-absolute-bottom">
                     
                       <li title="Share">
                           <a href=""><span class="fa fa-share-square-o give-yellow-c"></span> Share</a>
                      </li>
                       <li title="Views">
                          <p class="fa fa-eye give-yellow-c"></p> 300
                      </li>
                       <li title="Save">
                          <a href=""><span class="fa fa-heart give-yellow-c"></span> Save</a>
                      </li>
                      
                   </ul>
                  </div>
                  <div class="col-md-12 secondary-text give-gray-bg give-padding">
                     <h4 class="col-md-12 give-darkRed-c give-boldFamily"><a href="">A Review of the UK's Interdisciplinary Research using a Citation-based Approach</a></h4>
                     <ul class="col-md-12 post-list">
                       <li>
                          <p class="col-md-12 give-darkRed-c give-boldFamily post-list-title">Duration :</p>
                           From Sunday, January 25, 2016 To February 25, 2016
                      </li>
                       <li>
                          <p class="col-md-12 give-darkRed-c give-boldFamily post-list-title">Traniner :</p>
                           Mohamed Ahmed
                      </li>
                      <li>
                          <p class="col-md-12 give-darkRed-c give-boldFamily post-list-title">Venue :</p>
                          Grand Hall
                      </li>
                      <li>
                         <a href="" class="btn give-red-bg">More Details</a>
                      </li>
                   </ul>
                  </div>
                 </div>
               </article><!--end of training post-->

             </div><!--end of row-->
            </section>