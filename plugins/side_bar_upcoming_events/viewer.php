<?php
if($header_title){
	  $explode_header = explode("||",$header_title);
	  if($lang == "en"){
		  $pugin_title = $explode_header[0];
	  }else{
		  $pugin_title = $explode_header[1];
	  }
  }else{
	  $pugin_title = "";
  }
 
  $events = $define_node->front_node_data(null,"event",null,null,$lang_info->id,"no",null,"yes",null, null,null,null,"many");
  if($events){
	   echo "<h2 class='col-md-12 give-boldFamily give-title-border give-sidetitle-border give-darkRed-c give-mb-med'>$pugin_title</h2>";
	  foreach($events as $event){
		  $event_details = EventDetails::find_by_custom_filed("event_id",$event->id);
		  $timeStamp = strtotime($event_details->start_date);
		  $day =  date('j', $timeStamp);
		  $month = date('M', $timeStamp);
		  if($lang == "en"){
			  $view_month = $month;
		  }else{
			  $view_month = $ar_month[$month];
		  }
		  echo "<article class='clearfix event-post'>
                     <a href='event_details.php?lang=$lang&alias=$event->alias' class='col-md-3 event-image'>
                       <p class='col-md-12 event-day give-red-c give-boldFamily'>$day</p>
                       <p class='col-md-12 event-month give-red-c give-boldFamily'>$view_month</p>
                     </a>
                     <div class='col-md-8 event-text'>
                       <h4>$event->title</h4>
                       <a href='event_details.php?lang=$lang&alias=$event->alias' class='give-red-c'>$read_more</a>
                     </div>
                   </article>";
	 }
	  
}
if($plugin_value){
	$page_data = $define_node->get_node_content($plugin_value,$lang_info->id);
 ?>

<div class='col-md-12 text-right give-mb-lg'>
<a href='content.php?lang=<?php echo "$lang&alias=$page_data->alias" ?>' class='col-md-12 give-darkRed-c '><?php echo $view_all_events; ?>  <span class='fa fa-long-arrow-right'></span></a>
</div>
<?php } ?>
