 <section class="col-md-12 give-padding-tb"><!--start of internal content-->
        <div class="container">
         <div class="row clearfix">
  <?php
//get page category
//show posts
$define_page_categories = new NodesSelectedTaxonomies();
$define_page_categories->enable_relation();
$page_categories = $define_page_categories->return_node_taxonomy($get_page_content->id,'page','category','many',$lang_info->id);
$page_categories_array = array();
if(count($page_categories) > 0){
	
  foreach($page_categories as $category){
	  $page_categories_array[$category->id] = $category->taxonomy_name;
  }
  //get categories ids
  $page_categories_ids = array();
  foreach($page_categories_array as $key=>$value){
	  $page_categories_ids [] = $key;
  }
  
   $count = count($page_categories_ids);
   $index = 1;
   $categories = implode(",",$page_categories_ids);
   //count all posts in the page
   
  // get categories titles and list it as tabs for filtration  : 
  $categories_data = $define_taxonomy_class->front_taxonomies_data(null,'category',null,null,'ASC',
	                 'many',$lang_info->id,11,null,null,null); 


        // begin of ul nav as categories  to filtrate gelleries  

        echo "<ul class='nav nav-tabs  internal-gallery-tabs' role='tablist'>" ;        
        $category_active = 0 ; 
        foreach($categories_data as $get_one_category){
            echo " <li role='presentation' class=' " ;
            if($category_active == 0 ){
              echo " active  ";
            }  
            echo " '> " ; 
            echo "<a href='#$get_one_category->id' aria-controls='profile' role='tab' data-toggle='tab'>$get_one_category->name </a></li>";
            $category_active++;  
           
        }

        echo "</ul>"; 

 
  
  ?>
  
    
<?php
 // start of gallery content 
 echo "<div class='tab-content internal-gallery-content'>" ; 
 $gallery_index = 0 ; 
 
  foreach($categories_data as $get_one_category){


       	   	 echo "<div role='tabpanel' class='row clearfix tab-pane "; 
                if($gallery_index == 0 ){
                  echo " active "; 
                }
                echo " ' "; 
                echo "  id='$get_one_category->id'>";



             

              $sub_categories = $define_taxonomy_class->front_taxonomies_data(null,'category',null,null, null,
                   'many',$lang_info->id,$get_one_category->id,null,null,null); 

              foreach ($sub_categories as $one) {

               
                // echo $one->id;
                
                // here i will get all sub sub categories inside each sub category for example  : 
                // Gallery 1 is sub category from Gallery the main category and inside Gallery 1  
                // there are 4 sub sub category and if we change the tab to gallery 2  it will fetch the sub sub under 
                // Gallery 2 sub category and so on  ! 
                

               // loop through posts : 
               $List_posts_for_blog = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$one->id, null,null,null,"many",null,null);

               foreach ($List_posts_for_blog as $post) {

                    // i will try to fetch posts categories  in array  
                    $posts_categories =  $define_page_categories->return_node_taxonomy($post->id,'post','category','many',$lang_info->id);

                    $post_categories_array = array();
                    if(count($posts_categories)>0){

                      $post_categories_ids = array();
                      foreach($post_categories_array as $key=>$value){
                        $post_categories_ids [] = $key;
                      }

                       $count = count($page_categories_ids);
                       $sub_categories = implode(",",$page_categories_ids);
                                       


                   
                   // check to see if post has cover image or 
                   if($post->cover_image){
                     $image_path = get_image_src($post->cover_image,"large");
                    }else{
                      $image_path = "";
                    }


                      if($one->id == 12 || $one->id == 19){

                          $find_images_for_post = $define_image_gallery->get_images($post->id); 
                             $count_images = count($find_images_for_post) ; 
                         
                              echo " <div class='col-md-5 internal-gallery-item internal-gallery-left'>
                                  <a href='$image_path'  data-fresco-group='$post->id' class='fresco' style='background-image: url($image_path);'>
                                    <div class='col-md-12 give-semi-gradient give-absolute-full '></div>
                                    <div class='col-md-12 give-absolute-rlb give-padding-reg main-offers-caption'>
                                      <h3 class='col-md-12 give-float-left'>$post->title</h3>
                                      <p class='col-md-12 give-float-right'>$count_images</p>
                                    </div>
                                  </a>";          
                                echo "<div>"; 
                                foreach($find_images_for_post as $image){
                                  echo "<a href='media-library/$image->image'  data-fresco-group='$post->id' class='fresco'></a>"; 
                                }
                                echo "</div>"; 
                            echo "</div>"; 

                     }

                     if($one->id == 16 || $one->id == 20){

                          echo "<div class='col-md-7 internal-gallery-item internal-gallery-right'>";
                           $find_images_for_post = $define_image_gallery->get_images($post->id); 
                                $count_images = count($find_images_for_post) ; 
                              echo " <div class='col-md-12 internal-gallery-item internal-gallery-half'>
                              <a href='$image_path'  data-fresco-group='$post->id' class='fresco' style='background-image: url($image_path);'>
                                <div class='col-md-12 give-semi-gradient give-absolute-full '></div>
                                <div class='col-md-12 give-absolute-rlb give-padding-reg main-offers-caption'>
                                  <h3 class='col-md-12 give-float-left'>$post->title</h3>
                                  <p class='col-md-12 give-float-right'>$count_images</p>
                                </div>
                              </a>"; 
                                echo "<div>";  
                                foreach($find_images_for_post as $image){
                                  echo "<a href='media-library/$image->image'  data-fresco-group='$post->id' class='fresco'></a>"; 
                                }
                                echo "</div>"; 
                            echo "</div>"; 



                      }


                      if($one->id == 17 || $one->id == 21){

                         
                           $find_images_for_post = $define_image_gallery->get_images($post->id); 
                                $count_images = count($find_images_for_post) ; 
                              echo " <div class='col-md-12 internal-gallery-item internal-gallery-half'>
                              <a href='$image_path'  data-fresco-group='$post->id' class='fresco' style='background-image: url($image_path);'>
                                <div class='col-md-12 give-semi-gradient give-absolute-full '></div>
                                <div class='col-md-12 give-absolute-rlb give-padding-reg main-offers-caption'>
                                  <h3 class='col-md-12 give-float-left'>$post->title</h3>
                                  <p class='col-md-12 give-float-right'>$count_images</p>
                                </div>
                              </a>"; 
                                echo "<div>";  
                                foreach($find_images_for_post as $image){
                                  echo "<a href='media-library/$image->image'  data-fresco-group='$post->id' class='fresco'></a>"; 
                                }
                                echo "</div>"; 
                            echo "</div>"; 
                          echo "</div>";
                          


                      }


                    }
                
                 }




              }

                    echo "</div>";
          $gallery_index++; 



               
           

               
			 
		  }//end of foreach
       
      echo "</div>";

		  
 }//end of page_categories
 ?>





 


    </div>
 </section>

