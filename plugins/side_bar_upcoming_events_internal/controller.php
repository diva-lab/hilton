<?php
	//get plugin data
	$get_plugin_id = Plugins::find_by_custom_filed('source','side_bar_upcoming_events_internal');
    $get_plugin_value_for_plugin = NodesPluginsValues::get_plugin_values($get_plugin_id->id,$lang->id,$node_id,$node_type);
	//get all menu group
	$define_node_class = new Nodes();
	$define_node_class->enable_relation();
	$all_pages = $define_node_class->front_node_data(null,"page",null,null,$lang->id,null,null,null,null, null,null,null,"many");
?>
<div class="form-group">
<label class="col-lg-3">Section Title-<?php echo ucfirst($lang->label)?>:</label>
<div class="col-lg-4">
<input type="text" class="form-control" name="side_bar_upcoming_events_internal_title_<?php echo $lang->label?>" value="<?php  if($get_plugin_value_for_plugin->title){ echo $get_plugin_value_for_plugin->title;}?>" autocomplete="off">
</div>
</div>
<div class="form-group">
<label class="col-lg-3">Select All events Page-<?php echo ucfirst($lang->label)?>:</label>
<div class="col-lg-4">
<select class="form-control" name="side_bar_upcoming_events_internal_<?php echo $lang->label?>">
<option value="0">Select Page</option>
<?php
 foreach($all_pages as $page){
     echo "<option value='$page->id'";
	 if($page->id == $get_plugin_value_for_plugin->content){
		 echo " selected";
	 }
     echo ">$page->title</option>";
 }?>
</select>
</div>
</div>