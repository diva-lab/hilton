 <?php 
  $lang = $_GET['lang'] ;
  ?>
  
           
            <div class="col-md-6 internal-contact">

              <h2 class="col-md-12 give-title-slogan give-display-inline"><?php echo $contact_information ;  ?></h2>
              <ul class="col-md-12 footer-list give-nolist internal-contact-list">
                <li><p class="fa fa-envelope give-btn-c"></p><?php echo $header_info_mail ;  ?></li>
                <li><p class="fa fa-phone give-btn-c"></p><?php echo $header_info_phone ;  ?></li>
                <li class="col-md-12 give-flex"><p class="fa fa-map-marker give-btn-c"></p><?php echo strip_tags($get_page_content->body) ;  ?></li>
              </ul>
            </div>

            <div class="col-md-6 internal-contact">
              <h2 class="col-md-12 give-title-slogan give-display-inline"><?php echo $send_quick_message ;  ?></h2>
             <form action="main/data_model/contact_us.php" class="col-md-12 main-form contact-form" id="contact_us_form">
              <input type="hidden" id="lang" value="<?php echo $lang ;  ?>">
               <div class="form-group">
                 <input type="text" class="form-control" id="name" placeholder="<?php echo $name ; ?>"> 
               </div>

               <div class="form-group">
                 <input type="text" class="form-control" id="email" placeholder="<?php echo $email ; ?>"> 
               </div>

                <div class="form-group">
                 <textarea type="text" class="form-control" id="message" placeholder="<?php echo $message ;  ?>"></textarea>
               </div>

               <div class="form-group">
                 <button type="submit" id="send_message"  class="btn give-nav-bg"><?php echo $submit ;  ?></button>
                 <br>
                 <br>
                 <div id="info_message"></div>
               </div>
               
             </form>
            
            </div>
             
            <div class="col-md-all internal-map">
              <iframe src="<?php echo strip_tags($get_page_content->summary) ;  ?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
              <img src="media-library/<?php echo $get_page_content->cover_image ;  ?>" alt="" class="col-md-12 give-absolute-lb">
            </div>