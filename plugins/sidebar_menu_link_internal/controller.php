<?php
	//get plugin data
	$get_plugin_id = Plugins::find_by_custom_filed('source','sidebar_menu_link_internal');
    $get_plugin_value = NodesPluginsValues::get_plugin_values($get_plugin_id->id,$lang->id,$node_id,$node_type);
	//get all menu group
	$define_menu_class = new MenuGroup();
	$all_menu_groups = $define_menu_class->menu_group_data('inserted_date','DESC');
?>
<div class="form-group">
<label class="col-lg-3">Menu Group Title-<?php echo ucfirst($lang->label)?>:</label>
<div class="col-lg-4">
<input type="text" class="form-control" name="sidebar_menu_link_internal_title_<?php echo $lang->label?>" value="<?php  if($get_plugin_value->title){ echo $get_plugin_value->title;}?>" autocomplete="off">
</div>
</div>
<div class="form-group">
<label class="col-lg-3">Select Menu Group-<?php echo ucfirst($lang->label)?>:</label>
<div class="col-lg-4">
<select class="form-control" name="sidebar_menu_link_internal_<?php echo $lang->label?>">
<option value="0">Select Menu Group</option>
<?php
 foreach($all_menu_groups as $menu){
     echo "<option value='$menu->id'";
	 if($menu->id == $get_plugin_value->content){
		 echo " selected";
	 }
     echo ">$menu->title</option>";
 }?>
</select>
</div>
</div>