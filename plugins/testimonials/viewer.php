 <section class="col-md-12 give-padding-tb"><!--start of internal content-->
        <div class="container">  
         <div class="row clearfix">

<?php 
// show all posts under one page  
//get page category
//show posts
$define_page_categories = new NodesSelectedTaxonomies();
$define_page_categories->enable_relation();
$page_categories = $define_page_categories->return_node_taxonomy($get_page_content->id,'page','category','many',$lang_info->id);
$page_categories_array = array();
if(count($page_categories) > 0){
	
  foreach($page_categories as $category){
	  $page_categories_array[$category->id] = $category->taxonomy_name;
  }
  //get categories ids
  $page_categories_ids = array();
  foreach($page_categories_array as $key=>$value){
	  $page_categories_ids [] = $key;
  }
  
   $count = count($page_categories_ids);
   $index = 1;
  $categories = implode(",",$page_categories_ids);
   //count all posts in the page
   
  $page_all_post = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$categories, null,null,null,"many");
  //total posts
  $total_posts = count($page_all_post);
  //get page id if exist
  $page_number = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
 //number of posts in one page
  $per_page =6 ;
  $pagination = new Pagination($page_number , $per_page, $total_posts);
  //calculate the offset
  $offset = $pagination->offset();
  $count = $offset+1;
  $List_posts_for_blog = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$categories, null,null,null,"many",$per_page,$offset);
  
 
 

  foreach($List_posts_for_blog as $single_post){

      $summary = strip_tags($single_post->summary) ; 
      $body    = strip_tags($single_post->body) ; 


      $limit_words = string_limit_words($summary , 20) ; 

      $image_src = get_image_src($single_post->cover_image,"medium");
			  if($lang == "en"){
						  $date = english_date($single_post->start_publishing);
			 }else{
						  $date = arabic_date($single_post->start_publishing);
		     }
			 if($single_post->cover_image){
				 $image_path = get_image_src($single_post->cover_image,"large");
			 }else{
				 $image_path = "";
			 }
       if($lang =="en"){
	        $post_date = english_date($single_post->start_publishing);
        }else{
          $post_date = arabic_date($single_post->start_publishing);
        }


        

      echo " <article class='clearfix main-post'>
                <a href='post_details.php?lang=$lang&alias=$single_post->alias' class='col-md-4 main-post-image give-border-radius' style='background-image: url($image_path);'></a>
                   <div class='col-md-8 main-post-text'>
                    <h3><a href='post_details.php?lang=$lang&alias=$single_post->alias'>$single_post->title </a></h3>
                    <p class='col-md-12 give-btn-c'>$post_date</p>
                    <p class='col-md-12 give-text-c'>$limit_words<?p>
                  </div>
                
              </article>" ; 

  }// end of foreach for posts  


}// end for foreach for categories 



?>

               
           </div><!-- end of div that holds all posts  -->


        <?php 
        echo "<nav class='col-md-12 pagination-wrap'>";
        if($pagination->total_pages() > 1){
        echo "<ul class='pagination'>";
        if($pagination->has_previous_page()){
          $previous_page = $pagination->previous_page();
          
          echo "<li>
              <a href='content.php?lang=$lang&alias=$node_alias&page=$previous_page' aria-label='Previous'>
              <span class='fa fa-caret-left'></span> 
              </a>
            </li>"; 	
        } 
        for($i=1; $i <= $pagination->total_pages(); $i++) {
            if($i == $page_number){
            $active = "  active";
            }else{
            $active = " ";
            }
        echo "<li class='$active'><a href='content.php?lang=$lang&alias=$node_alias&page={$i}'>$i</a></li>";


        } 

        // check next page
        if($pagination->has_next_page()) { 
        $next_page = $pagination->next_page();
        echo "<li><a href='content.php?lang=$lang&alias=$node_alias&page=$next_page' aria-label='Next'>
              <span class='fa fa-chevron-right'></span></a></li>";
        }
        echo "</ul>";  	

        } 
        echo "</nav>"; 
        ?>


        </div>
      </section><!--end of internal content-->