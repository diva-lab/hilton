<section class="col-m-12 main-content-section"><!--start of main content section-->

               <header class="clearfix give-mb-med main-content-header"><!--start of main content section header-->
                <h1 class="col-md-12 give-boldFamily give-title-border give-maintitle-border pull-left">Latest Databases Resources</h1>
                <a href="" class="col-md-12 give-red-c pull-right">View all events <span class="fa fa-long-arrow-right"></span></a>
              </header><!--start of main content section header-->

              <article class="clearfix give-padding give-gray-bg primary-post database-post"><!--start of database post-->
                  <a href="" class="col-md-3 primary-image" style="background-image: url('main/images/dummy-image.jpg');"></a>
                  <div class="col-md-9 primary-text">
                    <h4 class="col-md-12 give-darkRed-c give-boldFamily"><a href="">A Review of the UK's Interdisciplinary Research using a Citation-based Approach</a></h4>
                    <p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking    </p>
                  </div>
                   <ul class="col-md-12 post-meta give-red-bg row give-padding-sm">
                      <li title="Author Name">
                          <p class="fa fa-user give-yellow-c"></p> Autor Name
                      </li>
                       <li title="Published">
                          <p class="fa fa-calendar give-yellow-c"></p> 12/1/2014
                      </li>
                       <li title="Share">
                           <a href=""><span class="fa fa-share-square-o give-yellow-c"></span> Share</a>
                      </li>
                       <li title="Views">
                          <p class="fa fa-eye give-yellow-c"></p> 300
                      </li>
                       <li title="Save">
                          <a href=""><span class="fa fa-heart give-yellow-c"></span> Save</a>
                      </li>
                      <li class="col-md-12 post-read">
                          <a href="" class="btn give-darkRed-bg " target="_blank">Read More</a>
                      </li>
                   </ul>
              </article><!--end of database post-->
               <article class="clearfix give-padding give-gray-bg primary-post database-post"><!--start of database post-->
                  <a href="" class="col-md-3 primary-image" style="background-image: url('main/images/dummy-image.jpg');"></a>
                  <div class="col-md-9 primary-text">
                    <h4 class="col-md-12 give-darkRed-c give-boldFamily"><a href="">A Review of the UK's Interdisciplinary Research using a Citation-based Approach</a></h4>
                    <p>The NCSU Libraries and NC State College of Sciences will host a talk by scientists from the Smithsonian Astrophysical Observatory (SAO) about the breathtaking  </p>
                  </div>
                   <ul class="col-md-12 post-meta give-red-bg row give-padding-sm">
                      <li title="Author Name">
                          <p class="fa fa-user give-yellow-c"></p> Autor Name
                      </li>
                       <li title="Published">
                          <p class="fa fa-calendar give-yellow-c"></p> 12/1/2014
                      </li>
                       <li title="Share">
                           <a href=""><span class="fa fa-share-square-o give-yellow-c"></span> Share</a>
                      </li>
                       <li title="Views">
                          <p class="fa fa-eye give-yellow-c"></p> 300
                      </li>
                       <li title="Save">
                          <a href=""><span class="fa fa-heart give-yellow-c"></span> Save</a>
                      </li>
                      <li class="col-md-12 post-read">
                          <a href="" class="btn give-darkRed-bg " target="_blank">Read More</a>
                      </li>
                   </ul>
              </article><!--start of database post-->
            </section>