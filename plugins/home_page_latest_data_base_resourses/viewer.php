<section class="col-m-12 main-content-section"><!--start of main content section-->
               <header class="clearfix give-mb-med main-content-header"><!--start of main content section header-->
                <h1 class="col-md-12 give-boldFamily give-title-border give-maintitle-border pull-left">Latest Databases resources</h1>
                <a href="" class="col-md-12 give-red-c pull-right">View all events <span class="fa fa-long-arrow-right"></span></a>
              </header><!--end of main content section header-->

              <div class="row clearfix"><!--start of row-->
                
                  <article class="col-md-4 database-cover">
                    <a href="" class="col-md-12 database-cover-image give-border" style="background-image: url('main/images/database-cover.jpg');"></a>
                    <div class="col-md-12 database-cover-text give-gray-bg give-padding">
                        <h2 class="col-md-12 give-red-c give-boldFamily"><a href="">Book Title</a></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                         <ul class="col-md-12 post-meta give-block">
                           <li title="Author">
                              <a href=""><span class="fa fa-user"></span> Author Name</a>
                          </li>
                           <li title="Date">
                              <p class="fa fa-calendar"></p> 31/5/2016
                          </li>
                           <li title="isbn">
                              <a href=""><span class="fa fa-book"></span> #145523</a>
                          </li>
                          <li title="Views">
                              <p class="fa fa-eye"></p> 300
                           </li>
                        </ul>
                    </div>
                  </article>
                      <article class="col-md-4 database-cover">
                    <a href="" class="col-md-12 database-cover-image give-border" style="background-image: url('main/images/database-cover.jpg');"></a>
                    <div class="col-md-12 database-cover-text give-gray-bg give-padding">
                        <h2 class="col-md-12 give-red-c give-boldFamily"><a href="">Book Title</a></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                         <ul class="col-md-12 post-meta give-block">
                           <li title="Author">
                              <a href=""><span class="fa fa-user"></span> Author Name</a>
                          </li>
                           <li title="Date">
                              <p class="fa fa-calendar"></p> 31/5/2016
                          </li>
                           <li title="isbn">
                              <a href=""><span class="fa fa-book"></span> #145523</a>
                          </li>
                          <li title="Views">
                              <p class="fa fa-eye"></p> 300
                           </li>
                        </ul>
                    </div>
                  </article>
                      <article class="col-md-4 database-cover">
                    <a href="" class="col-md-12 database-cover-image give-border" style="background-image: url('main/images/database-cover.jpg');"></a>
                    <div class="col-md-12 database-cover-text give-gray-bg give-padding">
                        <h2 class="col-md-12 give-red-c give-boldFamily"><a href="">Book Title</a></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                         <ul class="col-md-12 post-meta give-block">
                           <li title="Author">
                              <a href=""><span class="fa fa-user"></span> Author Name</a>
                          </li>
                           <li title="Date">
                              <p class="fa fa-calendar"></p> 31/5/2016
                          </li>
                           <li title="isbn">
                              <a href=""><span class="fa fa-book"></span> #145523</a>
                          </li>
                          <li title="Views">
                              <p class="fa fa-eye"></p> 300
                           </li>
                        </ul>
                    </div>
                  </article>
                      <article class="col-md-4 database-cover">
                    <a href="" class="col-md-12 database-cover-image give-border" style="background-image: url('main/images/database-cover.jpg');"></a>
                    <div class="col-md-12 database-cover-text give-gray-bg give-padding">
                        <h2 class="col-md-12 give-red-c give-boldFamily"><a href="">Book Title</a></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                         <ul class="col-md-12 post-meta give-block">
                           <li title="Author">
                              <a href=""><span class="fa fa-user"></span> Author Name</a>
                          </li>
                           <li title="Date">
                              <p class="fa fa-calendar"></p> 31/5/2016
                          </li>
                           <li title="isbn">
                              <a href=""><span class="fa fa-book"></span> #145523</a>
                          </li>
                          <li title="Views">
                              <p class="fa fa-eye"></p> 300
                           </li>
                        </ul>
                    </div>
                  </article>
                     <article class="col-md-4 database-cover">
                    <a href="" class="col-md-12 database-cover-image give-border" style="background-image: url('main/images/database-cover.jpg');"></a>
                    <div class="col-md-12 database-cover-text give-gray-bg give-padding">
                        <h2 class="col-md-12 give-red-c give-boldFamily"><a href="">Book Title</a></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                         <ul class="col-md-12 post-meta give-block">
                           <li title="Author">
                              <a href=""><span class="fa fa-user"></span> Author Name</a>
                          </li>
                           <li title="Date">
                              <p class="fa fa-calendar"></p> 31/5/2016
                          </li>
                           <li title="isbn">
                              <a href=""><span class="fa fa-book"></span> #145523</a>
                          </li>
                          <li title="Views">
                              <p class="fa fa-eye"></p> 300
                           </li>
                        </ul>
                    </div>
                  </article>
                     <article class="col-md-4 database-cover">
                    <a href="" class="col-md-12 database-cover-image give-border" style="background-image: url('main/images/database-cover.jpg');"></a>
                    <div class="col-md-12 database-cover-text give-gray-bg give-padding">
                        <h2 class="col-md-12 give-red-c give-boldFamily"><a href="">Book Title</a></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                         <ul class="col-md-12 post-meta give-block">
                           <li title="Author">
                              <a href=""><span class="fa fa-user"></span> Author Name</a>
                          </li>
                           <li title="Date">
                              <p class="fa fa-calendar"></p> 31/5/2016
                          </li>
                           <li title="isbn">
                              <a href=""><span class="fa fa-book"></span> #145523</a>
                          </li>
                          <li title="Views">
                              <p class="fa fa-eye"></p> 300
                           </li>
                        </ul>
                    </div>
                  </article>

             </div><!--end of row-->
            </section>