<?php

    $title = "";

	$summary = "";

	if($node_type == "page"){

		$node_id = $get_page_content->id;

		$node_layout = $page_layout_info->model;

		$title =  $get_page_content->title;

		

	 }else if($node_type == "post"){

		$node_id = $get_post_content->id;

		$node_layout = $post_layout_info->model;

		$title =  $get_post_content->title;

		$category = $define_node_tax->return_node_taxonomy($get_post_content->id,'post','category','one',$lang_info->id);

		// $page_data = $define_node_tax->return_taxonomy_nodes($category->id,"page","category",'one',$lang_info->id);

	}else{

		$node_id = $get_product_content->id;

		$node_layout = $product_layout_info->model;

		$title =  $get_product_content->title;

		 

	}

	

?> 

   <section class="col-md-12 main-hero main-cover text-center" style="background-image: url('main/images/slide.jpg');"><!--start of internal cover-->
   	 <div class="col-md-12 give-absolute-full give-semi-black"></div>
        <div class="container">

          <h1 class="col-md-12 give-white-c give-title-slogan"><?php  echo $title ; ?></h1>

          <ol class="breadcrumb">

            <li ><a href='index.php?lang=<?php echo $lang ;  ?>'><?php echo $home; ?></a></li>

            <li class="active "><?php  echo $title ; ?></li>

          </ol>

        </div>

      </section><!--end of internal cover-->

        <section class="col-md-12 give-padding-tb"><!--start of internal content-->
            <div class="container">
                 <div class="row clearfix">

            <?php 

				$define_theme_left_plugins = new ThemeLayoutModelPlugin();
				 $define_theme_left_plugins->enable_relation();
				 $left_plugins = $define_theme_left_plugins->front_get_model_plugin($node_layout, 'left');

				foreach($left_plugins as $plugin){

					require_once("plugins/".$plugin->plugin_source."/viewer.php");		

				}

            ?>

                </div>
            </div>
        </section><!--end of internal content-->

 