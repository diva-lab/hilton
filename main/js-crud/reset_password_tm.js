jQuery(document).ready(function($) {

	// process the form

	$('#reset_password_form').submit(function() {

		 $('#reset_pass_btn').attr('disabled', 'disabled'); 

		var formData = {

			lang: $('#lang').val(), 

			code : $('#code').val(),  

			new_password : $('#password').val()  


		};

		// process the form

		$.ajax({

			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)

			url 		: 'main/data_model/reset_password_tm.php', // the url where we want to POST

			data 		: formData, // our data object

			success : function(data) {
				 
		 
				 
				if(data.status == 'work'){


					$("#general_info_reset").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
					 window.location.href = "index.php?lang="+$('#lang').val(); 
				}else if (data.status == 'password_len'){
					$("#general_info_reset").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					 $('#reset_pass_btn').removeAttr('disabled', 'disabled'); 
				}else{
					$("#general_info_reset").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					}

	   
        
				

			}

		});

	return false;

	});

	

	

		

});