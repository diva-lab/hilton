jQuery(document).ready(function($) {

	// process the form

	$('#education_form').submit(function() {

		$('#save_education').attr('disabled', 'disabled'); 

		var formData = {

			lang: $('#lang').val(), 

			customer_id : $('#customer_id').val(),  

			university : $('#university').val(),  

			education_level : $('#education_level').val(),  

			field_of_study : $('#field_of_study').val(), 

			from_date : $('#datetimepicker6').val() , 

			to_date : $('#datetimepicker7').val() 


		};

		// process the form

		$.ajax({

			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)

			url 		: 'main/data_model/insert_education.php', // the url where we want to POST

			data 		: formData, // our data object

			success : function(data) {
				 
				 
			 
				if(data.status == 'work'){

					$("#notify_education").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");

					//$('#general_info')[0].reset();

				}else if (data.status == 'not_work'){
					$("#notify_education").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#save_education').removeAttr('disabled'); 
				}else if(data.status == 'email_exists'){
					$("#notify_education").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#save_education').removeAttr('disabled'); 
				}else if(data.status == 'pass_length'){
					$("#notify_education").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#save_education').removeAttr('disabled');
				}else{

					$("#notify_education").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");

				}

				 
			 
	       

			}

		});

	return false;

	});

	

	

		

});