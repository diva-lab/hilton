jQuery(document).ready(function($) {

	// process the form

	$('#forget_password_form').submit(function() {

		 $('#forget_pass_btn').attr('disabled', 'disabled'); 

		var formData = {

			lang: $('#lang').val(), 	 

			email : $('#email').val()  


		};

		// process the form

		$.ajax({

			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)

			url 		: 'main/data_model/forget_password.php', // the url where we want to POST

			data 		: formData, // our data object

			success : function(data) {
 
				if(data.status == 'work'){


					$("#general_info").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
					 window.location.href = "index.php?lang="+$('#lang').val(); 
				}else if (data.status == 'wrong_mail'){
					$("#general_info").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					 $('#forget_pass_btn').removeAttr('disabled', 'disabled'); 
				}else{
					$("#general_info").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					}
 
 

				

			}

		});

	return false;

	});

	

	

		

});