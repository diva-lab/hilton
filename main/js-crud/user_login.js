jQuery(document).ready(function($) {
	$('#login').submit(function() {
		//$('#submit').attr("disabled", "true");
		var formData = {
			lang: $('#lang').val(),
			email: $('#login_email').val(), 
			pass: $('#login_pass').val(),
		};
		
		// process the form
		$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: 'main/data_model/user_login.php', // the url where we want to POST
			data 		: formData, // our data object
			beforeSend: function(){
			//show laoding 
			$('#validation_message').html('<img src="main/images/loading.gif"/>');
			},
			success : function(data) {
				if(data.status == 'work'){
					window.location.reload(); 
				 }else{
					$("#validation_message").html("");
					$("#validation_message").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >")
				}
			}
		});
		return false;
	});
});