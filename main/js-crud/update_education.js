jQuery(document).ready(function($) {

	// process the form

	$('#education_form_update').submit(function() {

		 $('#update_education').attr('disabled', 'disabled'); 

		var formData = {

			lang: $('#lang').val(), 

			education_id : $('#education_id').val(),  

            university : $('#university').val(),  

			education_level : $('#education_level').val(),  

			field_of_study : $('#fileds_of_study').val(), 

			from_date : $('#datetimepicker6').val() , 

			to_date : $('#datetimepicker7').val() 


		};

		// process the form

		$.ajax({

			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)

			url 		: 'main/data_model/update_education.php', // the url where we want to POST

			data 		: formData, // our data object

			success : function(data) {
				 
				  
				if(data.status == 'work'){

					$("#updating_current_education").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
					window.location.href = "user-profile.php?lang="+$("#lang").val(); 
				 
					 

				}else if (data.status == 'not_work'){
					$("#updating_current_education").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#update_education').removeAttr('disabled'); 
				}else if(data.status == 'email_exists'){
					$("#updating_current_education").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#update_education').removeAttr('disabled'); 
				}else if(data.status == 'pass_length'){
					$("#updating_current_education").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#update_education').removeAttr('disabled');
				}else{

					$("#updating_current_education").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");

				}
 
			

				

			}

		});

	return false;

	});

	

	

		

});