 <section class='col-md-12 main-offers give-padding-tb'><!--start of offers-->
        <div class='container'>
        <?php 
        $this_part_id = 5 ; 
        $special_offers_info = $define_node->get_node_content($this_part_id , $lang_info->id); 

        $main_offers_title = $special_offers_info->title ; 
        ?>
          <h1 class='col-md-12 give-title-slogan text-center'><?php echo $main_offers_title ;  ?></h1>
          <div class='clearfix'>

            <!-- this is main special offers --> 
            <?php 
            /* we will get the most recent post with category main special offer */
            $main_special_offers_cat_id = 1;  
            $find_special_offers =  $define_node->front_node_data(null,'post',null,null,$lang_info->id,'yes',null,null,$main_special_offers_cat_id,null,null,null,'many',1,null);

            foreach($find_special_offers as $offer){

             echo "<article class='col-md-4 main-offers-item main-offers-left' style='background-image: url(media-library/$offer->cover_image);'>
                <div class='col-md-12 give-semi-blue give-absolute-full give-padding-reg text-center main-offers-caption'>"; 
                echo "<h1 class='col-md-12 give-mb-sm'> $offer->title</h1>";
                $summary_clean = strip_tags( $offer->summary); 
                echo "<h3>$summary_clean</h3>";
                echo "<div class='fa fa-angle-double-down '></div>" ; 
                // get body text and explode it  to get your variables  
                $body_text = strip_tags($offer->body); 
                $explode_body = explode('-',$body_text) ; 
                $offer_description = $explode_body[0]; 
                $offer_start_text = $explode_body[1]; 
                $offer_price = $explode_body[2] ;      
                echo " <p>$offer_description</p>"; 
                echo "<div class='col-md-12 give-title-slogan2 give-mb-sm'></div>"; 
                echo "<h2 class='col-md-12 give-mb-sm'>$offer_start_text</h2>"; 
                echo "<h1 class='col-md-12 give-mb-sm'>$offer_price</h1>"; 
                echo "<a href='' class='btn give-nav-bg'> $book_now</a>";  
                echo"</div> </article>"; 


            }
             
            ?>
            <!-- end of main special offer --> 


            <!-- begin of normal offers  --> 
            <div class='col-md-8  main-offers-item main-offers-right'>

                <div class='clearfix main-offers-item main-height-half '>

                <?php 
                   $normal_offers_cat_id = 2;  
                   $find_normal_offers =  $define_node->front_node_data(null,'post',null,null,$lang_info->id,'yes',null,null,$normal_offers_cat_id,null,null,null,'many',2,null);
                     foreach($find_normal_offers as $normal_offer){

                          echo " <article class='col-md-6 main-offers-item main-offers-half'>
                                 <a href='post_details.php?lang=$lang&alias=$normal_offer->alias' class='col-md-12 main-offers-item-link' style='background-image: url(media-library/$normal_offer->cover_image);'>";
                          echo " <div class='col-md-12 give-semi-triangle give-absolute-rb '></div>" ; 
                          echo " <div class='col-md-12 give-absolute-rb give-padding-reg text-right main-offers-caption'>
                                   <p>$normal_offer->title</p>";
                                 $clean_normal_summary = strip_tags($normal_offer->summary) ; 
                          echo "<div class='col-md-12 give-title-slogan2'></div>
                                     <h3>$clean_normal_summary</h3>
                                 </div>";
                          echo "</a> </article>" ; 

                     }
                ?>
                
                </div>
                <!-- end of normal offers  --> 

                <!-- begin news offers main height half post -->
                <?php 
                 $news_offers_cat_id = 3;  
                   $find_news_offers =  $define_node->front_node_data(null,'post',null,null,$lang_info->id,'yes',null,null,$news_offers_cat_id,null,null,null,'many',1,null);

                   foreach($find_news_offers as $news_offer){
                     echo "  <article class='col-md-12 main-offers-item main-height-half '>
                              <a href='post_details.php?lang=$lang&alias=$news_offer->alias' class='col-md-12 main-offers-item-link' style='background-image: url(media-library/$news_offer->cover_image);'>"; 
                     echo " <div class='col-md-12 give-semi-gradient give-absolute-full '></div>" ; 
                     
                     echo " <div class='col-md-12 give-absolute-rlb give-padding-reg main-offers-caption'>
                                <h3 class='col-md-12 give-no-margin'>$news_offer->title</h3>";
                     $news_description_cleaned = strip_tags($news_offer->summary) ; 
                     echo "     <p class='col-md-12 give-no-margin'>$news_description_cleaned</p>
                             </div>";
                     echo "  </a>
                            </article>" ; 
                   }
                
                ?>
                <!-- end of news offers main height half post -->
            </div>

          </div>
        </div>
      </section><!--end of offers-->