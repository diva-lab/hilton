<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="Classification" content="" />
    <meta name="RESOURCE-TYPE" content="DOCUMENT" />
    <meta name="DISTRIBUTION" content="GLOBAL" />
    <meta name="robots" content="all" />
    <meta name="revisit-after" content="5 days" />
    <meta name="rating" content="general" />
    <meta http-equiv="Content-Language" content="" />
    <meta name="author" content=""/>
  <?php
	 $main = "";
	if(isset($_GET["alias"])){
		$node_alias = $_GET["alias"];
		if($opened_url_file == "post_details.php"){
			   $post_info = $define_node->front_node_data(null,'post',$node_alias, null,$lang_info->id, null, null, null, null,  null,'one');
			   if(!empty($post_info)){
				   $main = $post_info;
				   if(!empty($post_info->cover_image)){
				          $image = $post_info->cover_image;
                  $parts = explode('/',$image);
                  $image_cover = $parts[count($parts)-1];
                  $folder = $parts[count($parts)-2];
                }else{
                  $folder = "";
                  $image_cover = "";
                }
				        $website_new_title = $post_info->title;
                echo "  <meta property='og:url' content='{$define_general_setting->site_url}post_details.php?lang=$lang;&alias=$post_info->alias' />
                <meta property'og:image' content='{$define_general_setting->site_url}/media-library-thumb/$folder/large/$image_cover '/>
                <meta property='og:description' content='".mb_substr(strip_tags($post_info->summary),0,110,'utf-8')."'/>
                <meta property='og:title' content='$post_info->title' />";
				      }

	        }else if($opened_url_file == "content.php"){
				$page_info =  $define_node->front_node_data(null,'page',$node_alias,null,$lang_info->id,null,null, null, null,null, 'one',null,null);
            if(!empty($page_info)){
              $main = $page_info;
              if($page_info->cover_image){
                $image = $page_info->cover_image;
                $parts = explode('/',$image);
                $image_cover = $parts[count($parts)-1];
                $folder = $parts[count($parts)-2];
              }else{
                $folder = "";
                $image_cover = "";
              }
					      $website_new_title = $page_info->title;
                echo "<meta property='og:url' content='{$define_general_setting->site_url}content.php?lang=$lang;&alias=$page_info->alias' />
                  <meta property'og:image' content='{$define_general_setting->site_url}/media-library-thumb/$folder/large/$image_cover '/>
                  <meta property='og:description' content='".mb_substr(strip_tags($page_info->summary),0,110,'utf-8')."'/>
                  <meta property='og:title' content='$page_info->title' />";
              }
			}

	}else{
		$website_new_title = $website_home_title ;
		?>
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?php echo $website_new_title;?>" />
        <meta property="og:description" content="<?php echo $define_general_setting->description;?>" />
        <meta property="og:url" content="<?php echo $define_general_setting->site_url;?>" />
        <meta property="og:site_name" content="<?php  echo $website_new_title ; ?>" />
	<?php }?>
<title> <?php echo $website_new_title;  ?> </title>

    <!-- Bootstrap -->
    <link href="main/css/bootstrap.min.css" rel="stylesheet">

    <!--font-awesome-->
    <link href="main/css/font-awesome.min.css" rel="stylesheet">
    <!-- datetime picker -->
    <link href="main/css/jquery.datetimepicker.css" rel="stylesheet">
     <!--pgwslideshow-->
    <link rel="stylesheet" href="main/css/pgwslideshow.min.css">

    <!--fresco-->
    <link rel="stylesheet" href="main/css/fresco.css">

    <!--Custom-->
    <link href="main/css/style-en.css" rel="stylesheet">
    <link href="main/css/responsive-en.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <header class="col-md-12 header"><!--start of header-->
       <div class="col-md-12 header-top give-padding-reg give-main-bg give-white-c"><!--start of header top-->
          <div class="container">
            <ul class="col-md-12 give-nolist header-top-contact">
            <?php  
              $this_part_id = 1 ; 
              $header_info = $define_node->get_node_content($this_part_id , $lang_info->id) ; 
              $header_info_mail = strip_tags($header_info->summary) ; 
              $header_info_phone = strip_tags($header_info->body) ; 
            ?>
              <li><p class="fa fa-envelope give-white-c2"></p><?php echo $header_info_mail ; ?></li>
              <li><p class="fa fa-phone give-white-c2"></p><?php echo $header_info_phone ;  ?></li>
            </ul>
          </div>
       </div><!--end of header top-->

       <nav class="col-md-12 header-middle give-padding-tb"><!--start of header middle-->
         <div class="container"><!--start of container-->

           <a href="index.php?lang=<?php echo $lang ; ?>" class="col-md-12 header-logo"><!--start of logo-->
             <img src="main/images/logo.png" alt="">
           </a><!--end of logo-->

           <div class="navbar-header"><!--start of navbar header-->
             <button class="navbar-toggle" data-target="#main-nav" data-toggle="collapse">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
             </button>
           </div><!--end of navbar header-->
           
           <div class="collapse navbar-collapse" id="main-nav"><!--start of main nav-->
             <ul class="nav navbar-nav">
               <li class="col-md-12 header-home">
                 <a href="index.php?lang=<?php echo $lang;?>" class="fa fa-home"></a>
               </li>

               <?php 
               $main_menu = $define_menu_link->menu_submenu_front_data("Sorting", "ASC" ,"main_menu",0, $lang_info->id); 
               if(count($main_menu)){
                 foreach($main_menu as $link){
                      $drop_down_menu =  $define_menu_link->menu_submenu_front_data("Sorting", "ASC" ,"main_menu",$link->id, $lang_info->id);
                      // check if there are any sub links inside one li 
                      if($drop_down_menu){
                          echo "<li class='dropdown'>
                                <a href='' class='dropdown-toggle' data-toggle='dropdown'>$link->title</a>
                                  <ul class='clearfix dropdown-menu'>
                                  <ul class='col-md-12 give-nolist give-float-left dropdown-menu-inner'>";
                                  foreach($drop_down_menu as $sub_link){
                                    	$path_link_data = $define_node->get_node_content($sub_link->path,$lang_info->id);
                                        $path = "";
                                        if($sub_link->path_type == "page"){
                                          $path = "content.php";
                                        }else if($sub_link->path_type == "post"){
                                          $path = "post_details.php";
                                        }else if($sub_link->path_type == "event"){
                                          $path = "event_details.php";
                                        }
                                        if($sub_link->path_type == "external"){
                                          echo "<li><a href='$link->external_path'>$sub_link->title</a></li>";
                                        }else{
                                          echo "<li><a href='$path?lang=$lang&alias=$path_link_data->alias'>$sub_link->title</a></li>";
                                        }
                                  }
                                  echo "</ul>";

                                  ?>
                                  <?php 
                                    // image in drop down to be dynamic and changable                              
                                    $image_url = $link->image ; 
                                    $explode = explode('/' , $image_url) ; 
                                    $image_title = $explode[1] ; 
         
                                  ?>
                                   <div class='col-md-12 dropdown-image give-float-right' style='background-image: url("media-library/main_menu/<?php echo $image_title ;  ?>");'></div>
                                  <?php echo "</ul></li>"; 
                      }else{

                            // else here means complete to list all links as they has no parent 
                            $path_link_data = $define_node->get_node_content($link->path,$lang_info->id);
                                        $path = "";
                                        if($link->path_type == "page"){
                                          $path = "content.php";
                                        }else if($link->path_type == "post"){
                                          $path = "post_details.php";
                                        }else if($link->path_type == "event"){
                                          $path = "event_details.php";
                                        }
                                        if($link->path_type == "external"){
                                          $get_class = $link->description ; 
                                          // strip p tag : 
                                          $class_stripped = strip_tags($get_class); 
                                          echo "<li><a href='$link->external_path' class='$class_stripped'>$link->title</a></li>";
                                        }else{
                                          $get_class = $link->description ; 
                                          // strip p tag : 
                                          $class_stripped = strip_tags($get_class); 
                                          echo "<li><a href='$path?lang=$lang&alias=$path_link_data->alias' class='$class_stripped'>$link->title</a></li>";
                                        } 


                                       

                      }


                 }
                                        // translate website button  : 
                                       
                                        echo " <li >" ; 
                                        if($lang=='en'){
                                          echo "<a href='$url_ar' class='btn give-btn-bg space'>Ar</a>"; 
                                        }else{
                                          echo "<a href='$url_en' class='btn give-btn-bg space'>EN</a>" ; 
                                        }

                 echo "</ul>"; 


               }
               
               ?>
              
             
           </div><!--start of main nav-->


         </div><!--end of container-->
       </nav><!--end of header middle-->
    </header><!--end of header-->