<div class="carousel slide" id="main-slider" data-ride='carousel'><!-- begin of slider div content  --> 
        <ol class="carousel-indicators"><!-- begin of ol holds carousel indicators -->
            <?php 
                $slider = $define_node->front_node_data(null,null,null,null,$lang_info->id,'yes','yes',null,null,null,null,null,'many',3,null); 
                if($slider){
                  $index = 0 ; 
                  if($slider){
                    foreach($slider as $slide){
                      echo "<li class = '" ; 
                      if($index == 0 ){
                        echo "active " ; 
                      }
                      echo " ' data-slide-to= '$index' data-target='#main-slider' ></li>";
                      $index++;  
                    }
                  }
                }
            ?>
        </ol><!-- end of ol -->


        <div class="carousel-inner"><!-- begin of carousel inner -->
            <?php 

              $index  = 0 ; 
              foreach($slider as $slide){

                $path = "" ; 
                if($slide->node_type == "page"){
                  $path = "content.php" ; 
                }else if ($slide->node_type == "post"){
                  $path = "post_details.php" ; 
                }else if($slide->node_type == "event"){
                  $path = "event_details.php" ; 
                }
                echo "<div class = ' item " ; 
                if($index == 0){
                  echo "active" ; 
                }
                echo " ' style='background-image:url(media-library/slider/$slide->slider_cover);' >"; 

                echo "<div class='col-md-12 main-hero-caption give-semi-black give-padding-reg' >" ; 
                echo "<h1 class='col-md-12 give-btn-c'>$slide->title</h1>" ; 

                $description = strip_tags($slide->summary) ; 
                $cut_description = string_limit_words($description , 30) ; 
                echo "<p class='col-md-12 give-white-c'>$cut_description</p>" ; 
                echo "</div>"; 
                echo "</div>"; 
                $index++;

              }


            ?>
         
        </div><!-- end of carousel inner div -->
      </div><!-- end of carousel div that holds all slider content  --> 
 



  
