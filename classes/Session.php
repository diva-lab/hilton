<?php 
class Session { 
    public $user_id; 
	public $user_name; 
    private $user_login = false; 
	public $lang_id; 
     
    //check if usr login or not 
    public function is_logged(){ 
        return $this->user_login; 
    } 
     
    //when call class restart session and run check login method 
    public function __construct() { 
        session_start(); 
        $this->check_login(); 
    } 
     
    //get user object and save it in user_id, session user id  
    public function Login($user){ 
      if($user){ 
          $this->user_id = $_SESSION["aof_user_id"] = $user->id; 
		  $this->user_name = $_SESSION["aof_user_name"] = $user->full_name(); 
          $this->user_login = true; 
      }else{ 
          unset($this->user_id); 
		  unset($this->user_name); 
          unset($_SESSION["aof_user_id"]); 
		  unset($_SESSION["aof_user_name"]); 
          $this->user_login = false; 
      }   
    } 
	 
    //destroy session 
    public function logout(){ 
        unset($_SESSION["aof_user_id"]); 
		unset($_SESSION["aof_user_name"]); 
        unset($this->user_id); 
		unset($this->user_name); 
        $this->user_login = false; 
        session_destroy(); 
    } 
     
    //check process if session set so give user login attribute true and ses user id 
    private function check_login(){ 
        if(isset($_SESSION["aof_user_id"]) && isset($_SESSION["aof_user_name"])){ 
            $this->user_id = $_SESSION["aof_user_id"]; 
			$this->user_name = $_SESSION["aof_user_name"]; 
            $this->user_login = true; 
        }else{ 
          unset($this->user_id); 
		  unset($this->user_name); 
          $this->user_login = false;            
        } 
    } 
} 
$session = new Session(); 
?>