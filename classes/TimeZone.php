<?php 
require_once 'CRUD.php'; 
class TimeZone extends CRUD{ 
   //calss attributes 
   public $id; 
   public $GMT; 
   public $name; 
    
   
   //relation table attribute 
   public $lang_name; 
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'time_zones'; 
	protected static $primary_fields = array('id', 'GMT', 'name' ); 
  
    
} 
?>
