<?php 
require_once 'CRUD.php'; 
class MediaLibraryDirectories extends CRUD{ 
   //calss attributes 
   public $id; 
   public $title; 
   public $description; 
   public $inserted_by; 
   public $created_date; 
   public $last_update; 
   public $update_by; 
   //relation table attribute 
    
    //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'media_library_directories'; 
	protected static $primary_fields = array('id', 'title', 'description', 'inserted_by', 'created_date', 'last_update', 'update_by'); 
	 
	//check directory availability 
   public static function check_directory($id){ 
       $sql = "SELECT * FROM media_library_directories WHERE id = {$id} "; 
       $result_array = static::find_by_sql($sql); 
       return !empty($result_array)? array_shift($result_array) : false; 
   }	 
	 
	//get directory data 
	public function directory_data($sort_filed = null, $order_by = null, $id = null){ 
		$sql = "SELECT media_library_directories.id AS id,media_library_directories.title AS title, media_library_directories.description AS description, 
			 	users.user_name AS inserted_by, media_library_directories.created_date AS created_date, media_library_directories.last_update AS last_update, 
				 user2.user_name AS update_by 
				FROM media_library_directories 
				LEFT JOIN users ON media_library_directories.inserted_by = users.id 
				LEFT JOIN users AS user2 ON media_library_directories.update_by = user2.id"; 
		if(!empty($id)){		 
			 $sql .= " WHERE media_library_directories.id = $id "; 
			 $result_array = static::find_by_sql($sql); 
			 return !empty($result_array)? array_shift($result_array) : false; 
		}else{		 
			if(!empty($sort_filed) && !empty($order_by)){ 
				$sql .= " ORDER BY ".$sort_filed." ".$order_by;  
			 } 
			return self::find_by_sql($sql);   
		}				 
	}     
} 
?>