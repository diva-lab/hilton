<?php 
require_once("MysqlDatabase.php"); 
class CRUD { 
   //get all rows from table 
   public static function find_all($sort_filed = null, $order_by = null){ 
       $sql = "SELECT * FROM ".static::$table_name; 
	   if(!empty($sort_filed)){ 
		   $sql .= " ORDER BY ".$sort_filed." ".$order_by; 
	   } 
       return static::find_by_sql($sql);   
   } 
    
	//get all custom filed 
	public static function find_all_by_custom_filed($filed, $value, $sort_filed = null, $order_by = null){ 
		global $database; 
		$value = $database->escape_values($value); 
		$sql = "SELECT * FROM ".static::$table_name." WHERE {$filed} = '{$value}' "; 
		if(!empty($sort_filed) && !empty($order_by)){ 
		   $sql .= " ORDER BY ".$sort_filed." ".$order_by; 
		} 
		return static::find_by_sql($sql); 
	} 
   //get by id 
   public static function find_by_custom_filed($filed, $value){ 
	   global $database; 
	   $value = $database->escape_values($value); 
       $sql = "SELECT * FROM ".static::$table_name." WHERE {$filed} = '{$value}' "; 
       $result_array = static::find_by_sql($sql); 
       return !empty($result_array)? array_shift($result_array) : false; 
   } 
   //get by id 
   public static function find_by_id($id){ 
       $sql = "SELECT * FROM ".static::$table_name." WHERE id = {$id} "; 
       $result_array = static::find_by_sql($sql); 
       return !empty($result_array)? array_shift($result_array) : false; 
   } 
   //excute sql command and loop throw rows then fetch array 
   protected static function find_by_sql($sql){ 
       global $database; 
       $result_set = $database->query($sql); 
       $object_array = array(); 
       while($row = $database->fetch_array($result_set)){ 
           $object_array[] = static::instantiate($row); 
       } 
       return $object_array; 
   } 
   //Object Initialization 
   protected static function instantiate($record){ 
       $called_class = get_called_class(); 
       $object = new $called_class; 
       foreach($record as $attribute => $value){ 
           if($object->has_attribute($attribute)){ 
            $object->$attribute = $value; 
           } 
       } 
       return $object; 
   } 
    
   //check if attribute exist in class 
   protected function has_attribute($attribute){ 
       $object_vars = $this->attribute(); 
       return array_key_exists($attribute, $object_vars); 
   } 
    
    //get only filed wich defined as attribute exist in db  
   //(macth attribute which exist in $database_fields with class attributes) 
   protected function attribute(){ 
      //this scope for parent class 
       //instatiate parent class 
      $attribute = array();  
	  foreach(static::$primary_fields as $field){ 
		 if(property_exists($this, $field)){ 
		   $attribute[$field] = $this->$field; 
		 } 
	  }      
      return $attribute;  
   } 
    
   //return cleaned attributes 
   protected function sanitized_attribute(){ 
       global $database; 
       $clean_attributes = array(); 
       foreach($this->attribute() as $key => $value){ 
           $clean_attributes[$key] = $database->escape_values($value); 
       } 
       return $clean_attributes; 
   } 
    
    
   //update command 
   public function update(){ 
      global $database; 
      $attributes = $this->sanitized_attribute(); 
      $attributes_pairs = array(); 
      foreach($attributes as $key => $value){ 
          $attributes_pairs[] = "{$key}='{$value}'"; 
      }   
      $sql = "UPDATE ".static::$table_name." SET "; 
      $sql .= join(",", $attributes_pairs);  
      $sql .= " WHERE id = ".$this->id; 
      
      if( $database->query($sql)){ 
          return true; 
      }else{ 
          return false; 
      }      
   } 
    
   //insert command 
   public function insert(){ 
       global $database; 
       $attributes = $this->sanitized_attribute(); 
       $sql = "INSERT INTO ".static::$table_name." ("; 
       $sql .= join(',', array_keys($attributes)); 
       $sql .= ")VALUES('"; 
       $sql .= join("','", array_values($attributes)); 
       $sql .= "')";  
       if($database->query($sql)){ 
           $this->id = $database->inserted_id(); 
           return true; 
       }else{ 
           return false; 
       }   
   } 
    
   
    
    
   //delete command 
   public function delete(){ 
       global $database;    
       $sql = "DELETE FROM ".static::$table_name." WHERE id = ".$this->id; 
       $database->query($sql); 
        
       if($database->mysql_affected_rows() == 1){ 
        return true; 
       }else{ 
        return false; 
       } 
   } 
} 
?>