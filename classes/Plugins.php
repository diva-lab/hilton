<?php 
require_once 'CRUD.php'; 
class Plugins extends CRUD{ 
   //calss attributes 
   public $id; 
   public $source; 
   public $name; 
   public $description; 
   public $version; 
   public $author; 
   public $uploaded_date;	 
   public $uploaded_by;  
   public $enable_event; 
   public $enable_post; 
   public $enable_page; 
   public $enable_options; 
   public $enable_translate; 
   public $enable_index; 
   public $position; 
   //relation table attribute 
    //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , 'user_name'); 
   }    
   //define table name and fields 
	protected static $table_name = 'plugins'; 
	protected static $primary_fields = array('id','source','name','description','version','author','uploaded_date','uploaded_by','enable_options', 
	'enable_translate','enable_event','enable_product','enable_post','enable_page','enable_index','position'); 
	 
	public function plugins_data($sort_filed = null, $order_by = null){ 
		$sql = "SELECT plugins.id AS id, plugins.source AS source, plugins.name AS name, plugins.description AS description, 
		        plugins.position AS position,plugins.version AS version, plugins.author AS author, users.user_name AS uploaded_by,  
				plugins.uploaded_date AS uploaded_date 
				FROM plugins 
				LEFT JOIN users ON plugins.uploaded_by = users.id"; 
		if(!empty($sort_filed) && !empty($order_by)){ 
			$sql .= " ORDER BY ".$sort_filed." ".$order_by;  
		 } 
		return self::find_by_sql($sql);  			 
	} 
} 
?> 
