<?php 
require_once 'CRUD.php'; 
class CMSModules extends CRUD{ 
	//calss attributes 
	public $id; 
	public $title; 
	public $sorting; 
	public $icon; 
	public $shadow; 
	public $type; 
	public $file_source; 
	public $sid; 
	public $inserted_by; 
    public $inserted_date; 
	public $update_by; 
	public $last_update; 
	//relation table attribute 
	 
	//push attributes for relational tables 
	public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
	}    
	//define table name and fields 
	protected static $table_name = 'cms_module_access'; 
	protected static $primary_fields = array('id', 'title', 'sid', 'sorting', 'icon', 'shadow', 'type','file_source','inserted_by', 'inserted_date','update_by', 'last_update'); 
	 
} 
?>