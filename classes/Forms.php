<?php
require_once 'CRUD.php';
	
class Forms extends CRUD{
	//calss attributes
	public $id;
	public $name;   
	public $label;
	public $enable;
	public $email_to;
	public $inserted_by;
	public $inserted_date;
	public $update_by;
	public $last_update;
	//relation table attribute
	
	//push attributes for relational tables
	public function enable_relation(){
		array_push(static::$primary_fields);
	}   
	//define table name and fields
	protected static $table_name = 'forms';
	protected static $primary_fields = array('id','name','label','enable','email_to','inserted_by','inserted_date','update_by','last_update');
	
	public function form_data($sort_filed = null, $order_by = null, $id = null){
		$sql = "SELECT forms.id AS id, forms.name AS name, forms.label AS label, forms.enable AS enable, users.user_name AS inserted_by,
				forms.inserted_date AS inserted_date,user2.user_name AS update_by,forms.last_update AS last_update,forms.email_to AS email_to
				FROM forms
				LEFT JOIN users ON forms.inserted_by = users.id
				LEFT JOIN users AS user2 ON forms.update_by = user2.id";
		if(!empty($id)){		
			 $sql .= " WHERE forms.id = $id ";
			 $result_array = static::find_by_sql($sql);
			 return !empty($result_array)? array_shift($result_array) : false;
		}else{		
			if(!empty($sort_filed) && !empty($order_by)){
				$sql .= " ORDER BY ".$sort_filed." ".$order_by; 
			 }
			return self::find_by_sql($sql);  
		}				
	}
	
	function get_file_content($lang_id,$file_name){
		$record_info = self::find_by_id($lang_id);
		$path = "../../../forms/";
		$file_path = $path.$record_info->label."/";
		$file_directory = $file_path.$file_name.".txt";
		if(file_exists($file_directory)){
			$file_open = fopen($file_directory,"r+");
			$size = filesize($file_directory);
			if($size!=0){
				$data = fread($file_open,$size);
				echo $data;
			}
		}
	} 
	
	
}
	
	?>