<?php 
require_once 'CRUD.php'; 
class MenuGroup extends CRUD{ 
   //calss attributes 
   public $id; 
   public $title; 
   public $alias; 
   public $image; 
   public $description; 
   public $inserted_by; 
   public $inserted_date; 
   public $update_by; 
   public $last_update; 
   //relation table attribute 
    
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
   //define table name and fields 
	protected static $table_name = 'structure_menu_group'; 
	protected static $primary_fields = array('id','title','alias','image','description','inserted_by','inserted_date','update_by','last_update'); 
  // get menu group data 
    public function menu_group_data($sort_filed = null, $order_by = null, $id = null){ 
		  $sql = "SELECT structure_menu_group.id AS id,structure_menu_group.title AS title, 
		  		  structure_menu_group.alias AS alias,structure_menu_group.description AS description,users.user_name AS inserted_by, 
				  structure_menu_group.image AS image,structure_menu_group.inserted_date AS inserted_date, 
				  user2.user_name AS update_by, structure_menu_group.last_update AS last_update 
				  FROM structure_menu_group 
				  LEFT JOIN users ON structure_menu_group.inserted_by = users.id 
				  LEFT JOIN users AS user2 ON structure_menu_group.update_by = user2.id"; 
		  if(!empty($id)){		 
			 $sql .= " WHERE structure_menu_group.id = $id "; 
			 $result_array = static::find_by_sql($sql); 
			 return !empty($result_array)? array_shift($result_array) : false; 
		}else{		 
			if(!empty($sort_filed) && !empty($order_by)){ 
				$sql .= " ORDER BY ".$sort_filed." ".$order_by;  
			 } 
			return self::find_by_sql($sql);   
		}				 
	} 
} 
?>
