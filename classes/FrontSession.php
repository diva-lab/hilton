<?php 
class FrontSession { 
    public $user_id; 
	public $user_name; 
    private $user_login = false; 
     
    //check if usr login or not 
    public function is_logged(){ 
        return $this->user_login; 
    } 
     
    //when call class restart session and run check login method 
    public function __construct() { 
		if(!isset($_SESSION)){ 
			session_start(); 
		} 
        $this->check_login(); 
    } 
     
    //get user object and save it in user_id, session user id  
    public function Login($user){ 
      if($user){ 
          $this->user_id = $_SESSION["front_3atayer_user_id"] = $user->id; 
		  $this->user_name = $_SESSION["front_3atayer_user_name"] = $user->full_name(); 
          $this->user_login = true; 
      }else{ 
          unset($this->user_id); 
		  unset($this->user_name); 
          unset($_SESSION["front_3atayer_user_id"]); 
		  unset($_SESSION["front_3atayer_user_name"]); 
          $this->user_login = false; 
      }   
    } 
    	 
	//update username 
	public function update_name($user){ 
		if($user){ 
			unset($this->user_name); 
			unset($_SESSION["front_3atayer_user_name"]); 
			$this->user_name = $_SESSION["front_3atayer_user_name"] = $user->full_name(); 
		} 
	} 
	 
    //destroy session 
    public function logout(){ 
        unset($_SESSION["front_3atayer_user_id"]); 
		unset($_SESSION["front_3atayer_user_name"]); 
        unset($this->user_id); 
		unset($this->user_name); 
        $this->user_login = false; 
        session_destroy(); 
    } 
     
    //check process if session set so give user login attribute true and ses user id 
    private function check_login(){ 
        if(isset($_SESSION["front_3atayer_user_id"]) && isset($_SESSION["front_3atayer_user_name"])){ 
            $this->user_id = $_SESSION["front_3atayer_user_id"]; 
			$this->user_name = $_SESSION["front_3atayer_user_name"]; 
            $this->user_login = true; 
        }else{ 
          unset($this->user_id); 
		  unset($this->user_name); 
          $this->user_login = false;            
        } 
    } 
} 
$front_session = new FrontSession(); 
?>