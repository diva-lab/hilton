<?php 
require_once 'CRUD.php'; 
class ThemeLayoutModelPlugin extends CRUD{ 
   //calss attributes 
   public $id; 
   public $model_id; 
   public $plugin_id; 
   public $sorting; 
   public $position; 
   //relation table attribute 
   public $plugin_source; 
   public $enable_translate; 
   public $enable_options; 
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields,'plugin_source','enable_translate','enable_options'); 
   }    
    
   //define table name and fields 
	protected static $table_name = 'theme_layout_model_plugin'; 
	protected static $primary_fields = array('id', 'model_id', 'plugin_id','sorting','position'); 
    
   // get right plugins for this models  
   	public function get_model_plugin_data($model_id){ 
		$sql = "SELECT plugins.source AS plugin_source, plugins.enable_translate AS enable_translate, plugins.enable_options AS enable_options, 
				theme_layout_model_plugin.plugin_id AS plugin_id 
				FROM theme_layout_model_plugin, plugins 
				WHERE theme_layout_model_plugin.plugin_id = plugins.id AND theme_layout_model_plugin.model_id = '$model_id'  
				AND plugins.enable_options = 'yes'  
				ORDER BY sorting ASC"; 
		return static::find_by_sql($sql); 
	} 
   // get plugins for this models  
   	public function front_get_model_plugin($model_id, $position){ 
		$sql = "SELECT plugins.source AS plugin_source 
				FROM theme_layout_model_plugin, plugins 
				WHERE theme_layout_model_plugin.plugin_id = plugins.id AND theme_layout_model_plugin.model_id = '$model_id'  
				AND theme_layout_model_plugin.position = '$position'  
				ORDER BY sorting ASC"; 
		return static::find_by_sql($sql); 
	} 
      
   
} 
?>
