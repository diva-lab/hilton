$(document).ready(function (){
$("#form_plugin_upload").submit(function (){
	$('#submit').attr("disabled", "true");
	file_data = new FormData($('#form_plugin_upload')[0]);
	//upload image
	$.ajax({
			type: 'POST',
			url: 'data_model/uploading_plugin.php',
			data: file_data,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend : function (){
				$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Uploading File ....');
			},
			success: function(upload){
				if(upload.status == "work"){
					var data = {	
						file_name: upload.name,
					};	
					//insert post data
					$.ajax({
						type: 'POST',
						url: 'data_model/insert.php',
						data: data,
						beforeSend: function(){
							$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading Compressing File ....');
						},
						success: function(data){
							if(data.status == "plugin_exist"){
								$('#upload_file').val('');
								$('#submit').removeAttr("disabled");
								$('#loading_data').html('Plugin is already exist, Please Change source name');
							}else if(data.status == "inserted_done"){
								$('#upload_file').val('');
								$('#submit').removeAttr("disabled");
								$('#loading_data').html('Successful Insert Process');
							}else if(data.status == "inserted_error"){
								$('#upload_file').val('');
								$('#submit').removeAttr("disabled");
								$('#loading_data').html('Cant Insert To DB');
							}else if(data.status == "not_read"){
								$('#upload_file').val('');
								$('#submit').removeAttr("disabled");
								$('#loading_data').html('Cant Read ZIP File');
							}else if(data.status == "not_exist"){
								$('#upload_file').val('');
								$('#submit').removeAttr("disabled");
								$('#loading_data').html('Cant Find ZIP File');
							}
						}
					})					
				}else{
					$('#upload_file').val('');
					$('#submit').removeAttr("disabled");
					$('#loading_data').html(upload['message']);
				}
			}
		})	
  return false;     
 })    
})