$(document).ready(function (){
	$(".address_update_insert").live("click",function(){
		$(".address_update_insert").fancybox({
			'width'				: '40%',
			'height'			: '90%',
			'autoScale'			: false,
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'type'				: 'iframe'
		});	
	return false;
	});
	$(".del_confirm").live("click", function(){
		//get record id
		var row_id = $(this).attr('id');
		var data = {	
			task: "delete",
			record: row_id,
		};
		$.ajax({
			type: 'post',
			url: 'data_model/delete_address.php',
			data: data,
		beforeSend: function(){
			//show loading 
			$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
		},
		success: function(data){
			if(data.status == 'work'){
				// remove row
				$("#row_"+row_id).closest("tr").remove(); 
				 //hide light box
				$(".modal").modal('hide');
			}
		}	
		})
		return false;
	});    
})