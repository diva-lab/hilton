$(document).ready(function (){
	
 $("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //get value of checkbox
	
	
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			title: $('#title').val(), 
			shadow: $('input[name=shadow]:checked').val(), 
			comment_body:$('#comment_body').val()
			
						
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 dataType:"JSON",
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
 		if(data.status == 'work'){
			 if($('#process_type').val() == 'update'){
			  window.location.href = "full_info.php?id="+$('#record').val();
			 }else{
			    $('#form_crud')[0].reset();
				$('#form_option')[0].reset();
				$('#submit').removeAttr("disabled");
				$('#loading_data').html('Successful Insert Process');
				
								 
			 }
		 }else{
			$('#loading_data').html('Error In Process');
		 }
	 }
});
 //do not go to any where
  return false;     
 }) 
    
})
