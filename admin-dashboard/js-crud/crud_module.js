$(document).ready(function (){
//use this with insert and update page
//if structure type changed show module div	
$("input[name=structure_type]").click(function() {
	var structure_type = $(this).val();
	if(structure_type == 'page'){
		$("#modules").show();
		$("#shadow").show();	
		$("#icon_available").hide();	
	}else{
		$("#modules").hide();
		$("#shadow").hide();	
		$("#icon_available").show();	
	}
});	

//for insert and update page when button clicked get data and send to php 
$("#form_crud").submit(function (){
 //disable button
	 $('#submit').attr("disabled", "true");
	 var structure_type = $('input[name=structure_type]:checked').val();
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			group_id: $('#group_id').val(),
			record: $('#record').val(),
			title: $('#title').val(), 
			icon: $('#icon').val(), 
			sorting: $('#sorting').val(), 
			shadow: $('input[name=shadow_status]:checked').val(), 
			structure_type: structure_type,
            module: $('#module_list').val(),
			source: $('#file_source').val()
		};
	$.ajax({
	 type: type,
	 url: url,
	 dataType:"JSON",
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){			 
		if(data.status == 'work'){
			 if($('#process_type').val() == 'update_page' || $('#process_type').val() == 'update_module'){
                window.location.href = "view.php";
			 }else{
				//add inserted module to module list
				if(structure_type == 'module'){
					$('#module_list').append($('<option>', { 
						value: data.id,
						text : data.title 
					}));
				}
				//clean form from added value
				$('#title').val(''); 
				$('#icon').val('');
				$('#sorting').val('');
				$('#file_source').val('');
				$('#module_list').val('');
				$('#submit').removeAttr("disabled");
				$('#loading_data').html('Successful Insert Process');
				//load list of module and page after insert data
				$.ajax({
					type:"GET",
					url: 'data_model/view_modules_list.php',
					beforeSend: function(){
						//show laoding 
						$('.panel-heading').html('Modules & Sub Pages&nbsp<img src="../../img/loading.gif"/>&nbspLoading ....');
					},success: function(data){
						$('#module_page_view_list').html('');
						$('#module_page_view_list').html(data);
						$('.panel-heading').html('Modules & Sub Pages');
					}
				 });					 
			 }
		 }else if(data.status == 'valid_error'){
			 $('#loading_data').html(data.fileds);
			 $('#loading_data').css('color', 'red');
			 $('#submit').removeAttr('disabled');	 
		 }else{
			$('#loading_data').html('Error In Process');
		 }
	 }
	});
 //do not go to any where
  return false;     
 });
 });
 
 




