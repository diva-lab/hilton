$(document).ready(function (){
	var main_content= {};
	$('input[name=main_menu]').click(function(){
		if($(this).val() == 'yes'){
			$('#display_image_div').removeClass('hide');
		}else{
			$('#display_image_div').addClass('hide');
		}
	})



$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	//get all main content values
	$(".main_content").each(function(){
		if($(this).is("textarea")){
			var textarea_value = tinymce.get($(this).attr("id")).getContent()
			main_content[$(this).attr("id")] = textarea_value; 
		}else{
			 main_content[$(this).attr("id")] = $(this).val(); 
		}
	});	 
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			main_content: main_content,
			cover:$('#imageVal').val(),
			sorting:$("#sorting").val(),
			shadow: $('input[name=shadow]:checked').val(),
			main_menu: $('input[name=main_menu]:checked').val(),
			display_image: $('input[name=display_image]:checked').val(),
			taxonomy:$('#taxonomies').val(),
			parent_id:$('#parent_id').val()
			
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
		if(data.status == 'work'){
			 if($('#process_type').val() == 'update'){
				window.location.href = "full_info.php?id="+$('#record').val()+"&lang="+$('#lang').val()+"type="+$('#taxonomies').val();
			 }else{
			    window.location.href = "full_info.php?id="+data.id;			 
			 }
		 }else if(data.status == 'exist'){
			 $('#form_crud')[0].reset();
			 $('#submit').removeAttr("disabled");
			 $('#loading_data').html(' The Taxonomy Category Is Already Exist');
			 
		}else if(data.status == 'valid_error'){
			 $('#loading_data').html(data.fileds);
			 $('#loading_data').css('color', 'red');
			 $('#submit').removeAttr('disabled');	 
		 }else{
			$('#loading_data').html('Error In Process');
		 }
	 }
	})
 //do not go to any where
  return false;     
 }); 
$('.confirm_delete').click(function(){
	var task = $("#task_type").val();
	var del_id = $(this).attr('id');
	var data = {
		 task : task ,
		 id : del_id
		};
	var url = "data_model/delete.php";
	var type = "GET";
	$.ajax({
		url : url,
		type : type,
		data : data,
		success : function(data){
			$('#category_'+del_id).remove();
		}	
		});
	});   
});