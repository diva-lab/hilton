$(document).ready(function (){
$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			first_name: $('#first_name').val(), 
			last_name: $('#last_name').val(),
			email:  $('#email').val(),
			password: $('#pwd').val()
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 dataType:"JSON",
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
		if(data.status == 'work'){
			$('#submit').removeAttr("disabled");
			$('#loading_data').html('Profile Updated');
		 }else if(data.status == 'email_exist'){
			$('#submit').removeAttr("disabled");
			$('#loading_data').html('Email already exist');
		 }else if(data.status == 'not_valid_pass'){
			$('#submit').removeAttr("disabled");
			$('#loading_data').html('Password is incorrect');
		 }else if(data.status == 'insert_pass'){
			$('#submit').removeAttr("disabled");
			$('#loading_data').html('Please enter your password ');
		 }else{
			$('#submit').removeAttr("disabled");
			$('#loading_data').html('Error In Process');
		 }
	 }
})


 //do not go to any where
  return false;     
 });

 
    
})
