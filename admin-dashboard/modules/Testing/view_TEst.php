<?php
require_once("../layout/initialize.php");
$languages = Localization::find_all();
//get all category 

require_once("../layout/header.php");
include("../../assets/texteditor4/head.php");
?>

<script type="text/javascript" src="../../js-crud/urlParse.js"></script> 
<script type="text/javascript" src="../../js-crud/auto_complete.js"></script> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php"); ?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
    <section class="wrapper site-min-height"> 
        <h4>Url Module</h4> 
        <div class="row"> 
            <aside class="col-lg-8"> 
                <section> 
                    <div class="panel"> 
                        <div class="panel-heading"> Add URl</div> 
                        <div class="panel-body"> 
                            <form class="form-horizontal tasi-form" role="form" id="ajaxform" action="data_model/insert.php"  method="post"> 
                                <input type="hidden" id="process_type" name="process_type" value="insert"> 
                                <section class="panel "> 
                                    <header class="panel-heading tab-bg-dark-navy-blue"> 
                                        <ul class="nav nav-tabs"> 
                                            <li class=" center-block active" > <a data-toggle="tab" href="#main_option" class="text-center"><strong> Add Url</strong></a></li> 

                                        </ul> 
                                    </header> 
                                    <div class="panel-body"> 
                                        <div class="tab-content"> 
                                            <div id="main_option" class="tab-pane active "> 
                                                <section class="panel col-lg-9"> 
                                                    <header class="panel-heading tab-bg-dark-navy-blue "> 
                                                        <ul class="nav nav-tabs"> 
                                                            <?php
                                                            //create tabs for all available languages  
                                                            $languages = Localization::find_all('id', 'desc');
                                                            $serial_tabs = 1;
                                                            foreach ($languages as $language) {
                                                                $lang_tab_header = ucfirst($language->name);
                                                                echo "<li class='";
                                                                if ($serial_tabs == 1) {
                                                                    echo " active ";
                                                                } echo"'> <a data-toggle='tab' href='#$language->name'> 
								<strong>$lang_tab_header</strong></a></li>";
                                                                $serial_tabs++;
                                                            }
                                                            ?> 
                                                        </ul> 
                                                    </header> 
                                                    <div class="panel-body"> 
                                                        <div class="tab-content"> 
                                                            <div class='form-group'> 
                                                                <label  class='col-lg-2'>url:</label> 
                                                                <div class='col-lg-9'> 
                                                                    <input type='text' class='form-control main_content' id='url' name='url'> 
                                                                </div> 
                                                            </div>
                                                            <?php
                                                            $serial_tabs_content = 1;
                                                            foreach ($languages as $language) {
                                                                echo " 
								<div id='$language->name' class='tab-pane";
                                                                if ($serial_tabs_content == 1) {
                                                                    echo " active ";
                                                                } echo"'> 
										<div class='form-group'> 
										<label  class='col-lg-2'>Title:</label> 
										<div class='col-lg-9'> 
										  <input type='text' class='form-control main_content' name='title_$language->label' id='title_$language->label'  autocomplete='off'  
										  onchange=\"add_char('title_$language->label','alias_$language->label')\"> 
										</div> 
									  </div> 
									  <div class='form-group'> 
										<label class='col-lg-2'>Alias:</label> 
										<div class='col-lg-9'> 
										  <input type='text' class='form-control main_content' name='alias_$language->label' id='alias_$language->label'> 
										</div> 
									  </div> 
                                                                         
									  <div class='form-group'> 
										<label  class='col-lg-2'>Description:</label> 
										<div class='col-lg-9'> 
										  <textarea class='form-control main_content' name='description_$language->label' id='description_$language->label'></textarea> 
										</div> 
									  </div> 
									 
									 
								</div>";
                                                                $serial_tabs_content++;
                                                            }
                                                            ?> 
                                                        </div> 
                                                    </div> 
                                                </section>   
                                            </div> 


                                        </div> 
                                    </div> 
                                </section> 
                                <div class="form-group"> 
                                    <div class="col-lg-offset-2 col-lg-4"> 
                                        <button type="submit" class="btn btn-info" id="submit">Save</button> 
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <div id="loading_data"></div> 
                                    </div> 
                                </div> 
                            </form> 
                        </div> 
                    </div> 
                </section> 
            </aside> 

        </div> 
        <!-- page end-->  
    </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php"); ?> 
