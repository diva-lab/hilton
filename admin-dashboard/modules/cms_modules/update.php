<?php  
	require_once("../layout/initialize.php");	 
	//check id access 
	$record_id = $_GET['id']; 
	$record_info = CMSModules::find_by_id($record_id); 
	if($user_profile->global_edit != 'all_records'){ 
		  redirect_to('view.php');	 
	  } 
	require_once("../layout/header.php"); 
?> 
  <script type="text/javascript" src="../../js-crud/crud_module.js"></script>  
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height">  
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Edit Module Or Page</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
                  <input type="hidden" id="process_type" value="update_<?php echo $record_info->type?>"> 
                   
                  <input type="hidden" id="record" value="<?php echo $record_info->id?>"/> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Title</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="title" value="<?php echo $record_info->title;?>" autocomplete="off"/> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Sorting:</label> 
                    <div class="col-lg-8"> 
                  <input type="text" class="form-control" style="width:50px" id="sorting" value="<?php echo $record_info->sorting;?>" autocomplete="off"/> 
                    </div> 
                  </div> 
                  <?php 
                	if($record_info->type == "module"){ 
						echo " 
						  <div class='form-group'> 
						  <label  class='col-lg-2'>Icon Name:</label> 
							<div class='col-lg-8'> 
							  <input type='text' class='form-control' id='icon' value='$record_info->icon' autocomplete='off'/> 
							</div> 
						  </div>";						 
					} 
				?> 
			  <?php 
                if($record_info->type == "page"){ 
                    echo " 
                    <div class='form-group'> 
                      <label  class='col-lg-2'>Shadow:</label> 
                      <div class='col-lg-8'> 
                        <div class='radio'> 
                          <label> 
                            <input type='radio' name='shadow_status' value='yes'"; if($record_info->shadow == "yes"){ echo "checked";} echo"/>Yes</label> 
                        </div> 
                        <div class='radio' style='padding-right:20px'> 
                          <label> 
                            <input  type='radio' name='shadow_status' value='no'"; if($record_info->shadow == "no"){echo "checked";} echo"/>No</label> 
                        </div> 
                      </div> 
                    </div>"; 
                } 
            ?> 
                  <div class="form-group"> 
                  <label  class="col-lg-2">Source Destination:</label> 
                    <div class="col-lg-8"> 
                     <textarea class="form-control" id="file_source"><?php echo $record_info->file_source?></textarea> 
                    </div> 
                  </div>					 
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>