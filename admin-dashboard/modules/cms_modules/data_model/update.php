<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/CMSModules.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
//update page 
if(!empty($_POST["task"]) && $_POST["task"] == "update_page" || $_POST["task"] == "update_module"){ 
	header('Content-Type: application/json'); 
	$required_fields = array('title'=>"- Insert title", 'sorting'=>'- Insert sorting' ,'source'=>'- Insert source Destination'); 
	$check_required_fields = check_required_fields($required_fields); 
	if(count($check_required_fields) == 0){ 
		//get data 
		$id = $_POST['record']; 
		$edit = CMSModules::find_by_id($id);	 
		$edit->title = $_POST["title"]; 
		$edit->sorting = $_POST["sorting"]; 
		$edit->file_source = $_POST["source"]; 
		if($_POST["task"] == "update_module"){ 
			$edit->icon = $_POST["icon"]; 
		}else{ 
			$edit->shadow = $_POST["shadow"]; 
		} 
		$update = $edit->update(); 
		//send json 
		if($update){ 
			$data  = array("status"=>"work"); 
			echo json_encode($data); 
		}else{ 
			$data  = array("status"=>"error"); 
			echo json_encode($data); 
		} 
	//update module 
  }else{ 
		  //validation error 
		  $comma_separated = implode("<br>", $check_required_fields); 
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		  echo json_encode($data); 
	  }			 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>