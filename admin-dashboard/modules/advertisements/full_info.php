<?php
	require_once("../layout/initialize.php");
	if(isset($_GET['id']) && is_numeric($_GET['id'])){
		$record_id = $_GET['id'];
		$translate_lang = new GeneralSettings();
	    $translate_lang->enable_relation();
	    $translate_lang_data = $translate_lang->general_settings_data();
		$define_class = new Advertisements();
		$define_class->enable_relation();
		$record_info = $define_class->adv_data(null,null,$record_id,null,$translate_lang_data->translate_lang_id);
		//check id access
		if(empty($record_info->id)){
			redirect_to("view.php");}
	}else{
		redirect_to("view.php");}
	require_once("../layout/header.php");
?>

  <!--header end--> 
  <!--sidebar start-->
  <?php require_once("../layout/navigation.php");?>
  <!--sidebar end--> 
  <!--main content start-->
  <section id="main-content">
    <section class="wrapper site-min-height">
      <h4> Structure Menu Group  Module</h4> 
      <!-- page start-->
      <div class="row">
        <aside class="col-lg-7">
          <section>
            <div class="panel">
              <div class="panel-heading"> Menu Group Info</div>
              <div class="panel-body">
                <form class="form-horizontal tasi-form" role="form" id="form_crud">
                <input type="hidden" id="process_type" value="insert">
                  <div class="form-group">
                    <label  class="col-lg-2 ">Titl eOne:</label>
                    <div class="col-lg-6" >
                    <?php echo $record_info->title_one?>
                    </div>
                  </div>
					<div class="form-group">
                    <label  class="col-lg-2 ">Title Two:</label>
                    <div class="col-lg-6" >
                    <?php echo $record_info->title_two?>
                    </div>
                  </div>   
                  <div class="form-group">
                    <label  class="col-lg-2 ">Page:</label>
                    <div class="col-lg-6" >
                    <?php echo $record_info->page_alias?>
                    </div>
                  </div>                
                <div class="form-group">
                    <label  class="col-lg-2 ">Image_cover :</label>
                    <div class="col-lg-6" >
                     <img src="../../../media-library/adv/<?php echo $record_info->image_cover?>"  style="width:100px; height:100px;">
                    </div>
                  </div>
                  <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button type="button" class="btn btn-info"  
                        onClick="window.location.href = 'update.php?id='+<?php echo $record_id?>" > <li class="icon-edit-sign"></li> Update </button>
                        </div>
                  </div>
                </form>
              </div>
            </div>
          </section>
        </aside>
        
      </div>
      
      <!-- page end--> 
    </section>
  </section>
  <!--main content end--> 
  <!--footer start-->
  <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
  <?php require_once("../layout/footer.php");?>