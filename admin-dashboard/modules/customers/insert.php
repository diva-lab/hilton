<?php  
	require_once("../layout/initialize.php"); 
	require_once("../layout/header.php"); 
	require_once("../../assets/texteditor4/head.php");  
	
		 
?> 
<script type="text/javascript" src="../../js-crud/customer.js"></script> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Adding New User Module</h4> 
    <!-- page start--> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading"> Add New User</div> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <section class="panel"> 
                  <header class="panel-heading tab-bg-dark-navy-blue "> 
                    <ul class="nav nav-tabs"> 
                      <li class="active"> <a data-toggle="tab" href="#main_info"><strong>User Main Info</strong></a> </li> 
                      <!-- <li class=""> <a data-toggle="tab" href="#address"><strong>Main Address</strong></a> </li>  -->
                    </ul> 
                  </header> 
                  <div class="panel-body"> 
                    <div class="tab-content"> 
                      <div id="main_info" class="tab-pane active"> 
                        <input type="hidden" id="process_type" value="insert"> 
                        <div class="form-group"> 
                          <label  class="col-lg-2 " > First Name:</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="first_name" placeholder="insert first name" autocomplete="off"> 
                          </div> 
                        </div>
                        <div class="form-group"> 
                          <label  class="col-lg-2 " > Last Name:</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="last_name" placeholder="insert last name" autocomplete="off"> 
                          </div> 
                        </div>
                         
                         <div class="form-group"> 
                          <label  class="col-lg-2 ">Email:</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="email" placeholder="insert email" autocomplete="off"> 
                          </div> 
                        </div> 
                         
                        
                       
                        <div class="form-group"> 
                          <label  class="col-lg-2 ">Password:</label> 
                          <div class="col-lg-8"> 
                            <input type="password" class="form-control" id="password" placeholder="insert password" autocomplete="off"> 
                          </div> 
                        </div> 

                        <div class="form-group"> 
                          <label  class="col-lg-2 " > City :</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="city" placeholder="insert city" autocomplete="off"> 
                          </div> 
                        </div>

                        <div class="form-group"> 
                          <label  class="col-lg-2 " > Area :</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="area" placeholder="insert area" autocomplete="off"> 
                          </div> 
                        </div>

                        <div class="form-group"> 
                          <label  class="col-lg-2 " > Street :</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control" id="street" placeholder="insert city" autocomplete="off"> 
                          </div> 
                        </div>

                        <div class="form-group"> 
                          <label  class="col-lg-2"> Gender :</label> 
                          <div class="col-lg-8"> 
                            <select name="" id="gender" class="form-control">
                              <option name = "gn" selected disabled value="">Select Gender</option>
                              <option name = "gn" value="Male">Male</option>
                              <option name = "gn" value="Female">Female</option>
                            </select> 
                          </div> 
                        </div>


                        <div class="form-group"> 
                          <label  class="col-lg-2 " > Birthday:</label> 
                          <div class="col-lg-8"> 
                            <input type="text" class="form-control start" id="birthday" placeholder="select or enter birthday" autocomplete="off"> 
                          </div> 
                        </div>
                         
                          
                         
                      </div> 
                       
                      <div class="form-group"></div> 
                      <div class="form-group"> 
                        <div class="col-lg-offset-2 col-lg-10"> 
                          <button type="submit" class="btn btn-info" id="submit">Save</button> 
                          <button type="reset" class="btn btn-default">Cancel</button> 
                          <div id="loading_data"></div> 
                        </div> 
                      </div> 
                    </div> 
                  </div> 
                </section> 
              </form> 
            </div> 
          </div> 
        </section> 
      </aside>
      <div class="col-lg-4"> 
      <section class="panel panel-primary"> 
          <header class="panel-heading"> Account Options </header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form"> 
              <div class="form-group"> 
                <label class="col-lg-4">Account Status</label> 
                <div class="col-lg-6">
                   <select class="form-control" id="account_status">
                     <option>Select Status</option>
                     <option value="verified">Verified</option>
                     <option value="not_verified">Not Verified</option>
                      <option value="blocked">Blocked</option>
                   </select>
                </div>
                
              </div> 
              
                  
            </form> 
          </div>
           
        </section>
        </div>
       
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>
<script type="text/javascript" src="../../js-crud/load_areas.js"></script>
<script type="text/javascript" src="../../js-crud/load_sub_areas.js"></script>