<?php
	require_once("../layout/initialize.php");
	//filter data

	//get filter data
	
	if(!empty($_GET["from"])){
	 $date_from = $newDate = date("Y-m-d", strtotime($_GET["from"]));
	}
	if(!empty($_GET["to"])){
	 $date_to = $newDate = date("Y-m-d", strtotime($_GET["to"]));
	}

// define registerd users class
	$define_class = new CustomerInfo();
	$records = $define_class->customer_data(null,"id", "DESC",null,null,null,null,null);
	
	
	require_once("../layout/header.php");
?>
<!--header end-->
<!--sidebar start-->
<?php require_once("../layout/navigation.php");?>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
  <section class="wrapper site-min-height">
    <h4>Registered Users Module</h4>
    <!-- page start-->
    <section class="panel">
      <header class="panel-heading"> View Registered Users </header>
      <br>
      <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'" <?php
	  $module_name = $opened_url_parts[count($opened_url_parts) - 2];
	  $opened_module_page_insert = $module_name.'/insert';
      if(!in_array($opened_module_page_insert, $user_allowed_page_array)){
	  	echo "disabled";
	  }
	  ?>>
      <li class="icon-plus-sign"></li>
      Add New Customer </button>
       <a  id="filter" href="#filter_form" class="btn btn-default  btn-info" style="margin-left:15px;"> <i class="icon-filter"></i>&nbsp;&nbsp;Filtration</a>
       <a  id="email_list" href="email_form.php"  style="margin-left:15px;"
       class="btn btn-default  btn-primary"> <i class="icon-mail"></i>&nbsp;&nbsp;Send Email list</a>

			 <?php
			 echo "
			    <a href='export.php'>
          <button type='button' style='margin-right:15px; margin-left:15px' class='btn  btn-success'>
					<i class='icon-filter'></i>&nbsp;&nbsp;Download Excel Sheet</button>
			    </a>
			  ";
	      ?>
        <br/>


      <div class="panel-body">
        <div class="adv-table editable-table ">

          <div class="space15"></div>
          <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>city</th>
                <th>area</th>
                <th>Registration Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
				  $serialize = 1;
				  if($records){
				 foreach($records as $record){
					 
					  echo "<tr>
					  <td>{$serialize}</td>
					  <td><a href='full_info.php?id={$record->id}'>{$record->full_name()}</a></td>
					  <td>{$record->email}</td>
					  <td>{$record->city}</td>
					  <td>{$record->area}</td>
					   <td>{$record->registeration_date}</td>
					<td>";
					$module_name = $opened_url_parts[count($opened_url_parts) - 2];
					include('../layout/btn_control.php');
					//delete dialoge
					echo "</td>
			 </tr>
			    	 		<div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
								  <div class='modal-dialog'>
									  <div class='modal-content'>
										  <div class='modal-header'>
											  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
											  <h4 class='modal-title'>Delete</h4>
										  </div>
										  <div class='modal-body'>
										   <p> Are you sure you want delete {$record->full_name()}</p>
										  </div>
										  <div class='modal-footer'>
											  <button class='btn btn-warning' type='button'
											  onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$record->id}'\"/>Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancel</button>
										  </div>
									  </div>
								  </div>
							  </div>";
  				$serialize++;
				   }
				  }
				  ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <div style="display:none">
    <div class="" id="filter_form"  style=" width:auto; height:500px; ">

          <div class="panel">
            <div class="panel-body">
              <form class="form-horizontal tasi-form" role="form"  method="get" action="view.php">
              
                <div class="form-group">
                  <label class=" col-lg-3">Insert Date</label>
                  <div class="col-lg-8">
                     <span class="list-group-item-text">&nbsp;&nbsp;<strong>From</strong></span>
                      <input type="text" class="form-control " name="from"  id="start" value="<?php if(isset($_GET["from"])){ echo  $date_from;} ?>">
                      <br>
                      <br>
                      <span class="list-group-item-text">&nbsp;&nbsp;<strong>To</strong></span>
                      <input type="text" class="form-control" name="to" value="<?php if(isset($_GET["from"])){ echo  $date_from;} ?>" id="end">

                    <span class="help-block">Select date range</span> </div>
                </div>
                 <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" class="btn btn-info" >Execute</button>
                    <button type="reset" class="btn btn-default" onClick="window.location.href = 'view.php'">Clear</button>
                  </div>
                </div>
              </form>
            </div>
          </div>

    </div>
  </div>
    <!-- page end-->
  </section>
</section>

<!--main content end-->
<!--footer start-->
<?php require_once("../layout/footer.php");?>
 <script>
  $(document).ready(function() {
    $("#email_list").fancybox({

				'width'				: '75%',
				'height'			: '100%',
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'

			});

  });
</script>
