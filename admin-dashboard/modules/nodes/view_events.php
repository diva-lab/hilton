<?php  
require_once("../layout/initialize.php"); 
$define_class = new Nodes; 
$define_class->enable_relation(); 
//event info 
//filter data 
$status = null; 
$date_from = null; 
$date_to = null; 
$cat_id = null; 
$lang = null; 
$inserted_by = null; 
$start_date_to = null; 
$start_date_from = null; 
//get filter data 
if(!empty($_GET["status"])){ 
 $status = $_GET["status"]; 
} 
if(!empty($_GET["inserted_by"])){ 
	$inserted_by = $_GET["inserted_by"]; 
} 
if(!empty($_GET["lang"])){ 
	$lang = $_GET["lang"]; 
} 
if(!empty($_GET["category"])){ 
	$cat_id = $_GET["category"]; 
} 
if(!empty($_GET["from"])){ 
	$date_from = date("Y-m-d", strtotime($_GET["from"])); 
} 
if(!empty($_GET["to"])){ 
	$date_to = date("Y-m-d", strtotime($_GET["to"])); 
} 
if(!empty($_GET["from_date"])){ 
	$start_date_from =  date("Y-m-d", strtotime($_GET["from_date"])); 
} 
if(!empty($_GET["to_date"])){ 
	$start_date_to = date("Y-m-d", strtotime($_GET["to_date"])); 
} 
 


$records =   $define_class->node_data('event',null,$status,$cat_id,$date_from,$date_to,$general_setting_info->translate_lang_id,$inserted_by,"inserted_date","DESC"); 
//get all categories 
$define_Taxonomies_class= new Taxonomies(); 
$define_Taxonomies_class->enable_relation(); 
$categories = $define_Taxonomies_class->taxonomies_data('category',null,null,'inserted_date', 'DESC',null,'many'); 
require_once("../layout/header.php"); 
?> <!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->
<script src="../../js-crud/nodes.js"></script>
<!--main content start--> 
<section id="main-content"> 
<section class="wrapper site-min-height"> 
<h4>Nodes Module</h4> 
      <section class="panel"> 
          <header class="panel-heading"> 
              View Events 
          </header> 
          <br> 
        
        
      <a id="filter" href="#filter_form" style="margin-right:15px;" class="btn btn-default  btn-info"><i class="icon-filter"></i>&nbsp;&nbsp;Filtration</a> 
      <br/> 
      <br />
      
          
          <div class="panel-body"> 
              <div class="adv-table editable-table "> 
                  <div class="space15"></div> 
                  <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
                  <thead> 
                    <tr> 
                       <th>#</th> 
                      <th style="width:300px"><i class=""></i> Title</th> 
                      <th><i class=""></i> Status</th> 
                      <th><i class=""></i> Duration</th> 
                      <th class=""><i class=""></i> Created By</th> 
                      <th><i class=""></i> Created  Date</th> 
                      <th>Action</th> 
                    </tr> 
                  </thead> 
                  <tbody id="myTable"> 
					<?php  
                       $serialize = 1;  
                       foreach($records as $record){ 
					   		//get content
							$node_contents = $define_class->get_node_content($record->id);
                       		$event_details = EventDetails::find_by_custom_filed('event_id',$record->id);

                           echo "<tr id='node_{$record->id}'>
                        		<td>{$serialize}</td> 
                        	 <td><ul>";
							  foreach($node_contents as $node_content){
								  echo "<li><a href='full_info.php?id={$node_content->content_id}'>- {$node_content->title}</a><li>";
							  }
							  echo "</ul></td> 
                       			<td>{$record->status}</td> 
                       			<td style='width'><span style='font-weight:bold'>From:</span> $event_details->start_date;<br><span style='font-weight:bold'> To:</span> $event_details->end_date;</td>
                        		<td>{$record->inserted_by}</td> 
                       			<td>{$record->inserted_date}</td> 
                      			<td width='12%'>"; 
								  $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
								  include('../layout/btn_control.php'); 
									  //plugin option 
								  $event_model =   $define_class->get_model($record->id); 
								  if(!empty($event_model)){ 
									  echo "<a href='plugin_option.php?id={$record->id}&model={$event_model->model}' class='btn btn-primary btn-xs tooltips'  
									  data-placement='top' data-toggle='tooltip' data-original-title='Plugin Option' style='margin-left: 3px;' ><i class='icon-cog'></i></a>"; 
								  } 
               			echo "</td>		 
                    		    </tr> ";
								//delete dialoge 
										echo "<div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
                                        <div class='modal-dialog'> 
                                            <div class='modal-content'> 
                                                <div class='modal-header'> 
                                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
                                                    <h4 class='modal-title'>Delete</h4> 
                                                </div> 
													<div class='modal-body'> 
													 <p> Are you sure you want delete  $record->title?!</p> 
													</div> 
                                                           <div class='modal-footer'>
                                                          <input type='hidden' id='task_type' value='delete'> 
                                                            <button class='btn btn-warning confirm_delete' id='{$record->id}' type='button'  data-dismiss='modal' /> Confirm</button> 
                                                             <button data-dismiss='modal' class='btn btn-default' type='button'>cancel</button> 
                                                        </div> 
                                                    </div> 
                                                </div> 
                                            </div>"; 
                                      $serialize++; 
                                  }?> 
                                   </tbody> 
                              </table>
                              
                            
                            </div> 
                        </div> 
                    </section> 
                    <!-- page end--> 
                </section> 
              </section> 
              <div style="display:none"> 
              <div class="" id="filter_form"  style=" width:auto; height:500px; "> 
              <aside class="col-lg-12"> 
              <section> 
                <div class="panel"> 
                  <div class="panel-body"> 
                    <form class="form-horizontal tasi-form" role="form"  method="get" action="view.php"> 
                       
                      <div class="form-group"> 
                        <label class="col-lg-3">Status:</label> 
                        <div class="col-lg-8"> 
                          <label class="checkbox-inline">  
                            <input type="radio" name="status" class="radio" value="draft" <?php if(isset($_GET["status"])){ 
                                if($status == "draft") echo "checked";} ?>> 
                            Draft</label> 
                          <label class="checkbox-inline"> 
                            <input type="radio" name="status" class="radio" value="publish" <?php if(isset($_GET["status"])){ 
                                if($status == "publish") echo "checked";} ?>> 
                            Publish</label> 
                        </div> 
                      </div> 
                       
                      <div class="form-group"> 
                        <label class="col-lg-3"> Categories:</label> 
                        <div class="col-lg-8"> 
                          <div class="checkboxes" style="max-height: 150px;overflow-y:auto;"> 
                            <?php  
                             foreach($categories as $category){ 
                             echo "<label class='label_check'><input type='checkbox' value='{$category->id}' name='category[]'"; 
                             if(!empty($_GET["category"])){ 
                              if(in_array($category->id, $cat_id)){ 
                                  echo "checked"; 
                              }    
                             } 
                             
                            echo ">{$category->name}</label>"; 
                           }?> 
                          </div> 
                        </div> 
                      </div> 
                      <div class="form-group"> 
                        <label class=" col-lg-3">Insert Date</label> 
                        <div class="col-lg-8"> 
                           <span class="list-group-item-text">&nbsp;&nbsp;<strong>From</strong></span> 
                            <input type="text" class="form-control " name="from"  id="start" value="<?php if(isset($_GET["from"])){ echo  $date_from;} ?>"> 
                            <br> 
                            <br> 
                            <span class="list-group-item-text">&nbsp;&nbsp;<strong>To</strong></span> 
                            <input type="text" class="form-control" name="to" value="<?php if(isset($_GET["from"])){ echo  $date_from;} ?>" id="end"> 
                           
                          <span class="help-block">Select date range</span> </div> 
                      </div> 
                       
                      <div class="form-group"> 
                        <label  class="col-lg-2">Created By:</label> 
                        <div class="col-lg-3"> 
                          <?php  $users = Users::find_all();?> 
                          <select multiple="multiple" class="multi-select s" id="my_multi_select1" name="inserted_by[]"> 
                            <?php  
                            foreach ($users as $user) : 
                            if(!empty($inserted_by)){ ?> 
                            <?php  if(in_array($user->id , $inserted_by)){   
                                       echo "<option value={$user->id} selected>{$user->user_name}</option>"; 
                                 } 
                              }else{ 
                                  echo "<option value={$user->id} >{$user->user_name}</option>"; 
                              } 
                        ?> 
                            </option> 
                            <?php endforeach; 
                           
                            ?> 
                          </select> 
                        </div> 
                      </div> 
                      <div class="form-group"> 
                        <div class="col-lg-offset-2 col-lg-10"> 
                          <button type="submit" class="btn btn-info" >Execute</button>
                          <button type="reset" class="btn btn-default" onClick="window.location.href = 'view.php'">Reset</button>
                        </div> 
                      </div> 
                    </form> 
                  </div> 
                </div> 
              </section> 
              </aside> 
              </div> 
              </div> 
              <!--main content end--> 
              <!--footer start--> 
              <?php require_once("../layout/footer.php");?> 
