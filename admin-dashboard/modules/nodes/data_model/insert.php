<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/Nodes.php'); 
require_once('../../../../classes/NodesContent.php'); 
require_once('../../../../classes/EventDetails.php'); 
require_once('../../../../classes/NodesSelectedTaxonomies.php'); 
require_once('../../../../classes/Localization.php'); 
require_once('../../../../classes/Taxonomies.php'); 
require_once('../../../../classes/TaxonomiesContent.php'); 
require_once('../../../../classes/NodesImageGallery.php');
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
//send json data 
header('Content-Type: application/json');
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	$required_fields = array('model'=>'- Insert Model');
	$check_required_fields = check_required_fields($required_fields); 
	if(count($check_required_fields) == 0){ 
		$add = new Nodes();
		$type = $_POST["type"]; 
		$add->status = $_POST["shadow"]; 
		$add->inserted_by = $session->user_id; 
		$add->inserted_date = date_now(); 
		$add->enable_comments = $_POST["enable_comments"]; 
		$add->enable_summary = $_POST["enable_summary"]; 
		$add->front_page = $_POST["show_in_front_page"]; 
		$add->slide_show = $_POST["show_in_slide_show"]; 
		$add->node_type = $type; 
		$add->model = $_POST["model"]; 
		$add->side_bar = $_POST["side_bar"];
        $add->shortcut_link = strtotime(date_now()); 
		if(!empty($_POST['slider_cover'])){ 
			$current_file = $_POST['slider_cover']; 
			$parts = explode('/',$current_file); 
			$image_cover = $parts[count($parts)-1]; 
			$add->slider_cover = $image_cover; 
		} 
		if(!empty($_POST['imageVal'])){ 
			$current_file = $_POST['imageVal']; 
			$parts = explode('/',$current_file); 
			$image_cover = $parts[count($parts)-1]; 
			$folder = $parts[count($parts)-3]; 
			$add->media_file = $folder; 
			$add->image_cover = $image_cover; 
	    }  
		if(!empty($_POST["start_time"])){ 
			$add->start_publishing = $_POST["start_time"]; 
		}else{ 
		  $add->start_publishing =  date_now(); 
		} 
		$add->end_publishing = $_POST["end_date"]; 
		$insert = $add->insert(); 
		$inserted_id = $add->id; 
		if($insert){ 
			//event details 
			$add_event_details = new EventDetails();
			$add_event_details->event_id = $inserted_id;
			$add_event_details->place  = $_POST["place"];
			$add_event_details->start_date = $_POST["start_date"];
			$add_event_details->end_date = $_POST["end_date"];
			$insert_event_details =  $add_event_details->insert();
			//retrieve all available languages 
			$languages = Localization::find_all('label','asc'); 
		   //insert event content  
			foreach($languages as $language){ 
				$add_event_content = new NodesContent(); 
				$add_event_content->node_id = $inserted_id; 
				$add_event_content->title = $_POST['main_content']['title_'.$language->label]; 
				$add_event_content->alias = $_POST['main_content']['alias_'.$language->label];	 
				$add_event_content->body = $_POST['main_content']['full_content_'.$language->label]; 
				$add_event_content->summary = $_POST['main_content']['summary_'.$language->label]; 
				$add_event_content->lang_id = $language->id; 
				$add_event_content->meta_description = $_POST['main_content']['meta_description_'.$language->label]; 
				$add_event_content->meta_keys = $_POST['main_content']['meta_keys_'.$language->label]; 
				$add_event_content->insert(); 
			}
			//insert selected_image_gallery 
			  if(!empty($_POST["selected_image_gallery"])){ 
				  //update orders table 
				  $selected_image_gallery = $_POST["selected_image_gallery"]; 
				  foreach($selected_image_gallery as $key => $value){ 
					  if($key != 0){ 
						   $explode_value = explode(',', $value); 
						   if(!empty($explode_value[0])){ 
							   $add_image_gallery = new  NodesImageGallery(); 
							   $add_image_gallery->image = file_folder_src($explode_value[0]); 
							   $add_image_gallery->sort = $explode_value[1];
							   $add_image_gallery->caption = $explode_value[2]; 
							   $add_image_gallery->related_id = $inserted_id; 
							   $add_image_gallery->insert();						  
						   } 
					  } 
				  }	 
			  }		 
			//insert categories 
			if(!empty($_POST["categories"])){ 
			  //remove duplicates 
			  $selected_categories = array_unique($_POST["categories"]); 
			  //convert to be array 
			  foreach($selected_categories as $category){ 
				  $add_category = new NodesSelectedTaxonomies(); 
				  $add_category->node_id = $inserted_id; 
				  $add_category->node_type = $type; 
				  $add_category->taxonomy_id = $category; 
				  $add_category->taxonomy_type = 'category'; 
				  $insert_category = $add_category->insert(); 
			  } 
			} 
			//insert post tags 
	   if(!empty($_POST["tags"])){ 
			$tags = $_POST["tags"];
			$new_tags = array(); 
			foreach($tags as $tag){ 
			//get tag id from db 
				$tag_id = Taxonomies::get_tag_id($tag); 
				if($tag_id){ 
					//insert tag 
					$add_tag = new NodesSelectedTaxonomies(); 
					$add_tag->node_id = $inserted_post_id; 
					$add_tag->taxonomy_id = $tag_id->id; 
					$add_tag->node_type = $type; 
					$add_tag->taxonomy_type = 'tag'; 
					$insert_tag = $add_tag->insert(); 
				}else{ 
						$add_new_tag = new Taxonomies(); 
						$add_new_tag->parent_id = 0; 
						$add_new_tag->status = "publish"; 
						$add_new_tag->taxonomy_type = 'tag'; 
						$add_new_tag->inserted_date = date_now(); 
						$add_new_tag->inserted_by = $session->user_id; 
						$insert_new_tag = $add_new_tag->insert(); 
						$tag_new_id = $add_new_tag->id; 
						//insert new tag  content 
						$insert_new_tag_content = new TaxonomiesContent(); 
						$insert_new_tag_content->taxonomy_id = $tag_new_id; 
						$insert_new_tag_content->name = $tag; 
						$insert_new_tag_content->alias = preg_replace('/\s+/', '_',$tag); 
						$insert_new_tag_content->lang_id = 0; 
						$add_new_tag_content = $insert_new_tag_content->insert(); 
						$new_tags[] = $tag_new_id; 
				} 
			} 
			//insert new tags 
			foreach($new_tags as $tag){ 
				$add_new_tag = new NodesSelectedTaxonomies(); 
				$add_new_tag->node_id = $inserted_post_id; 
				$add_new_tag->taxonomy_id = $tag; 
				$add_new_tag->taxonomy_type  = 'tag'; 
				$add_new_tag->node_type = $type; 
				$insert_new_tag = $add_new_tag->insert();				 
			} 
	   } 
		$data  = array("status"=>"work","inserted_id"=>$inserted_id); 
		echo json_encode($data); 
		}else{ 
		$data  = array("status"=>"error"); 
		echo json_encode($data); 
		} 
	}else{ 
		//validation error 
		$comma_separated = implode("<br>", $check_required_fields); 
		$data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		echo json_encode($data); 
	}	 
		  
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>