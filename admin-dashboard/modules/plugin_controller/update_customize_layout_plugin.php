  
	<?php  
	require_once("../../../classes/Plugins.php"); 
	require_once("../../../classes/ThemeLayoutModel.php"); 
	require_once("../../../classes/ThemeLayoutModelPlugin.php"); 
	 $model_id = $_GET['id']; 
	 $record_info = ThemeLayoutModel::find_by_id($model_id); 
	 $model_plugins = ThemeLayoutModelPlugin::find_all_by_custom_filed('model_id', $model_id,'sorting','DESC'); 
	 $plugins_left_ids = array(); 
	 $plugins_right_ids = array(); 
	 foreach($model_plugins as $model){ 
		 if($model->position == 'left' ){ 
		   array_push($plugins_left_ids , $model->plugin_id); 
		 } 
	 } 
	 foreach($model_plugins as $model){ 
		 if($model->position == 'right'){ 
		   array_push($plugins_right_ids , $model->plugin_id); 
		 } 
	 } 
	 $plugin_type = "enable_".$record_info->type; 
	 $all_plugins = Plugins::find_all_by_custom_filed($plugin_type,'yes'); 
	  
	?> 
    <link type="text/css" href="selectmultiple_drag_drop/css/jquery-ui.css" rel="stylesheet" /> 
    <link type="text/css" href="selectmultiple_drag_drop/css/ui.multiselect.css" rel="stylesheet" /> 
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script> 
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js"></script> 
	<script type="text/javascript" src="selectmultiple_drag_drop/js/ui.multiselect.js"></script> 
    <script type="text/javascript"> 
		$(function(){ 
			 
			$(".multiselect").multiselect(); 
			 
		}); 
	</script> 
    <form action="data_model/update_layout_model_plugin.php"  id="form_crud" method="post" style="width:500px; height:500px;" > 
    <input type="hidden"  id="record" value="<?php echo $model_id;?>"  name="record" /> 
    <input type="hidden"  id="type" value="<?php echo $record_info->type?>"  name="type"/> 
     
    <label>Model Name</label> 
    <input  type="text" id="name" name="name" value="<?php  echo $record_info->name;?>"/> 
     
    <br /> 
  <table width="100"> 
  <tr> 
  <td> 
     Select Model Plugin Postion Left  
     <select  class="multiselect" multiple="multiple" name="plugins_left[]" style="width:500px; height:500px;"> 
       <?php foreach($all_plugins as $plugin): 
	   if($plugin->position == 'left'){ 
	   ?> 
        <option value="<?php  echo $plugin->id?>" <?php if(in_array($plugin->id,$plugins_left_ids)) echo "selected"?>><?php  echo $plugin->name?></option> 
        <?php  } endforeach;?> 
       </select> 
      </td> 
      <td> 
       Select Model Plugin Postion Right 
     <select class="multiselect" multiple="multiple" name="plugins_right[]" style="width:500px; height:500px;"> 
       <?php foreach($all_plugins as $plugin): 
	   if($plugin->position == 'right'){ 
	   ?> 
        <option value="<?php  echo $plugin->id?>" <?php if(in_array($plugin->id,$plugins_right_ids)){ echo "selected"; } ?> 
    ><?php  echo $plugin->name?></option> 
        <?php } endforeach;?> 
       </select> 
       </td> 
       </tr> 
       </table> 
        
        
       <br /> 
       <br /> 
      <input type="submit" value="Save Changes" id="submit"/> 
    </form> 
     
    
