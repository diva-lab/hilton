<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/ThemeLayoutModel.php'); 
require_once('../../../../classes/ThemeLayoutModelPlugin.php'); 
//insert theme model  
$theme =$_POST['theme_id']; 
$add_model = new ThemeLayoutModel(); 
$add_model->name = $_POST["name"]; 
$add_model->theme_id = $_POST["theme_id"]; 
$add_model->layout_id = $_POST["layout_id"]; 
$add_model->type = $_POST["type"]; 
$inser_model = $add_model->insert(); 
if($inser_model) 
{ 
	//insert model plugin  
	 
	$inserted_model_id = $add_model->id; 
	if(!empty($_POST["plugins_left"])){ 
		$selected_plugins =   $_POST["plugins_left"]; 
		$sort = 1; 
		foreach($selected_plugins as $plugin) 
		{   $add_model_plugin = new ThemeLayoutModelPlugin(); 
			$add_model_plugin->model_id = $inserted_model_id; 
			$add_model_plugin->plugin_id = $plugin; 
			$add_model_plugin->sorting = $sort; 
			$add_model_plugin->position = 'left'; 
			$insert_model_plugin = $add_model_plugin->insert(); 
			 
			$sort++; 
	   } 
	  } 
	  if(!empty($_POST["plugins_right"])){ 
		$selected_plugins =   $_POST["plugins_right"]; 
		$sort = 1; 
		foreach($selected_plugins as $plugin) 
		{   $add_model_plugin = new ThemeLayoutModelPlugin(); 
			$add_model_plugin->model_id = $inserted_model_id; 
			$add_model_plugin->plugin_id = $plugin; 
			$add_model_plugin->sorting = $sort; 
			$add_model_plugin->position = 'right'; 
			$insert_model_plugin = $add_model_plugin->insert(); 
			 
			$sort++; 
	   } 
	  } 
	   
	 redirect_to("../../themes/view_layouts.php?id=$theme");	  
}else{ 
	  redirect_to("../../themes/view_layouts.php?id=$theme"); 
	} 
//close connection 
if(isset($database)){ 
$database->close_connection();	 
} 
?>