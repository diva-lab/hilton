<?php 
	require_once("../layout/initialize.php"); 
	require_once("../layout/header.php"); 
	$menu_groups = MenuGroup::find_all(); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start-->  
<script src="../../js-crud/crud_plugin_video.js"></script> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Plugin Module</h4> 
    <div class="row"> 
      <aside class="profile-info col-lg-9"> 
        <section> 
          <div class="panel "> 
            <div class="panel-heading"> Create Links Viwers</div> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert_video.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                <div class="form-group"> 
                  <label  class="col-lg-2 ">Name:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="name" placeholder=" " autocomplete="off"> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label  class="col-lg-2 ">Post alias:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="post_alias" placeholder=" " autocomplete="off"> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label  class="col-lg-2 ">Description:</label> 
                  <div class="col-lg-8"> 
                    <textarea class="wysihtml5 form-control" id="description" placeholder=" " rows="10"></textarea> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="submit" class="btn btn-info" id="submit">Save</button> 
                    <button type="reset" class="btn btn-default">Cancel</button> 
                    <div id="loading_data"></div> 
                  </div> 
                </div> 
              </form> 
            </div> 
          </div> 
        </section> 
      </aside> 
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 
