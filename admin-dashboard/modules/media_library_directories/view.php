<?php  
	require_once("../layout/initialize.php"); 
	$path = "../../../media-library/"; 
	$records = array_diff(scandir($path), array('..', '.'));  
	$main_folder = array('adv','slider'); 
	require_once("../layout/header.php"); 
?> 
  <!--header end-->  
  <!--sidebar start--> 
  <script type="text/javascript" src="../../js-crud/crud_media_directory.js"></script> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Media Library Module</h4> 
      <!-- page start--> 
      <section class="panel"> 
        <header class="panel-heading"> View Directories </header> 
        <div class="panel-body"> 
          <div class="adv-table editable-table ">  
           
               <button type="button" class="btn btn-danger" style="margin-left:5px" onClick="window.location.href = 'insert.php'"> 
               <li class="icon-plus-sign"></li>   Add New Media Directory</button> 
            <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
              <thead> 
                <tr> 
                  <th><i class=""></i> #</th> 
                  <th><i class=""></i> Title</th> 
                 <th>Action</th> 
                </tr> 
              </thead> 
              <tbody> 
                <?php 
				$serialize = 1;   
				foreach($records as $record){ 
					if(!in_array($record,$main_folder)){ 
				 echo "<tr> 
				 <td>{$serialize}</td> 
					  <td>{$record}</td> 
					 <td> 
					 <a href='../media_library_files/view.php?title={$record}' class='btn btn-primary btn-xs tooltips' data-placement='top' 
					  data-toggle='tooltip' data-original-title='Madia fille' >  <i class='icon-file'></i></a>"; 
					  
					 if($user_profile->global_edit == 'all_records'){ 
						echo " <a href='update.php?title={$record}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
						data-original-title='Edit' >  <i class='icon-edit'></i></a>"; 
					}else { 
					   echo "<a href='' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
					   data-original-title='Edit' disabled >  <i class='icon-edit'></i></a>"; 
					}	 
	                  //delete	 
					   if($user_profile->global_delete == 'all_records'){ 
							   echo " <a href='#my{$serialize}' data-toggle='modal' class='btn btn-primary btn-xs tooltips' data-placement='top' 
							    data-original-title='Delete'> 
							   <i class='icon-remove'></i></a>"; 
						  }else{ 
							   echo " <a href= '' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip' 
							    data-original-title='Delete' disabled > 
								 <i class='icon-remove'></i></a>"; 
						  }		 echo " </td> 
				  </tr> 
				  <div class='modal fade' id='my{$serialize}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
                                  <div class='modal-dialog'> 
                                      <div class='modal-content'> 
                                          <div class='modal-header'> 
                                              <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
                                              <h4 class='modal-title'>Delete</h4> 
                                          </div> 
                                          <div class='modal-body'> 
                                           <p> Are you sure you want delete  $record ??</p> 
                                          </div> 
                                          <div class='modal-footer'> 
                                               
                                              <button class='btn btn-warning' type='button'  
                                              onClick=\"window.location.href = 'data_model/delete.php?task=delete&title={$record}'\"/> Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
                                          </div> 
                                      </div> 
                                  </div> 
                              </div>"; 
				  $serialize++; 
				 }} 
				  ?> 
              </tbody> 
            </table> 
            
          </div> 
        </div> 
      </section> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?> 
