<?php 
 	require_once("../layout/initialize.php"); 
	$record_title = $_GET['title']; 
	 
	require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js-crud/crud_media_directory.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Media Library Module</h4> 
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel panel-primary"> 
              <div class="panel-heading"> Edit Media Directory</div> 
              <div class="panel-body"> 
               <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
                <input type="hidden" id="process_type" value="update"> 
                 <input type="hidden" id="record" value="<?php echo $record_title; ?>"> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">Name:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="title" placeholder=" " value="<?php echo $record_title;?>"autocomplete="off"> 
                    </div> 
                  </div> 
                   
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
         
      </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>