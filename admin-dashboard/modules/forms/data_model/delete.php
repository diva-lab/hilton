<?php
require_once('../../../../classes/Session.php');
require_once('../../../../classes/Functions.php');
require_once('../../../../classes/MysqlDatabase.php');
require_once('../../../../classes/Forms.php');
require_once('../../../../classes/Users.php');
require_once('../../../../classes/Profile.php');
require_once('../../../../classes/Localization.php');
//check  session user  log in
if($session->is_logged() == false){
	redirect_to("../../../index.php");
}
// get user profile  
$user_data = Users::find_by_id($session->user_id);
// get user profile data
$user_profile  = Profile::Find_by_id($user_data->user_profile);
// check if the user profile block
if($user_profile->profile_block == "yes"){
   redirect_to("../../../index.php");	
}
$path="../../../../forms/";
$languages = Localization::find_all();	
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){
	//get data
	$id = $_GET['id'];
	//find record	
	$find_form= Forms::find_by_id($id);
	if($user_profile->globel_delete == 'all_records' || $find_form->inserted_by == $session->user_id){
		//if there is record perform delete
		//if there is no record go back to view
		if($find_form){
			$form = $find_form->label;
			$delete = $find_form->delete();
			if($delete){
				foreach($languages as $lang){
					unlink($path.$form."/".$lang->label.".txt");
				}
				rmdir($path.$form);
				//delete all realted  posts ,events and pages categories
				//delete  posts categories
				$sql_delete_forms = "DELETE FROM form_attributes WHERE form_id = '{$id}'";
				$preform_delete_forms = $database->query($sql_delete_forms);
				
				
				redirect_to("../view.php");
			}else{
				redirect_to("../view.php");
			}	
			//if there is no record go back to view
		}else{
			redirect_to("../view.php");	
		} 
	}else {
		 redirect_to("../view.php");
	}	
}else{
	//if task wasnot delete go back to view
	redirect_to("../view.php");	
}
//close connection
if(isset($database)){
	$database->close_connection();
}

?>