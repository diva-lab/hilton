<?php
require_once('../../../../classes/Session.php');
require_once('../../../../classes/Functions.php');
require_once('../../../../classes/MysqlDatabase.php');
require_once('../../../../classes/Forms.php');
require_once('../../../../classes/Users.php');
require_once('../../../../classes/Profile.php');
require_once('../../../../classes/Localization.php');
//check  session user  log in
if($session->is_logged() == false){
	redirect_to("../../../index.php");
}
// get user profile  
$user_data = Users::find_by_id($session->user_id);
// get user profile data
$user_profile  = Profile::Find_by_id($user_data->user_profile);
// check if the user profile block
if($user_profile->profile_block == "yes"){
   redirect_to("../../../index.php");	
}
$path = "../../../../forms/";
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){
	//validite required required
	$required_fields = array('name'=>"- Insert Name", 'label'=>'- Insert label');
	$check_required_fields = check_required_fields($required_fields);
	if(count($check_required_fields) == 0){
		$label = $_POST["label"]; 
		//check if form label is valid 
		if(strpbrk($label, "\\/?%*:|\"<>") === false){
	   //insert data
			$add = new Forms();
			$add->name = $_POST["name"];
			$add->email_to = $_POST["email"];
			$add->enable = $_POST["enable"];
		    $add->label = $_POST["label"];
			$add->inserted_date = date_now();
			$add->inserted_by=$session->user_id;
			$insert = $add->insert();
			$inserted_id = $add->id;
			header('Content-Type: application/json');
			if($insert){
				//create form dir
				$form_file = mkdir( $path.$add->label, 0777);
				$form_path = $path.$add->label."/";
				$languages = Localization::find_all();								
				if($form_file){
					foreach($languages as $lang)
					$file_maessges = fopen($form_path.$lang->label.".txt","x+");
					  
				}
				
				$data  = array("status"=>"work","id"=>$inserted_id);
				echo json_encode($data);
			}else{
				$data  = array("status"=>"error");
				echo json_encode($data);
			}
		}else{
			 //if label not valid
			 $data  = array("status"=>"wrong");
		     echo json_encode($data);
		}
		  
  }else{
		  //validation error
		  $comma_separated = implode("<br>", $check_required_fields);
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated);
		  echo json_encode($data);
	  }
}
//close connection
if(isset($database)){
	$database->close_connection();
}

?>