<?php  
	require_once("../layout/initialize.php"); 
	$record_id = 1; 
	$record_info = GeneralSettings::find_by_id($record_id); 
	$time_zones = TimeZone::find_all(); 
	$translate_languages = Localization::find_all(); 
	$front_languages = Localization::find_all(); 
	
	//check id access    
	require_once("../layout/header.php"); 
?> 
  <script type="text/javascript" src="../../js-crud/crud_general_settings.js"></script>  
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4> General Settings Module</h4> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Edit Settings</div> 
              <div class="panel-body">  
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
                  <input type="hidden" id="process_type" value="update"> 
                  <input type="hidden" id="record" value="<?php  echo $record_id; ?>"> 
                  <div class="form-group"> 
                    <label  class="col-lg-3 ">Title:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="title" placeholder=" " value="<?php echo $record_info->title; ?>" autocomplete="off"> 
                    </div> 
                  </div> 
                   <div class="form-group"> 
                    <label  class="col-lg-3 ">Site Url:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="site_url" placeholder=" " value="<?php echo $record_info->site_url; ?>" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">Time Zone:</label> 
                    <div class="col-lg-8"> 
                      <select class="form-control" id="time_zone"> 
                        <option value=""> Select Time Zone </option> 
                        <?php 
					 	 foreach($time_zones as $time_zone){ 
							 echo "<option value='$time_zone->id' "; 
							 if($time_zone->id==$record_info->time_zone_id){  
							   echo "selected"; 
							} 
							 echo " >$time_zone->name</option>"; 
						 }?> 
                      </select> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">Transalte Language:</label> 
                    <div class="col-lg-8"> 
                      <select class="form-control" id="translate_lang"> 
                        <option value=""> Select Language </option> 
                        <?php 
					 	 foreach($translate_languages as $translate_language){ 
							 echo "<option value='$translate_language->id' "; 
							 if($translate_language->id==$record_info->translate_lang_id){  
							   echo "selected"; 
							} 
							 echo " >$translate_language->name</option>"; 
						 }?> 
                      </select> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">Front Language:</label> 
                    <div class="col-lg-8"> 
                      <select class="form-control" id="front_lang"> 
                        <option value=""> Select Language </option> 
                        <?php 
					 	 foreach($front_languages as $front_language){ 
							 echo "<option value='$front_language->id' "; 
							 if($front_language->id == $record_info->front_lang_id){  
							   echo "selected"; 
							} 
							 echo " >$front_language->name</option>"; 
						 }?> 
                      </select> 
                    </div> 
                  </div> 
                   
                   
                  <div class="form-group"> 
                    <label  class="col-lg-3 ">Email:</label> 
                    <div class="col-lg-8"> 
                      <input type="email" class="form-control" id="email" placeholder=" " value="<?php echo $record_info->email; ?>" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label class="col-lg-3 ">Enable website:</label> 
                    <div class="col-lg-8"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="enable_website" class="radio" value="yes" <?php if($record_info->enable_website=="yes") echo 'checked'?>> 
                        yes</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="enable_website" class="radio" value="no"  <?php if($record_info->enable_website=="no") echo 'checked'?> > 
                        No</label> 
                    </div> 
                  </div>
                   
                  <div class="form-group"> 
                    <label  class="col-lg-3 ">Offline Messages:</label> 
                    <div class="col-lg-8"> 
                      <textarea class="form-control" id="offile_messeges" rows="5"><?php echo $record_info->offline_messages; ?></textarea> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-3">Meta Keys:</label> 
                    <div class="col-lg-8"> 
                     <textarea class="form-control" id="meta_key"><?php echo $record_info->meta_key; ?></textarea> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-3 ">Description:</label> 
                    <div class="col-lg-8"> 
                      <textarea class="form-control" id="description" rows="5"><?php echo $record_info->description; ?></textarea> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-3 ">Google Analytic:</label> 
                    <div class="col-lg-8"> 
                      <textarea class="form-control" id="google_analitic" rows="5"><?php echo $record_info->google_analitic; ?></textarea> 
                    </div> 
                  </div>
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
      </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start-->  
  <?php require_once("../layout/footer.php");?>