<?php 
require_once("../layout/initialize.php");
require_once("../layout/header.php");
$attributes = Attributes::find_all();
if(isset($_GET['form_id'])){
	$form_id = $_GET['form_id'];
}else{
	 redirect_to('../forms/view.php');	
}


include("../../assets/texteditor4/head.php"); ?>

  <!--header end--> 
  <!--sidebar start-->
  <?php require_once("../layout/navigation.php");?>
  <!--sidebar end--> 
  <script type="text/javascript" src="../../js-crud/crud_form_attributes.js"></script>
  <!--main content start-->
  <section id="main-content">
    <section class="wrapper site-min-height">
      <h4>Forms</h4>
      <!-- page start-->
      <div class="row">
        <aside class="col-lg-10">
          <section>
            <div class="panel">
              <div class="panel-heading"> Add  Form Attributes </div>
              <div class="panel-body">
                 <form class="form-horizontal tasi-form" role="form" action="data_model/insert.php"  id="form_crud">
                  <input type="hidden" id="process_type" value="insert">
                  <input type="hidden" id="form_id" value="<?php echo $form_id?>" />
                   <div class="form-group">
                     <label class="col-lg-2"> Select Attribute:</label>
                            <div class="col-lg-8">
                           <select class="form-control" id="attribute" >
                        <option value=""> Select Attributes </option>
                        <?php
					 	 foreach($attributes as $att){
							 
							 echo "<option value='$att->id'>$att->type</option>";
						 }?>
                      </select>
                </div>
              </div>
             <div class="form-group">
             <label for="" class="col-lg-2 "> Attributes Label:</label>
              <div class="col-lg-8">
                <input type="text" class="form-control" id="label" name="label">
               </div>
             </div>
             <div class="form-group">
               <label  class="col-lg-2">Sorting:</label>
                  <div class="col-lg-8">
                    <input type="text" class="form-control" id="sorting" placeholder=" " autocomplete="off" style="width:50px;"/>
                  </div>
                </div>
                <div class="form-group">
                        <label class="col-lg-2">Required:</label>
                        <div class="col-lg-8">
                          <label class="checkbox-inline">
                            <input type="radio" name="required" class="radio" value="yes" >
                            Yes</label>
                          <label class="checkbox-inline">
                            <input type="radio" name="required" class="radio" value="no" checked>
                            No</label>
                        </div>
                      </div>
               <div class="form-group">
                <label class="col-lg-2">Values:</label>
                <div class="col-lg-8">
                 <input type="text" class="form-control" id="att_values"  placeholder="Separate between each value with comma value1,value2,...">
                </div>
              </div>
                    
                        
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-6">
                            <button type="submit" id="submit" class="btn btn-info">Save</button>
                             <button type="submit" class="btn btn-default">Cancel</button>
                             <div id="loading_data"></div
                        ></div>
                    </div>
            </form>
                             
              </div>
            </div>
          </section>
        </aside>
        
      </div>
      <!-- page end--> 
    </section>
  </section>
  <!--main content end--> 
  <!--footer start-->
  
  <?php require_once("../layout/footer.php");?>