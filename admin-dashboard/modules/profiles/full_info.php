<?php 
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$define_profile = new Profile(); 
	    $define_profile->enable_relation(); 
	    $record_info = $define_profile->profile_data(null,null,$record_id); 
		//profile modules 
		$define_profile_module = new ProfileModulesAccess(); 
		$define_profile_module->enable_relation(); 
		$modules = $define_profile_module->user_profile_modules_data($record_id); 
		 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php");	 
		} 
	}else{ 
		redirect_to("view.php");	 
	} 
	require_once("../layout/header.php");	 
?> 
<?php require_once("../layout/header.php");?> 
  <script type="text/javascript" src="../../js-crud/crud_post_change_cover.js"></script>  
   
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Profile Module</h4> 
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Profile Info</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud"> 
                  <input type="hidden" id="record" value="<?php echo $record_id?>"/> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">Title:</label> 
                    <div class="col-lg-4" ><?php echo $record_info->title?> </div> 
                  </div> 
                   
                  <div class="form-group"> 
                    <label  class="col-lg-2">Modules:</label> 
                    <div class="col-lg-8">  <?php foreach($modules as $module){ 
						echo "- ".$module->module_title ."<br>"; 
					} 
					 
					 
					?>  
                    
                     
                     
                    </div> 
                  </div> 
                 
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'update.php?id='+<?php echo $record_id?>" > 
                      <li class="icon-edit-sign"></li> 
                      Update </button> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-4"> 
          <section class="panel "> 
            <header class=" panel-heading  tab-bg-dark-navy-blue"> 
              <ul class="nav nav-tabs"> 
                <li class="center-block active"><a data-toggle="tab" href="#op1" class="text-center"> <i class=" icon-check"></i><strong> Entry Info</strong></a></li> 
                <li > <a data-toggle="tab" href="#op2" class="text-center"> <i class=" icon-check"></i> <strong> Rules </strong> </a> </li> 
                <li > <a data-toggle="tab" href="#op3" class="text-center"> <i class=" icon-calendar "></i> <strong> Modules Option</strong> </a> </li> 
              </ul> 
            </header> 
            <div class="panel-body"> 
              <div id="list_info"> 
                <ul> 
                  <div class="tab-content"> 
                    <div id="op1" class="tab-pane active "> <br /> 
                  <li><span style="color:#428bca">> Created By:</span> <?php echo $record_info->user_name?></li> 
                  <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                  <li><span style="color:#428bca">> Last Update Date:</span> 
                    <?php  
						if($record_info->last_update != "0000-00-00 00:00:00"){ 
					 	 	echo $record_info->last_update; 
				      	}else{ 
						  echo "--"; 
					  	} 
					  ?> 
                  </li> 
                    </div> 
                    <div id="op2" class="tab-pane"> <br /> 
                      <li><span style="color:#428bca">> Global Edit:</span> <?php echo $record_info->global_edit?></li> 
                      <li><span style="color:#428bca">> Global Delete:</span> <?php echo $record_info->global_delete?></li> 
                      <li><span style="color:#428bca">> Developer Mode:</span> <?php echo $record_info->developer_mode?></li> 
                      <li><span style="color:#428bca">> Profile Bloke:</span> <?php echo $record_info->profile_block ?></li> 
                     </div> 
                    <div id="op3"  class="tab-pane  "> 
                      <li><span style="color:#428bca">> Post Publishing:</span> <?php echo $record_info->post_publishing ?></li> 
                      <li><span style="color:#428bca">> Event Publishing:</span> <?php echo $record_info->event_publishing ?></li> 
                      <li><span style="color:#428bca">> Page Publishing:</span> <?php echo $record_info->page_publishing ?></li> 
                      
                    </div> 
                  </div> 
                </ul> 
              </div> 
            </div> 
          </section> 
           
        </div> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>