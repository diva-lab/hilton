<?php 
	$opened_module_page_fullinfo = $module_name.'/full_info'; 
	$opened_module_page_update = $module_name.'/update'; 
	$opened_module_page_delete = $module_name.'/trash';	 
	//full info 
 	if(!in_array($opened_module_page_fullinfo, $user_allowed_page_array)){ 
		echo " <a href='full_info.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
				  data-original-title='Full info' disabled>&nbsp;<i class='icon-info'></i>&nbsp;</a>";	 
	}else{ 
		echo " <a href='full_info.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
				  data-original-title='Full info' >&nbsp;<i class='icon-info'></i>&nbsp;</a>";	 
	} 
	//update 
 	if(!in_array($opened_module_page_update, $user_allowed_page_array)){ 
		 echo " <a href='update.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
		   data-original-title='Edit' disabled >  <i class='icon-edit'></i></a>"; 
	}else{ 
		if($user_profile->global_edit == 'all_records' || $record->inserted_by == $user_data->user_name){ 
			echo " <a href='update.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
			data-original-title='Edit' >  <i class='icon-edit'></i></a>"; 
		}else { 
		   echo "<a href='update.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
		   data-original-title='Edit' disabled >  <i class='icon-edit'></i></a>"; 
		}	 
	} 
	//delete	 
 	if(!in_array($opened_module_page_delete, $user_allowed_page_array)){ 
		 	echo " <a href='#my{$record->id}' data-toggle='modal' class='btn btn-primary btn-xs tooltips'  
			data-placement='top' data-original-title='move to trash' disabled>  <i class='icon-trash'></i></a>"; 
	}else{ 
		if($user_profile->global_delete == 'all_records' || $record->inserted_by == $user_data->user_name){ 
		 	echo " <a href='#my{$record->id}' data-toggle='modal' class='btn btn-primary btn-xs tooltips'  
			data-placement='top' data-original-title='move to trash'  >  <i class='icon-trash'></i></a>"; 
		}else{ 
		 	echo " <a href='#my{$record->id}' data-toggle='modal' class='btn btn-primary btn-xs tooltips'  
			data-placement='top' data-original-title='move to trash' disabled>  <i class='icon-trash'></i></a>"; 
		}		 
	} 
	  
?>