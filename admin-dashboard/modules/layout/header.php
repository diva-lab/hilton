<!DOCTYPE html> 
<html lang="en"> 
<head> 
	<meta charset="UTF-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="shortcut icon" href="../../img/favicon.png"> 
    <title>Diva-CMS</title>     
    <link rel="stylesheet" type="text/css" href="../../js/fancybox/jquery.fancybox-1.3.4.css" media="screen" /> 
    <link rel="stylesheet" type="text/css" href="../../js/jquery.datetimepicker.css"/> 
 	<link rel="stylesheet" href="../../js/fancybox/style.css" /> 
     <link href="../../css/bootstrap.min.css" rel="stylesheet"> 
    <link href="../../css/bootstrap-reset.css" rel="stylesheet"> 
    <!--external css--> 
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /> 
	<!--table css--> 
    <link rel="stylesheet" href="../../assets/data-tables/DT_bootstrap.css" /> 
    <!-- Custom styles for this template --> 
    <link href="../../css/style.css" rel="stylesheet"> 
     <link type="text/css" href="../../assets/selectmultiple_drag_drop/css/jquery-ui.css" rel="stylesheet" /> 
    <link type="text/css" href="../../assets/selectmultiple_drag_drop/css/ui.multiselect.css" rel="stylesheet" /> 
    <link href="../../css/style-responsive.css" rel="stylesheet" /> 
    <link href="../../assets/dropzone/css/dropzone.css" rel="stylesheet"/> 
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-fileupload/bootstrap-fileupload.css" /> 
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" /> 
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-datepicker/css/datepicker.css" /> 
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-timepicker/compiled/timepicker.css" /> 
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-colorpicker/css/colorpicker.css" /> 
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-daterangepicker/daterangepicker-bs3.css" /> 
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap-datetimepicker/css/datetimepicker.css" />   
    <link rel="stylesheet" type="text/css" href="../../assets/jquery-multi-select/css/multi-select.css" />   
    <link rel="stylesheet" type="text/css" href="../../assets/gritter/css/jquery.gritter.css" /> 
    <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
   <script src="../../js/jquery-1.8.3.min.js"></script>  
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries --> 
    <!--[if lt IE 9]> 
      <script src="js/html5shiv.js"></script> 
      <script src="js/respond.min.js"></script> 
    <![endif]--> 
  </head> 
<body> 
<section id="container" class=""> 
<!--header start--> 
<header class="header white-bg"> 
  <div class="sidebar-toggle-box"> 
    <div data-original-title="Toggle Navigation" data-placement="right" class="icon-reorder tooltips"></div> 
  </div> 
  <!--logo start-->  
  <a href="../../index.php" class="logo" >Diva<span>lab</span></a>  
  <!--logo end--> 
  <div class="nav notify-row" id="top_menu">  
    <!--  notification start --> 
    <ul class="nav top-menu"> 
      <!-- settings start --> 
       
      <!-- settings end -->  
      <!-- inbox dropdown start--> 
     
     
        
          
          
           
           
         
      </li> 
       
    </ul> 
  </div> 
  <div class="top-nav "> 
    <ul class="nav pull-right top-menu"> 
<!--      <li> 
        <input type="text" class="form-control search" style="margin-top:2px;" placeholder=" Search"> 
      </li>--> 
      <!--<li class="dropdown language"> 
                      <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="#"> 
                          <img src="../../img/flags/us.png" alt=""> 
                          <span class="username">US</span> 
                          <b class="caret"></b> 
                      </a> 
                      <ul class="dropdown-menu"> 
                          <li><a href="#"><img src="../../img/flags/es.png" alt=""> Spanish</a></li> 
                          <li><a href="#"><img src="../../img/flags/de.png" alt=""> German</a></li> 
                          <li><a href="#"><img src="../../img/flags/ru.png" alt=""> Russian</a></li> 
                          <li><a href="#"><img src="../../img/flags/fr.png" alt=""> French</a></li> 
                      </ul> 
                  </li>-->  
      <!-- user login dropdown start--> 
      <li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <span class="username">Welcome: <?php echo $session->user_name?></span> <b class="caret"></b> </a> 
        <ul class="dropdown-menu extended logout"> 
          <div class="log-arrow-up"></div> 
          <li><a href="../user-profile/profile_info.php"><i class=" icon-suitcase"></i>Profile</a></li> 
          <li><a href="#"><i class="icon-cog"></i> Settings</a></li> 
          <li><a href="#"><i class="icon-bell-alt"></i> Notification</a></li> 
          <li><a href="../../logout.php"><i class="icon-key"></i> Log Out</a></li> 
        </ul> 
      </li> 
      <!-- user login dropdown end --> 
    </ul> 
  </div> 
</header> 
<script> 
//for alias 
//replace spaces with _ underscore 
function add_char(field1,field2) { 
	var field1 = document.getElementById(field1).value; 
	var field_lowercase = field1.toLowerCase(); 
	document.getElementById(field2).value = field_lowercase.replace(/ |!|@|#|\$|%|\^|\&|\*|\"|\'|\’|\:|\?|\)|\)/g, "_"); 
} 

</script> 
<script>
$(document).on('keyup keypress', 'form input[type="text"]', function(e) {
 if(e.which == 13) {
 e.preventDefault();
 return false;
 }
});


</script>
 <?php require_once("fancybox.php")?>