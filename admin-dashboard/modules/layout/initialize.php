<?php 
require_once("../../../classes/Session.php"); 
require_once("../../../classes/Functions.php"); 
require_once("../../../classes/MysqlDatabase.php"); 
require_once("../../../classes/GeneralSettings.php"); 
require_once("../../../classes/TimeZone.php"); 
require_once("../../../classes/Localization.php"); 
require_once("../../../classes/CMSModules.php"); 
require_once("../../../classes/AdvertisementContent.php"); 
require_once("../../../classes/MediaLibraryDirectories.php"); 
require_once("../../../classes/MediaLibraryFiles.php"); 
require_once("../../../classes/MenuGroup.php"); 
require_once("../../../classes/Forms.php"); 
require_once("../../../classes/FormAttributes.php"); 
require_once("../../../classes/FormInsertedData.php"); 
require_once("../../../classes/Attributes.php"); 
require_once("../../../classes/MenuLink.php"); 
require_once("../../../classes/MenuLinkContent.php"); 
require_once("../../../classes/TaxonomiesContent.php"); 
require_once("../../../classes/Advertisements.php"); 
require_once("../../../classes/Plugins.php"); 
require_once("../../../classes/NodesSelectedTaxonomies.php"); 
require_once("../../../classes/Nodes.php"); 
require_once("../../../classes/NodesPluginsValues.php"); 
require_once("../../../classes/Profile.php"); 
require_once("../../../classes/ProfileModulesAccess.php"); 
require_once("../../../classes/ProfilePagesAccess.php"); 
require_once("../../../classes/resize_class.php"); 
require_once("../../../classes/SocialComments.php"); 
require_once("../../../classes/SocialEmailSubscription.php"); 
require_once("../../../classes/Taxonomies.php"); 
require_once("../../../classes/Themes.php"); 
require_once("../../../classes/ThemesLayouts.php"); 
require_once("../../../classes/UploadFile.php"); 
require_once("../../../classes/Users.php"); 
require_once("../../../classes/NodesContent.php"); 
require_once("../../../classes/ThemeLayoutModel.php"); 
require_once("../../../classes/ThemeLayoutModelPlugin.php"); 
require_once("../../../classes/NodesPluginsValues.php"); 
require_once("../../../classes/NodesImageGallery.php"); 
require_once("../../../classes/Pagination.php"); 
require_once("../../../classes/EventDetails.php");
require_once("../../../classes/PollQuestions.php");
require_once("../../../classes/PollQuestionsOptions.php");
require_once("../../../classes/ContactUs.php");
require_once("../../../classes/HomePageLayout.php");
require_once("../../../classes/CustomerInfo.php");
require_once("../../../classes/UsersFeeds.php");



//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
// user log in profile details to check authority 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
//chech if profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../index.php");	 
} 
//get main lnaguage info 
$define_general_setting = new GeneralSettings(); 
$define_general_setting->enable_relation(); 
$general_setting_info = $define_general_setting->general_settings_data();
$main_lang_id = $general_setting_info->translate_lang_id; 
//main language info 
$return_current_lang_info = Localization::find_by_id($main_lang_id); 
//get opened module and page 
$currentFile = $_SERVER["PHP_SELF"]; 
$opened_url_parts = Explode('/', $currentFile); 
$php_page = str_replace(".php", '', $opened_url_parts[count($opened_url_parts) - 1]); 
$completed_url = $opened_url_parts[count($opened_url_parts) - 2]."/".$php_page;   
//get user allowed pages 
//perfome if not home page  
$global_link = array("home/index","user-profile/profile_info","user-profile/update_info","user-profile/update_password"); 
if(!in_array($completed_url,$global_link)){ 
	$user_pages_access =  new ProfilePagesAccess(); 
	$user_pages_access->enable_relation(); 
	$user_allowed_pages = $user_pages_access->user_profile_pages_access($user_data->user_profile, NULL); 
	$user_allowed_page_array = array(); 
	foreach($user_allowed_pages as $pages){ 
		$user_allowed_page_array[] = $pages->source; 
	} 
	if(!in_array($completed_url, $user_allowed_page_array)){ 
		redirect_to("../../restrict.html");	 
	}	 
} 
?>