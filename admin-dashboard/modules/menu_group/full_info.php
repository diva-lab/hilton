<?php 
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$define_class = new MenuGroup(); 
		$define_class->enable_relation(); 
		$record_info =$define_class->menu_group_data(null,null,$record_id); 
		$translate_lang = new GeneralSettings(); 
	    $translate_lang->enable_relation(); 
	    $translate_lang_data = $translate_lang->general_settings_data(); 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php");} 
	}else{ 
		redirect_to("view.php");} 
	require_once("../layout/header.php"); 
?> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4> Structure Menu Group  Module</h4>  
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-7"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Menu Group Info</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud"> 
                <input type="hidden" id="process_type" value="insert"> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">Title:</label> 
                    <div class="col-lg-6" > 
                    <?php echo $record_info->title?> 
                    </div> 
                  </div> 
					<div class="form-group"> 
                    <label  class="col-lg-2 ">Alias:</label> 
                    <div class="col-lg-6" > 
                    <?php echo $record_info->alias?> 
                    </div> 
                  </div>                    
                <div class="form-group"> 
                    <label  class="col-lg-2 ">Description:</label> 
                    <div class="col-lg-6" > 
                    <?php echo html_entity_decode($record_info->description)?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'update.php?id='+<?php echo $record_id?>" > <li class="icon-edit-sign"></li> Update </button> 
                         <button type="button" class="btn btn-info" 
                onClick="window.location.href = 'view.php?menu_id='+<?php echo $record_info->id?>+'&lang='+<?php echo $translate_lang_data-> 
				translate_lang_id;?>" > 
                      <i class="icon-edit-sign"></i> 
                      View Links </button> 
                        </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-4"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Entry Information: </header> 
            <div class="panel-body"> 
              <div id="list_info"> 
                <ul> 
                    <li><span style="color:#428bca">> Created By:</span> <?php echo $record_info->inserted_by?></li> 
                  <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                  <li><span style="color:#428bca">> Last Update By:</span> <?php if($record_info->update_by!=""){ 
					  echo $record_info->update_by; 
				      }else{ 
						  echo "--";} 
					  ?></li> 
                  <li><span style="color:#428bca">> Last Update Date:</span> <?php if($record_info->last_update!="0000-00-00 00:00:00"){ 
					  echo $record_info->last_update; 
				      }else{ 
						  echo "--";} 
					  ?></li> 
                </ul> 
              </div> 
            </div> 
          </section> 
         </div> 
         <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading">  Included Images  :</header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_image"> 
            <div class="form-group"> 
                  <label  class="col-lg-4 ">Image Cover:</label> 
                  <div class="col-lg-6">  
                   <img src="../../../media-library/group/<?php echo $record_info->image?>" id="imageSrc" style="width:80px; height:80px;"></div> 
                 
                </div> 
              
            </form> 
          </div> 
        </section> 
      </div> 
      </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  
  <?php require_once("../layout/footer.php");?>