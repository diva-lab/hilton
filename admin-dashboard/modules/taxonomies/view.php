<?php 
	require_once("../layout/initialize.php"); 
	$define_class= new Taxonomies(); 
	$define_class->enable_relation(); 
 	//get texonomy content class 
    $define_taxonomy_content = new TaxonomiesContent(); 
	require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<script src="../../js-crud/crud_taxonomy_category.js"></script> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Taxonomy Categories Module</h4> 
    <section class="panel"> 
      <header class="panel-heading"> View Taxonomy Category </header> 
      <div class="panel-body"> 
      <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'"  
		<?php 
		  $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
		  $opened_module_page_insert = $module_name.'/insert'; 
		  if(!in_array($opened_module_page_insert, $user_allowed_page_array)){ 
			echo "disabled"; 
		  } 
		  ?>> Add New Category 
       </button>
       <br />
       <form id="form_categories" >
      <div class="form-group "> 
            <label  class="col-lg-2">Search:</label> 
            <div class="col-lg-3"> 
              <input type="text" class="form-control" id="search_input" placeholder=" " autocomplete="off" style="width:250px" /> 
            </div> 
          </div> 
          </form>
          <br />
          <br />
          <br />
         <div class="adv-table editable-table">  
         
            <table class="table table-striped table-hover table-bordered" > 
             <thead> 
              <tr> 
                <th>Name</th> 
                <th class=""> Created Date </th> 
               <th>Action</th> 
              </tr> 
            </thead> 
              <tbody id="myTable">
               <?php  
				$define_class->enable_relation(); 
				$categories = $define_class->get_categories(0,0,$main_lang_id);
				if(!empty($categories)){ 
				foreach ($categories as $key=>$value) { 
					$record = Taxonomies::find_by_id($key); 
					echo "<tr> 
					 <td><a href='full_info.php?id={$record->id}'> {$value}</a></td> 
					 <td>{$record->inserted_date}</td> 
						<td>"; 
						 $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
						 include('../layout/btn_control.php'); 
						 "</td> 
					  </tr> 
					  <div class='modal fade' id='my{$key}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
										  <div class='modal-dialog'> 
											  <div class='modal-content'> 
												  <div class='modal-header'> 
													  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
													  <h4 class='modal-title'>Delete</h4> 
												  </div> 
												  <div class='modal-body'> 
												   <p> Are you sure you want delete  $record->name ??</p> 
												  </div> 
												  <div class='modal-footer'> 
													  <button class='btn btn-warning' type='button'  
													  onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$record->id}'\"/> Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
												  </div> 
											  </div> 
										  </div> 
									  </div>"; 
					 
				}
				}
				?>  
                
              </tbody> 
            </table> 
          </div> 
      </div> 
    </section> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 



