<?php 
require_once("../layout/initialize.php"); 
	//get directory data 
$docs = array(); 
$directory_title= $_GET['title']; 
$path = "../../../media-library/"; 
	//$records = array_diff(scandir($path.$directory_title), array('..', '.'));  
	foreach (glob($path.$directory_title."/*") as $path) { // lists all files in folder called "test" 
//foreach (glob("*.php") as $path) { // lists all files with .php extension in current folder 
  $docs[$path] = filectime($path); 
}  
arsort($docs); 

require_once("../layout/header.php"); 
?> 
<!--header end-->  
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Media Library Module</h4> 
    <!-- page start--> 
    <section class="panel"> 
      <header class="panel-heading"> View <?php echo $directory_title?> Files </header> 
      <br> 
      <button type="button" class="btn btn-danger" style="margin-left:15px"  
      onClick="window.location.href = 'insert.php?title=<?php echo $directory_title?>'">Add Media File  </button> 

      <button type="button"  id = "submit" class="btn btn-danger" style="margin-left:15px"  
>Delete  </button> 
      <br> 
      <br> 
      <div class="panel-body"> 
        <div class="adv-table editable-table ">  
            <!--<div class="clearfix"> 
                              <div class="btn-group"> 
                                  <button id="editable-sample_new" class="btn green"> 
                                      Add New <i class="icon-plus"></i> 
                                  </button> 
                              </div> 
                              <div class="btn-group pull-right"> 
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i> 
                                  </button> 
                                  <ul class="dropdown-menu pull-right"> 
                                      <li><a href="#">Print</a></li> 
                                      <li><a href="#">Save as PDF</a></li> 
                                      <li><a href="#">Export to Excel</a></li> 
                                  </ul> 
                              </div> 
                            </div>--> 
                            <div class="space15"></div> 
                            <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
                              <thead> 
                                <tr> 
                                  <th style="width:20px">#</th> 
                                  <th style="width:180px;" align="center" >&nbsp;</th> 
                                  <th>File Name  &nbsp; &nbsp;<input type="checkbox" id="selectall"></th> 
                                  <th> Action</th> 

                                </tr> 
                              </thead> 
                              <tbody> 
                                <?php   
                                $serialize = 1; 
                                foreach($docs as $record => $timestamp){ 
                                 $name = basename($record); 
                                 if($name != 'Thumbs.db'){ 

                                   echo "<tr> 
                                   <td>{$serialize}</td> 
                                   <td align='center'><input  type='checkbox' name='check[]' value='$name'>&nbsp; &nbsp;<img src='../../../media-library-thumb/{$directory_title}/small/$record' style='width:80px; height:80px;'></td> 
                                    <input type='hidden'  id='folder' value ='$directory_title'> <td>{$name}</a></td> 
                                   <td>"; 
					 //delete	 
                                    if($user_profile->global_delete == 'all_records'){ 
                                      echo " <a href='#my{$serialize}' data-toggle='modal' class='btn btn-primary btn-xs tooltips' data-placement='top' 
                                      data-original-title='Delete'> 
                                      <i class='icon-remove'></i></a>"; 
                                    }else{ 
                                      echo " <a href= '' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip' 
                                      data-original-title='Delete' disabled > 
                                      <i class='icon-remove'></i></a>"; 
                                    }		 echo " </td>
                                  </tr> 
                                  <div class='modal fade' id='my{$serialize}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
                                    <div class='modal-dialog'> 
                                      <div class='modal-content'> 
                                        <div class='modal-header'> 
                                          <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
                                          <h4 class='modal-title'>Delete</h4> 
                                        </div> 
                                        <div class='modal-body'> 
                                         <p> Are you sure you want delete  $name ??</p> 
                                       </div> 
                                       <div class='modal-footer'> 

                                        <button class='btn btn-warning' type='button'  
                                        onClick=\"window.location.href = 'data_model/delete.php?task=delete&title={$name}&dir={$directory_title}'\"/> Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
                                      </div> 
                                    </div> 
                                  </div> 
                                </div>"; 



                                echo "</tr>"; 
                                $serialize++; 
                              } 
                            } 
                            ?> 
                          </tbody> 
                        </table> 
                      </div> 
                    </div> 
                  </section> 
                  <!-- page end-->  
                </section> 
              </section> 
              <!--main content end-->  
              <!--footer start--> 
              <?php require_once("../layout/footer.php");?> 
              <script src="../../js-crud/multiple_delete.js" type="text/javascript" ></script>


