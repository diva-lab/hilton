<?php 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 

  // this is the response :
 header('Content-Type: application/json');

// user log in profile details to chech authority 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
if($user_profile->global_delete == 'all_records'){ 
	if(!empty($_POST["task"]) && $_POST["task"] == "delete"){ 
                
                $file_names= $_POST['ids']; 
	   $dir_name  = $_POST['folder_name']; 
	   
	   $path = "../../../../media-library/".$dir_name."/"; 
	   $path_thumb = "../../../../media-library-thumb/".$dir_name."/"; 
	   foreach($file_names as $file_name){
	    if(file_exists($path.$file_name)){ 
		   $delete = unlink($path.$file_name); 
			 if($delete){ 
			   unlink($path_thumb."/small/".$file_name); 
			   unlink($path_thumb."/medium/".$file_name); 
			   unlink($path_thumb."/large/".$file_name); 
			  }
	  
				}
             }

                      $data  = array("status"=>"delete");
						  echo json_encode($data);

                                          }

                        }else{ 
	                        $data  = array("status"=>"error");
		           echo json_encode($data);
	 
	                      } 
?>