<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/PollQuestionsOptions.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
	//get data 
	$id = $_GET['id']; 
	//find record	 
	$find_poll_option = PollQuestionsOptions::find_by_id($id); 
	$poll_id = $find_poll_option->poll_id; 
	//if there is record perform delete 
	//if there is no record go back to view 
	if($find_poll_option){ 
		$delete = $find_poll_option->delete(); 
		if($delete){ 
				redirect_to("../view.php?poll_id={$poll_id}");	 
		}else{ 
				redirect_to("../view.php?poll_id={$poll_id}");	 
		}	 
		//if there is no record go back to view 
	}else{ 
		redirect_to("../view.php?poll_id={$poll_id}");	 
	}  
}else{ 
	//if task wasnot delete go back to view 
	redirect_to("../view.php?poll_id={$poll_id}");	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>