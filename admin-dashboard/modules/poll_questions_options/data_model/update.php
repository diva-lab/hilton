<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/PollQuestionsOptions.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	//get data 
	$id = $_POST['record']; 
	//update	 
	$edit = PollQuestionsOptions::find_by_id($id); 
	$edit->poll_option = $_POST["option"]; 
	$edit->poll_id = $_POST["poll_id"]; 
	$edit->last_update = date_now(); 
	$edit->update_by = $session->user_id; 
	$update = $edit->update(); 
 	header('Content-Type: application/json'); 
	if($update){ 
		$data  = array("status"=>"work"); 
		echo json_encode($data); 
	}else{ 
		$data  = array("status"=>"error"); 
		echo json_encode($data); 
	} 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>