<!DOCTYPE html> 
<html lang="en"> 
  <head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta name="description" content=""> 
    <meta name="author" content="Mosaddek"> 
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina"> 
    <!-- Bootstrap core CSS --> 
    <link href="../../css/bootstrap.min.css" rel="stylesheet"> 
    <link href="../../css/bootstrap-reset.css" rel="stylesheet"> 
    <!--external css--> 
    <link rel="stylesheet" type="text/css" href="../../assets/gritter/css/jquery.gritter.css" /> 
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /> 
     <link href="../../assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" /> 
    <link href="../../assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" /> 
     <link href="../../css/style.css" rel="stylesheet"> 
    <link href="../../css/style-responsive.css" rel="stylesheet" /> 
   <?php  
    require_once("../../../classes/Posts.php"); 
	require_once("../../../classes/PostCategories.php"); 
	require_once("../../../classes/Pages.php"); 
	require_once("../../../classes/PageCategories.php"); 
	require_once("../../../classes/Events.php"); 
	require_once("../../../classes/EventCategories.php"); 
	 
	// get category id 
	$id = $_GET['id']; 
    
	?> 
    <!-- Custom styles for this template --> 
     
 </head> 
 <body style="background:#fff"> 
  <section class="panel"> 
   <header class="panel-heading">Show Related Data </header> 
   <header class="panel-heading tab-bg-dark-navy-blue"> 
  <ul class="nav nav-tabs"> 
      <li  class=" center-block active" style="width:310px;"> 
          <a data-toggle="tab" href="#home-2" class="text-center"> 
              <i class="icon-pushpin"></i> 
             <strong> Posts</strong> 
          </a> 
      </li> 
      <li style="width:310px;"> 
          <a data-toggle="tab" href="#page" class="text-center"> 
              <i class=" icon-file-text "></i> 
             <strong> Pages</strong> 
          </a> 
      </li> 
      <li style="width:368px;"> 
          <a data-toggle="tab" href="#event" class="text-center"> 
              <i class="icon-calendar"></i> 
              <strong>Events</strong> 
          </a> 
      </li> 
  </ul> 
  </header> 
<div class="panel-body"> 
 <div class="tab-content"> 
  <div id="home-2" class="tab-pane active "> 
  <div class="adv-table"> 
  <table  class="display table table-bordered table-striped" id="example2"> 
     
           <thead> 
               <tr> 
				  <th>#</th> 
				  <th><i class=""></i> Title</th> 
                   <th><i class=""></i> Langage</th> 
				  <th><i class=""></i> Categories</th> 
				  <th><i class=""></i> Created Date</th> 
				 </tr> 
              </thead> 
              <tbody> 
                <?php  
				$posts = Posts::find_all_by_custom_filed("inserted_by",$id,"inserted_date","DESC"); 
				 
				 
				 $serialize = 1; 
				 foreach($posts as $post){ 
					 //get post categories 
				  $post_categories_class = new PostCategories(); 
				  $post_categories_class->enable_relation(); 
				  $post_categories = $post_categories_class->return_post_categories($post->id); 
				  $post_class = new Posts; 
				  $post_class->enable_relation(); 
				  $post_data = $post_class->post_data(null,null,$post->id); 
	           
					 //get post categories 
					  				 
				 echo "<tr> 
				  <td>{$serialize}</td> 
				  <td><a href='../posts/full_info.php?id={$post_data->id}'>{$post_data->title}</a></td> 
				   <td>{$post_data->lang_name}</td> 
				   
				 <td>";  
					foreach($post_categories as $category){ 
						echo "{$category->category_name} <br/>"; 
					} 
				  echo "</td> 
				  <td>{$post_data->inserted_date}</td> 
				  				   
				   
			  </tr>"; 
				  $serialize++; 
				  }?> 
                   
            </tbody> 
      </table> 
      </div> 
      </div> 
      <!-- page tab--> 
      <div id="page" class="tab-pane"> 
   <div class="adv-table"> 
  <table  class="display table table-bordered table-striped" id="example1"> 
           <thead> 
                <tr> 
                <th>#</th> 
                <th><i class=""></i> Title</th> 
                <th><i class=""></i> Langage</th> 
                <th><i class=""></i> Categories</th> 
                <th><i class=""></i> Created Date</th>                
                  
                </tr> 
              </thead> 
              <tbody> 
                <?php  
				 $serialize = 1; 
			   $pages = Pages::find_all_by_custom_filed("inserted_by",$id,"inserted_date","DESC"); 
			   foreach($pages as $page){ 
				 $post_categories_class = new PageCategories(); 
				  $post_categories_class->enable_relation(); 
				  $post_categories = $post_categories_class->get_page_categories($page->id);	 
				  $page_class = new Pages; 
				  $page_class->enable_relation(); 
				  $page_data = $page_class->page_data(null,null,$page->id); 
				  				 
			 echo "<tr> 
				  <td>{$serialize}</td> 
				  <td><a href=../pages/'full_info.php?id={$page_data->id}'>{$page_data->title}</a></td> 
				  <td>{$page_data->lang_name}</td> 
				   
				 <td>";  
					foreach($post_categories as $category){ 
						echo "{$category->category_name} <br/>"; 
					} 
				  echo "</td> 
				  <td>{$page_data->inserted_date}</td> 
				 			   
				  
					   
				  </tr>"; 
				  $serialize++; 
				  }?> 
                   
            </tbody> 
      </table> 
      </div> 
      </div> 
      <!-- end page tab--> 
      <!-- event tab--> 
      <div id="event" class="tab-pane"> 
     <div class="adv-table"> 
  <table  class="display table table-bordered table-striped" id="example"> 
           <thead> 
                <tr> 
                <th>#</th> 
                <th><i class=""></i> Title</th> 
                <th>Langage</th> 
                <th><i class=""></i> Categories</th> 
                 <th><i class=""></i> Start Date</th> 
                 <th><i class=""></i> End Date</th> 
                <th><i class=""></i> Created Date</th>      
                </tr> 
              </thead> 
              <tbody> 
                <?php  
				 
				$serialize = 1; 
			   $events= Events::find_all_by_custom_filed("inserted_by",$id,"inserted_date","DESC"); 
			   foreach($events as $event){ 
					 //get post categories 
				  $event_categories_class = new EventCategories(); 
				  $event_categories_class->enable_relation(); 
				  $event_categories = $event_categories_class->get_event_categories($event->id);	 
				  $event_class = new Events; 
				  $event_class->enable_relation(); 
				  $record = $event_class->event_data(null,null,$event->id); 
				  				 
					  				 
				 echo "<tr> 
					  <td>{$serialize}</td> 
                       <td><a href='../events/full_info.php?id={$record->id}'>{$record->title}</a></td> 
					   <td>{$record->lang_name}</td> 
					   <td>";  
					   	foreach($event_categories as $category){ 
						echo "{$category->category_name} <br/>"; 
					} 
					 
				  echo "</td> 
					   <td>{$record->start_date}</td> 
					   <td>{$record->end_date}</td> 
					   <td>{$record->inserted_date}</td> 
					 			   
					   
				  </tr>"; 
				  $serialize++; 
				  }?> 
                   
            </tbody> 
      </table> 
      </div> 
      </div> 
      <!-- event tab--> 
      </div> 
      </div> 
     <!-- END JAVASCRIPTS --> 
      <script src="../../js/jquery.js"></script> 
    <script src="../../js/bootstrap.min.js"></script> 
    <script class="include" type="text/javascript" src="../../js/jquery.dcjqaccordion.2.7.js"></script> 
    <script src="../../js/jquery.scrollTo.min.js"></script> 
    <script src="../../js/jquery.nicescroll.js" type="text/javascript"></script> 
    <script src="../../js/respond.min.js" ></script> 
     <script type="text/javascript" src="../../assets/gritter/js/jquery.gritter.js"></script> 
    <script type="text/javascript" language="javascript" src="../../assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
    <!--common script for all pages--> 
    <script src="../../js/common-scripts.js"></script> 
    <script src="../../js/gritter.js" type="text/javascript"></script> 
    <script type="text/javascript" charset="utf-8"> 
          $(document).ready(function() { 
              $('#example2').dataTable( { 
                  "aaSorting": [[ 4, "desc" ]] 
              } ); 
			   $('#example1').dataTable( { 
                  "aaSorting": [[ 4, "desc" ]] 
              } ); 
			   $('#example').dataTable( { 
                  "aaSorting": [[ 4, "desc" ]] 
              } ); 
          } ); 
      </script> 
    </section> 
 </body> 
</html> 
