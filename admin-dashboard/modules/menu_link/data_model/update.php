<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/MenuLink.php'); 
require_once('../../../../classes/MenuLinkContent.php'); 
require_once('../../../../classes/Localization.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
$link_id = $_POST["record"]; 
$edit = MenuLink::find_by_id($link_id); 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
//check global edit and update authorization 
if($user_profile->global_edit != 'all_records' && $edit->inserted_by != $session->user_id ){ 
	redirect_to("../view.php?group_id=$edit->group_id"); 
}else{ 
	//send notifiction by json  
	header('Content-Type: application/json'); 
	$required_fields = array('sorting'=>'- Insert sorting'); 
	$check_required_fields = check_required_fields($required_fields); 
	if(count($check_required_fields) == 0){ 
		  $edit->icon = $_POST["icon"]; 
		  $edit->status = $_POST["shadow"]; 
		  if(!empty($_POST['imageVal'])){ 
			  $current_file = $_POST['imageVal']; 
			  $parts = explode('/',$current_file); 
			  $image_cover = $parts[count($parts)-1]; 
			  $folder = $parts[count($parts)-3]; 
			  $path = $folder."/".$image_cover; 
			  $edit->image = $path; 
		   } 
		  $edit->external_path = $_POST["external_path"]; 
		  $edit->parent_id = $_POST["sid"]; 
		  $edit->path_type=$_POST["path_type"]; 
		  $edit->drop_down = $_POST['drop_down']; 
		  $edit->drop_down_style = $_POST["drop_dwon_style"]; 
		  $edit->sorting = $_POST["sorting"]; 
		  $edit->path = $_POST["path"]; 
		  $edit->update_by = $session->user_id; 
		  $edit->last_update = date_now(); 
		  $update = $edit->update(); 
		  if($update){ 
			//retrieve all available languages 
			$languages = Localization::find_all('label','asc');			 
			//update page content  
			foreach($languages as $language){ 
				//check content id exist or not 
				//if exist update if not insert new record 
				$edit_content = MenuLinkContent::find_by_id($_POST['main_content']['content_id_'.$language->label]); 
				if($edit_content){ 
					$edit_content->title = $_POST['main_content']['title_'.$language->label]; 
					$edit_content->description = $_POST['main_content']['description_'.$language->label]; 
					$edit_content->update();				 
				}else{ 
					$add_content = new MenuLinkContent(); 
					$add_content->link_id = $link_id; 
					$add_content->lang_id = $language->id; 
					$add_content->title = $_POST['main_content']['title_'.$language->label]; 
					$add_content->description = $_POST['main_content']['description_'.$language->label]; 
					$add_content->insert(); 
				} 
			} 
			  $data  = array("status"=>"work","group_id"=>$link_id); 
			  echo json_encode($data); 
		  }else{ 
			  $data  = array("status"=>"error"); 
			  echo json_encode($data); 
		  } 
	   }else{ 
		  //validation error 
		  $comma_separated = implode("<br>", $check_required_fields); 
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		  echo json_encode($data); 
	  } 
   } 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>