<?php 
require_once("../layout/initialize.php"); 
//check id access 
if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
	$record_id = $_GET['id']; 
	$define_class = new MenuLink(); 
	$define_class->enable_relation(); 
	//Menu Group Info 
	$record_info =  $define_class->find_by_id($record_id);  
	//check id access 
	if(empty($record_info->id)){ 
		redirect_to("view.php");	 
	}else{ 
		$group_data = MenuGroup::find_by_id($record_info->group_id); 
		if($record_info->path_type == 'category'){ 
			//get all category 
		$define_cat_class = new Taxonomies(); 
	    $define_cat_class->enable_relation(); 
	    $records = $define_cat_class->taxonomies_data('category',null,null,'inserted_date','DESC',null,null,$general_setting_info->translate_lang_id,'yes'); 
		}else{ 
			///get all nodes 
		$define_path_class = new Nodes;	 
		$define_path_class->enable_relation(); 
		$records = $define_path_class->node_data($record_info->path_type,null,null,null,null,null,$general_setting_info->translate_lang_id,null,null,null); 
		} 
		 
		 
	} 
}else{ 
	redirect_to("../MenuGroup/view.php"); 
} 
require_once("../layout/header.php");	 
require_once("../../assets/texteditor4/head.php");  
?> 
<script type="text/javascript" src="../../js-crud/menu_links.js"></script> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Menu link Module </h4> 
    <!-- page start--> 
    <div class="row"> 
    <aside class="profile-info col-lg-8"> 
      <section> 
        <div class="panel"> 
          <div class="panel-heading"> Edit Menu link in <?php echo '"'.$group_data->title.'"' ?> </div> 
          <div class="panel-body"> 
          <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
            <input type="hidden" id="process_type" value="update"> 
            <input type="hidden" id="group_id" value="<?php echo $record_info->group_id; ?>"> 
            <input type="hidden" id="record" value="<?php echo $record_id; ?>"> 
            <section class="panel"> 
              <header class="panel-heading tab-bg-dark-navy-blue"> 
                <ul class="nav nav-tabs"> 
                  <li class=" center-block active" > <a data-toggle="tab" href="#main_info" class="text-center"><strong>Main Info</strong></a></li> 
                  <li class=" center-block"> <a data-toggle="tab" href="#link_option" class="text-center"><strong>Link Option</strong></a></li> 
                </ul> 
              </header> 
              <div class="panel-body"> 
                <div class="tab-content"> 
                  <div id="main_info" class="tab-pane active "> 
                  <section class="panel col-lg-9"> 
                    <header class="panel-heading tab-bg-dark-navy-blue "> 
                      <ul class="nav nav-tabs"> 
                        <?php 
                            //create tabs for all available languages  
                            $languages = Localization::find_all('id','desc'); 
                            $serial_tabs = 1; 
                            foreach($languages as $language){ 
                                $lang_tab_header = ucfirst($language->name); 
                                echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
                                <strong>$lang_tab_header</strong></a></li>"; 
                                $serial_tabs++; 
                            } 
                          ?> 
                      </ul> 
                    </header> 
                    <div class="panel-body"> 
                      <div class="tab-content"> 
                        <?php 
                            $serial_tabs_content = 1; 
                            foreach($languages as $language): 
								//get data by language 
								$main_content = MenuLinkContent::get_link_content($record_id, $language->id); 
								echo "<div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'>"; 
								?> 
                                 <input class='main_content' type='hidden' id='<?php echo "content_id_$language->label"; ?>' 
                          		value='<?php if(!empty($main_content )){echo $main_content->id;}else{ echo "0";} ?>'>	 
                                <div class='form-group'> 
                                  <label  class='col-lg-2'>Title:</label> 
                                  <div class='col-lg-9'> 
                                    <input type='text' class='form-control main_content' id='<?php echo "title_$language->label";?>' autocomplete='off'  
                                       value='<?php if(!empty($main_content ))echo $main_content->title; ?>' > 
                                  </div> 
                                </div> 
                                <div class='form-group'> 
                                <label  class='col-lg-2'>Description:</label> 
                                <div class='col-lg-9'> 
                                  <textarea class=' form-control main_content' id='<?php echo "description_$language->label";?>'> 
                                        <?php if(!empty($main_content ))echo $main_content->description; ?></textarea> 
                                </div> 
                              </div> 
                      </div> 
						  <?php   
                            $serial_tabs_content++; 
                                endforeach; 
                        ?> 
                    </div> 
                  </section> 
                </div> 
                <div id="link_option" class="tab-pane "> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Sorting:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="sorting" autocomplete="off" value=" <?php echo $record_info->sorting; ?>"  
                     style="width:50px;"/> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Parent:</label> 
                    <div class="col-lg-8"> 
                      <select  class="form-control" id="sid"> 
                        <option value="0">Root</option> 
                        <?php echo $define_class->getMenu(0,$record_info->parent_id,$record_info->group_id,$general_setting_info->translate_lang_id);?> 
                      </select> 
                    </div> 
                  </div> 
                  <div class="form-group" > 
                    <label  class="col-lg-2 "> Path Type:</label> 
                    <div class="col-lg-8"> 
                      <select class="form-control" id="path_type"> 
                        <option value="page" <?php if($record_info->path_type == "page") echo "selected";?>>Page</option> 
                        <option value="post" <?php if($record_info->path_type == "post") echo "selected";?>>Post</option> 
                        <option value="event" <?php if($record_info->path_type == "event") echo "selected";?>>Event</option> 
                        <option value="external" <?php if($record_info->path_type == "external") echo "selected";?>>External</option> 
                        <option value="category" <?php if($record_info->path_type == "category") echo "selected";?>>Category</option> 
                      </select> 
                    </div> 
                  </div> 
                  <div class="form-group"  > 
                    <label  class="col-lg-2 "> Path:</label> 
                    <div class="col-lg-8" id="select_div"> 
                      <input type='text' class='form-control <?php  if($record_info->path_type != 'external'){echo " hide";}?>' id='external_path'  
                  value='<?php echo $record_info->external_path?>'/> 
                      <select class="form-control  <?php  if($record_info->path_type== 'external'){ echo " hide";}?>" id="path"> 
                        <option value=""> Select <?php  if($record_info->path_type == 'category'){ echo 'category'; }else{echo"Nodes"; }?> </option> 
                        <?php foreach($records as $record){ 
						echo "<option value='$record->id'"; 
						if($record->id  == $record_info->path){ 
							echo "selected"; 
						} 
						echo ">"; 
						 if($record_info->path_type == 'category'){ echo "$record->name"; }else{echo"$record->title"; } 
						echo"</option>"; 
					}?> 
                      </select> 
                    </div> 
                    <div id="node_loading_data"></div> 
                  </div> 
                </div> 
              </div> 
              </div> 
            </section> 
            <div class="form-group"> 
              <div class="col-lg-offset-2 col-lg-10"> 
                <button type="submit" class="btn btn-info" id="submit">Save</button> 
                <button type="submit" class="btn btn-default">Cancel</button> 
                <button type="button" class="btn btn-info"  
                    onClick="window.location.href = 'view.php?group_id='+<?php echo $group_data->id?>" >View Links </button> 
                <div id="loading_data"></div> 
              </div> 
            </div> 
          </form> 
        </div> 
        </div> 
      </section> 
    </aside> 
    <div class="col-lg-4"> 
      <section class="panel panel-primary"> 
        <header class="panel-heading"> Publish Options: </header> 
        <div class="panel-body"> 
          <form class="form-horizontal tasi-form" role="form"> 
            <div class="form-group "> 
              <label class="col-lg-5">Status:</label> 
              <div class="col-lg-6"> 
                <label class="checkbox-inline"> 
                  <input type="radio" name="shadow" class="radio" value="draft" <?php if($record_info->status=="draft") echo 'checked'?>> 
                  Draft</label> 
                <label class="checkbox-inline"> 
                  <input type="radio"  name="shadow" class="radio" value="publish" <?php if($record_info->status=="publish") echo 'checked'?>> 
                  Publish</label> 
              </div> 
            </div> 
            <div class="form-group <?php if($record_info->path_type != 'category'){echo " hide";} ?>" id="drop_down"> 
                <label class="col-lg-5">Drop Down:</label> 
                <div class="col-lg-6"> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="drop_down" class="radio" value="yes" <?php if($record_info->drop_down ==  "yes") echo 'checked'?>> 
                    Yes</label> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="drop_down" class="radio" value="no" <?php if($record_info->drop_down == "no") echo 'checked'?>> 
                    No</label> 
                </div> 
              </div> 
            <div class="form-group   <?php if($record_info->path_type != 'category'){echo " hide";} ?>" id="op_div"> 
              <label class="col-lg-4">Drop Down Style:</label> 
              <div class="col-lg-8"> 
              <label class="checkbox-inline"> 
                  <input type="radio" name="drop_dwon_style" class="radio" value="0" <?php if($record_info->drop_down_style == "") echo 'checked'?>> 
                 No </label> 
                <label class="checkbox-inline"> 
                  <input type="radio" name="drop_dwon_style" class="radio" value="op1" <?php if($record_info->drop_down_style == "op1") echo 'checked'?>> 
                 Op1</label> 
                <label class="checkbox-inline"> 
                  <input type="radio"  name="drop_dwon_style" class="radio" value="op2" <?php if($record_info->drop_down_style == "op2") echo 'checked'?>> 
                  Op 2</label> 
                   <label class="checkbox-inline"> 
                  <input type="radio"  name="drop_dwon_style" class="radio" value="op3" <?php if($record_info->drop_down_style == "op3") echo 'checked'?>> 
                  Op 3</label> 
              </div> 
            </div> 
          </form> 
        </div> 
      </section> 
    </div> 
    <div class="col-lg-4"> 
      <section class="panel panel-primary"> 
        <header class="panel-heading"> Included Images & Icons :</header> 
        <div class="panel-body"> 
          <form class="form-horizontal tasi-form" role="form" id="form_image"> 
            <div class="form-group"> 
              <label  class="col-lg-2 ">Icon:</label> 
              <div class="col-lg-8"> 
                <input type="text" class="form-control" id="icon" placeholder=" " 
                        value="<?php echo $record_info->icon;?>"autocomplete="off"> 
              </div> 
            </div> 
            <div class="form-group"> 
              <label  class="col-lg-4 ">Image Cover:</label> 
              <div class="col-lg-6"> <a href="../file_mangers/media_filemanager/view_media_directories.php" id="image_cover">Select Image</a> <br /> 
                <br /> 
                <input type="hidden" class="form-control" id="imageVal" autocomplete="off" value="../../../media-library/<?php echo $record_info->image?>"> 
                <div  id="imageShow" <?php if(empty($record_info->image)) {echo "style='display:none'";} ?>>
                <?php 
                  $image_url = $record_info->image ; 
                  $explode = explode('/' , $image_url) ; 
                  $image_title = $explode[1] ; 
                  ?> 
                 <img src="../../../media-library/main_menu/<?php echo $image_title ; ?>" id="imageSrc" style="width:80px; height:80px;"></div> 
              </div> 
            </div> 
          </form> 
        </div> 
      </section> 
    </div> 
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 
<script src="../../js-crud/load_nodes.js"></script>