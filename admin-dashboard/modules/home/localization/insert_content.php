<?php 
	require_once("../layout/initialize.php"); 
	$define_class = new Localization(); 
	$id = $_GET["id"]; 
	$file_name = $_GET["file"]; 
	require_once("../layout/header.php");  
?> 
<script type="text/javascript" src="../../js-crud/crud_localization_content_file.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php"); ?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
     <section class="wrapper site-min-height"> 
      <h4> Localization  Module</h4> 
       <div class="row"> 
          <aside class="profile-info col-lg-12"> 
            <section> 
              <div class="panel"> 
                <div class="panel-heading">Add Content</div> 
                <div class="panel-body"> 
                <form  class="form-horizontal " role="form" id="form_content" action="data_model/insert_into_file.php"> 
                <input type="hidden" id="file_name" value="<?php echo  $file_name;?>"> 
                <input type="hidden" id="record" value="<?php echo  $id;?>"> 
                <div class="form-group"> 
                      <label class="col-lg-2">Content</label> 
                      <div class="col-lg-8"> 
                      <textarea class="form-control" id="file_content" placeholder="" rows="20"  ><?php $define_class->get_file_content($id,$file_name)?></textarea> 
                </div> 
                </div> 
                 
               <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                  <button type="submit" id="submit" class="btn btn-info">Submit</button> 
                  </div> 
                  </div> 
                   <div id="loading_data"></div> 
               </form> 
              </div> 
             </div> 
           </section> 
        </aside> 
       </div> 
       <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>