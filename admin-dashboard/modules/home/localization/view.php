<?php 
require_once("../layout/initialize.php"); 
$records=Localization::find_all("name","ASC"); 
require_once("../layout/header.php"); 
?> 
<script src="../../js-crud/crud_localization.js"></script> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Localization Module </h4> 
    <div class="row"> 
      <div class="col-lg-8"> 
        <section class="panel"> 
          <header class="panel-heading"> Show All Languages</header> 
          <br/> 
          <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'"  
          <?php 
		  $module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
		  $opened_module_page_insert = $module_name.'/insert'; 
		  if(!in_array($opened_module_page_insert, $user_allowed_page_array)){ 
			echo "disabled"; 
		  } 
		  ?>> 
          <li class="icon-plus-sign"></li> 
          Add New language</button> 
          <br> 
          <br> 
          <table class="table table-striped table-advance table-hover"> 
            <thead> 
              <tr> 
                <th>#</th> 
                <th><i class=""></i>Name</th> 
                <th><i class=""></i>Label</th> 
                <th><i class="">Files</i></th> 
                <th>Action</th> 
              </tr> 
            </thead> 
            <tbody> 
              <?php  
				  $serialize = 1;  
				 foreach($records as $record){ 
				 echo "<tr> 
						<td>{$serialize}</td> 
						<td>{$record->name}</td> 
						<td>{$record->label}</td> 
						<td> 
						<a href='insert_content.php?id={$record->id}&file=labels'>-labels.php</a> 
						<br> 
						<a href='insert_content.php?id={$record->id}&file=messages'>-messages.php</a> 
						</td> 
					  <td>"; 
					  		$module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
							include('../layout/btn_control_no_info.php'); 
					  echo "</td> 
				  </tr> 
				  <div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
									  <div class='modal-dialog'> 
										  <div class='modal-content'> 
											  <div class='modal-header'> 
												  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
												  <h4 class='modal-title'>Delete</h4> 
											  </div> 
											  <div class='modal-body'> 
											   <p> Are you sure you want delete  $record->name??</p> 
											  </div> 
											  <div class='modal-footer'> 
												  <button class='btn btn-warning' type='button'  
												  onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$record->id}'\"/> Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
											  </div> 
										  </div> 
									  </div> 
								  </div>"; 
				   $serialize++; 
				 }?> 
            </tbody> 
          </table> 
        </section> 
      </div> 
    </div> 
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>